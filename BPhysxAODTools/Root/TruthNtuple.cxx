#include "BPhysxAODTools/TruthNtuple.h"
#include "BPhysxAODTools/BPhysFunctions.h"
#include <sstream>
#include "TTree.h"

using namespace std;
using namespace xAOD;

TruthNtupleNode::TruthNtupleNode(int PDG, const std::string &name, TruthNtupleNode* parent) 
       : TruthParticleNode(PDG, name),
       parentLink(parent),
       b_pdg  (nullptr),
       b_barcode (nullptr),
       b_mass  (nullptr),
       b_pt(nullptr),
       b_eta(nullptr),
       b_rap(nullptr),
       b_phi(nullptr),
       b_x (nullptr),
       b_y(nullptr),
       b_z(nullptr),
       b_lxy(nullptr),
       b_lifetime(nullptr),
       b_ctau(nullptr){ AddLifetimeInfo = (parent==nullptr);   }

TruthNtupleNode::TruthNtupleNode(int PDG, const std::string &name) :
       TruthNtupleNode(PDG, name, nullptr) {} 

TruthNtupleNode*   TruthNtupleNode::addChild(int PDG, const std::string &name){

   if(m_count<max_n_children2) {
    m_count++;
    auto newnode = new TruthNtupleNode(PDG, name, this);
    m_children[m_count-1] = newnode;
    return newnode;
  }else{
    return nullptr;
  }
}

void TruthNtupleNode::Book(TTree* mytree){
   if(b_pdg == nullptr){
      b_pdg = new std::vector<int>();
      b_barcode = new std::vector<long>();
      b_mass = new std::vector<float>();
      b_pt = new std::vector<float>();
      b_eta = new std::vector<float>();
      b_rap = new std::vector<float>();
      b_phi = new std::vector<float>();
      b_x = new std::vector<float>();
      b_y = new std::vector<float>();
      b_z = new std::vector<float>();
      b_lxy = new std::vector<float>();
      b_lifetime = new std::vector<float>();
      b_ctau = new std::vector<float>();
   }
   std::string prefix = getVarName();
   prefix+="_";
   mytree->Branch((prefix + "PDG").c_str(), &b_pdg);
   mytree->Branch((prefix + "barcode").c_str(), &b_barcode);
   mytree->Branch((prefix + "mass").c_str(), &b_mass);
   mytree->Branch((prefix + "pt").c_str(), &b_pt);
   mytree->Branch((prefix + "eta").c_str(), &b_eta);
   mytree->Branch((prefix + "rap").c_str(), &b_rap);
   mytree->Branch((prefix + "phi").c_str(), &b_phi);
   mytree->Branch((prefix + "px").c_str(), &b_x);
   mytree->Branch((prefix + "py").c_str(), &b_y);
   mytree->Branch((prefix + "pz").c_str(), &b_z);
   if(AddLifetimeInfo)
   {
     mytree->Branch((prefix + "lxy").c_str(), &b_lxy);
     mytree->Branch((prefix + "lifetime").c_str(), &b_lifetime);
     mytree->Branch((prefix + "ctau").c_str(), &b_ctau);
   }
   
   for(size_t i=0;i<m_count;i++){
     static_cast<TruthNtupleNode*>(m_children[i])->Book(mytree);
   }
}

void TruthNtupleNode::Book(TTree* mytree, uint32_t *run, uint32_t *lumi, unsigned long long *event){
    mytree->Branch("run", run);
    mytree->Branch("lumiblock", lumi);
    mytree->Branch("event", event);
    Book(mytree);
}

const TruthNtupleNode* TruthNtupleNode::GetHighestParent() const{
    if(parentLink!=nullptr) return parentLink->GetHighestParent();
    else return this;
}

void TruthNtupleNode::PrepareForFill(){


   ClearNtupleTrees();

   for(size_t i=0;i < m_part.size(); i++){
       const auto p = m_part[i];
       b_pdg->emplace_back(p->pdgId());
       b_barcode->emplace_back(p->barcode());
       b_mass->emplace_back(p->m());
       b_pt->emplace_back(p->pt());
       b_eta->emplace_back(p->eta());
       b_rap->emplace_back(p->rapidity());
       b_phi->emplace_back(p->phi());
       b_x->emplace_back(p->px());
       b_y->emplace_back(p->py());
       b_z->emplace_back(p->pz());

   }

   if(AddLifetimeInfo){
     
     for(size_t i=0;i < m_part.size(); i++){
       auto highestParentVertex = GetHighestParent()->m_part.at(i)->prodVtx();
       if(highestParentVertex == 0 ){ 
          std::cout << "ERROR Highestparent null " << m_part.at(i)->pdgId() << std::endl;
          b_lxy->emplace_back(-999999.);
          b_lifetime->emplace_back(-999999.);
          b_ctau->emplace_back(-999999.);
          continue;
       }
       const auto p = m_part[i];
//       if(p == 0 ) std::cout << "p null " << std::endl;
       b_lxy->emplace_back( BPhysFunctions::lxy(p , highestParentVertex));
       b_lifetime->emplace_back( BPhysFunctions::tau(p , highestParentVertex));
       b_ctau->emplace_back( BPhysFunctions::ctau(p , highestParentVertex));
     }
   }
   for(size_t i=0;i<m_count;i++){
     static_cast<TruthNtupleNode*>(m_children[i])->PrepareForFill();
   }

}

std::string TruthNtupleNode::getVarName() const{
  
   if(parentLink!=nullptr){
       std::stringstream ss;
       ss << parentLink->getVarName() << "_";
       ss << m_name;
       return ss.str();
   }else{
       return m_name;
   }
   
}

void TruthNtupleNode::ClearNtupleTrees(){
    b_pdg->clear();
    b_barcode->clear();
    b_mass->clear();
    b_pt->clear();
    b_eta->clear();
    b_rap->clear();
    b_phi->clear();
    b_x->clear();
    b_y->clear();
    b_z->clear();
    b_lxy->clear();
    b_lifetime->clear();
    b_ctau->clear();  
}

TruthNtupleNode::~TruthNtupleNode()
{
    delete b_pdg;
    delete b_barcode;
    delete b_mass;
    delete b_pt;
    delete b_eta;
    delete b_rap;
    delete b_phi;
    delete b_x;
    delete b_y;
    delete b_z;
    delete b_lxy;
    delete b_lifetime;
    delete b_ctau;
}



