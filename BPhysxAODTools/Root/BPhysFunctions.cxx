#include "BPhysxAODTools/BPhysFunctions.h"

#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTracking/Vertex.h"
using namespace xAOD;



// @brief Access to element link to object of type T stored in auxdata
     template<class T>
     const T* getLink(const xAOD::IParticle* particle, const std::string &name){
       if (!particle) return 0;
       typedef ElementLink< DataVector<T> > Link_t;
       
       if (!particle->isAvailable< Link_t >(name) ) 
     { 
       return 0; 
     }  
       const Link_t link = particle->auxdata<Link_t>(name);
       if (!link.isValid()) 
     { 
       return 0; 
     }
       return *link;
     }



const TruthParticle* BPhysFunctions::getParentParticle(const TruthParticle* part) {

  const TruthVertex* vtx = part->prodVtx();
  if(vtx == NULL) return NULL;

  size_t nParents =vtx->nIncomingParticles();

  //return something only if there is exactly one mother
  if(nParents == 1)
    return vtx->incomingParticle(0);
  else
    return NULL;

}

double BPhysFunctions::SumPtSq(const xAOD::Vertex * vtx){
   double total =0.;
   size_t N = vtx->nTrackParticles ();
   for(size_t i =0;i<N;i++) {
     double P = vtx->trackParticle(i)->pt();
     total+= P * P;
   }
   return total;
}

double BPhysFunctions::SumPt(const xAOD::Vertex * vtx){
   double total =0.;
   size_t N = vtx->nTrackParticles ();
   for(size_t i =0;i<N;i++) total+= vtx->trackParticle(i)->pt();
   return total;
}

void BPhysFunctions::track_to_vertex(const xAOD::TrackParticle *trk , const xAOD::Vertex * vtx,       double *d0V,       double *z0V) {

	double
	xp[3] = {
	- trk->d0() * sin(trk->phi()) + trk->vx(),
	  trk->d0() * cos(trk->phi()) + trk->vy(),
	  trk->z0()                   + trk->vz()
	},
	xv[3] = {
	  vtx->x(),
	  vtx->y(),
	  vtx->z()
	},
	n[3] = {
	  sin(trk->theta()) * cos(trk->phi()),
	  sin(trk->theta()) * sin(trk->phi()),
	  cos(trk->theta())
	},
	z[3] = {0.,0.,1.},
	c = cos(trk->theta()),
	k = 0.;
	double delta[3], xpV[3];
	for (unsigned int i = 0; i < 3; i++)
		delta[i] = xv[i] - xp[i];

	for (unsigned int i = 0; i < 3; i++)
		k += delta[i] * (n[i] - c*z[i]);
	k /= (1.- pow(c,2));
	for (unsigned int i = 0; i < 3; i++)
		xpV[i] = xp[i] + n[i]*k - xv[i];
	*d0V = -sin(trk->phi())*xpV[0] + cos(trk->phi())*xpV[1];
	*z0V = xpV[2];
	return;
}

double BPhysFunctions::lxy(const TruthParticle* particle,
          const TruthVertex *firstvertex)
{
    auto decayVertex = particle->decayVtx();
    double dx = decayVertex->x() - firstvertex->x();
    double dy = decayVertex->y() - firstvertex->y();

    double pt = particle->pt();
    return (particle->px() * dx + particle->py() * dy) / pt;

}

double BPhysFunctions::ctau(const TruthParticle* particle,
          const TruthVertex *firstvertex)
{
    auto decayVertex = particle->decayVtx();
    double length = (decayVertex->v4() - firstvertex->v4()).Vect().Mag();
    const auto &p4 = particle->p4();
    return length * p4.M() / p4.Vect().Mag();
}

double BPhysFunctions::tau(const TruthParticle* particle,
          const TruthVertex *firstvertex){
//double CONST = 1000./299.792;
    constexpr double c = 1000./299.792458;
    return c * ctau(particle, firstvertex);
}

double BPhysFunctions::lxy(const TruthParticle* particle){
   return lxy(particle, particle->prodVtx());
}

double BPhysFunctions::ctau(const TruthParticle* particle){
   return ctau(particle, particle->prodVtx());
}

double BPhysFunctions::tau(const TruthParticle* particle){
   return tau(particle, particle->prodVtx());
}

const TruthParticle* BPhysFunctions::getTrkGenParticle(const TrackParticle *trk)
{
  const TruthParticle* link = getLink<TruthParticle>(trk, "truthParticleLink");

  return(link);
}

