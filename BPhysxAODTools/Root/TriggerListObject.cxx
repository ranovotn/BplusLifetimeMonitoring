#include "BPhysxAODTools/TriggerListObject.h"
#include <functional>
#include <algorithm> 
#include <iostream>

TriggerListObject::TriggerListObject(const std::string &name) : trigger_name(name), count(1), passed(0){

    hashcode = std::hash<std::string>()(name);

}



bool TriggerListObject::FindCollection(const std::string &trigger,
               const std::vector<TriggerListObject> &collection, size_t &i ){
     
    size_t hash = std::hash<std::string>()(trigger);
    size_t size = collection.size();
    for(i=0; i<size;i++){
         if(hash == collection[i].hashcode){
             return true;
         }
    }
    return false;
}

bool TriggerListObject::FindCollection(const std::string &trigger,
               const std::vector<TriggerListObject> &collection, size_t &i , size_t hint ){

    size_t hash = std::hash<std::string>()(trigger);
    size_t size = collection.size();
    size_t size2 = std::min(size, hint+1);
//    size_t foundit = 0;
    for(i=hint+1; i<size;i++){
//         foundit++;
         if(hash == collection[i].hashcode){
             goto myexit;
         }
    }
    
    for(i=0;i<size2 ;i++){
//        foundit++;
         if(hash == collection[i].hashcode){
             goto myexit;
         }
    }

    return false;   
myexit:
//    if(foundit > 1) std::cout << "found within iterations " << foundit << std::endl;    
    return true;
}


