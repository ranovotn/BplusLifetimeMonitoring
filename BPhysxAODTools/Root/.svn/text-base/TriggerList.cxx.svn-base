#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <BPhysxAODTools/TriggerList.h>
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "xAODEventInfo/EventInfo.h"

// this is needed to distribute the algorithm to the workers
ClassImp(TriggerList)

/// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )                     \
   do {                                                     \
      if( ! EXP.isSuccess() ) {                             \
         Error( CONTEXT,                                    \
                XAOD_MESSAGE( "Failed to execute: %s" ),    \
                #EXP );                                     \
         return EL::StatusCode::FAILURE;                    \
      }                                                     \
   } while( false )

TriggerList :: TriggerList () : m_trigDecisionTool(0), m_trigConfigTool(0),print_trigger_regexp(".*"),
   output_name         ("DefaultOutput"), m_event_counter(0), m_lastrun(0), m_miminalList(false)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode TriggerList :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  
  job.useXAOD();
  EL_RETURN_CHECK("setupJob()", xAOD::Init());
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerList :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  TriggerObjectList.clear();
  TFile *output_file = wk()->getOutputFile(output_name);
  m_tree         = new TTree("TriggerList", "TriggerList");
  m_tree        ->SetDirectory(output_file);
  
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerList :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerList :: changeInput (bool)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  
//  xAOD::TEvent* event = wk()->xaodEvent();

  if (m_trigDecisionTool)
    {   
    delete m_trigDecisionTool;//m_trig_config_tool
    ToolHandle< TrigConf::ITrigConfigTool > trigConfigHandle( m_trigConfigTool );
    m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
    EL_RETURN_CHECK( "initialize", m_trigDecisionTool->setProperty( "ConfigTool", trigConfigHandle ) ); // connect the TrigDecisionTool to the ConfigTool
    EL_RETURN_CHECK( "initialize", m_trigDecisionTool->setProperty( "TrigDecisionKey", "xTrigDecision" ) );
    EL_RETURN_CHECK( "initialize", m_trigDecisionTool->initialize() );
    }
  
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerList :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  xAOD::TEvent* event = wk()->xaodEvent();
  Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int
  m_trigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool"); // gives us access to the meta-data
  EL_RETURN_CHECK("initialize()", m_trigConfigTool->initialize());
  ToolHandle<TrigConf::ITrigConfigTool> trig_config_handle(m_trigConfigTool);
  m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
  EL_RETURN_CHECK("initialize()", m_trigDecisionTool->setProperty("ConfigTool", trig_config_handle)); // connect the TrigDecisionTool to the ConfigTool
  EL_RETURN_CHECK("initialize()", m_trigDecisionTool->setProperty("TrigDecisionKey", "xTrigDecision"));
  EL_RETURN_CHECK("initialize()", m_trigDecisionTool->initialize());  
  if(m_miminalList) Info("initialize()", "minimalist mode on"); else  Info("initialize()", "minimalist mode off");
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerList :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  
  m_event_counter++;
  if(m_miminalList) {
     xAOD::TEvent *event = wk()->xaodEvent();
     const xAOD::EventInfo *event_info = nullptr;
     EL_RETURN_CHECK("execute()", event->retrieve(event_info, "EventInfo"));
     auto run_number = event_info->runNumber  ();
     static_assert(std::is_same<decltype(run_number), decltype(m_lastrun)>::value, "types should match");

     if(m_lastrun == run_number) return EL::StatusCode::SUCCESS;
     m_lastrun = run_number;
  }
  
  if ((m_event_counter % 100) == 0)
    Info("execute()", "%lu events prcessed so far", m_event_counter);
  
    
    size_t lastindex = 0;
    auto chain_group = m_trigDecisionTool->getChainGroup(print_trigger_regexp.c_str());
    if(chain_group !=nullptr){
      auto list = chain_group->getListOfTriggers();
      for(auto &trigstring : list)
        {
	    bool alreadyexists = TriggerListObject::FindCollection(trigstring, TriggerObjectList, lastindex, 
	         lastindex);
//            auto chain1 = m_trigDecisionTool->getChainGroup(trigstring.c_str());     
	    if(alreadyexists){
	        TriggerObjectList.at(lastindex).count++;
	        TriggerObjectList[lastindex].passed+= m_trigDecisionTool->isPassed(trigstring);
	    }else{
	        TriggerObjectList.emplace_back(trigstring);
	        TriggerObjectList.back().passed+= m_trigDecisionTool->isPassed(trigstring);
	    }
        } 
    }else
        Error("execute()", "Trigger returned null 429");
    
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerList :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerList :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  

  
  delete m_trigDecisionTool;
  m_trigDecisionTool = nullptr;
  delete m_trigConfigTool;
  m_trigConfigTool=nullptr; 
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerList :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  TString name;
  ULong64_t hash;
  int count;
  int passed;
  m_tree->Branch("TriggerName", &name);
  m_tree->Branch("hash", &hash);
  m_tree->Branch("count", &count);
  m_tree->Branch("passed", &passed);
  Info("finalize()", "Number of triggers to write = %lu", TriggerObjectList.size() ); // print long long int  
  for(auto &ob : TriggerObjectList){
      name = ob.trigger_name;
      hash = ob.hashcode;
      count = ob.count;
      passed = ob.passed;
      Info("finalize()", " %s = %li  #%li ",name.Data(), hash, passed ); // print long long int  
      m_tree->Fill();

  }  
  
  return EL::StatusCode::SUCCESS;
}

