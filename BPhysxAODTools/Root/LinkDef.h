#include <BPhysxAODTools/TracksPVStats.h>

#include <BPhysxAODTools/TriggerList.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;


#ifdef __CINT__
#pragma link C++ class TriggerList+;
#endif


#endif

#ifdef __CINT__
#pragma link C++ class TracksPVStats+;
#endif
