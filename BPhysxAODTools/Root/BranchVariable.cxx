#include <BPhysxAODTools/BranchVariable.h>

std::map<void *, std::map<char, std::list<IBranchVariable *> > > IBranchVariable::m_branches
  = std::map<void *, std::map<char, std::list<IBranchVariable *> > >();

IBranchVariable::IBranchVariable(const char *br_name, void *master, char group)
  : m_br_name(br_name)
  {
  m_list = &(m_branches[master][group]);
  m_list->push_back(this);
  m_me = m_list->end(); m_me--;
  }

IBranchVariable::~IBranchVariable()
  {
  m_list->erase(m_me);
  }

void IBranchVariable::ResetAllBranches(void *master, char group)
  {
  std::list<IBranchVariable *> &branches = m_branches[master][group];
  for (auto br : branches)
    br->Reset();
  }

void IBranchVariable::SetAllBranches(TTree *tree, TTree *tree_single, void *master, char group)
  {
  std::list<IBranchVariable *> &branches = m_branches[master][group];
  for (auto br : branches)
    br->SetBranch(tree, tree_single);
  }

void IBranchVariable::SetAllElementsToStore(size_t i, void *master, char group)
  {
  std::list<IBranchVariable *> &branches = m_branches[master][group];
  for (auto br : branches)
    br->SetElementToStore(i);
  }
