#include "BPhysxAODTools/TrueDecayFinder.h"

#include "xAODTruth/TruthVertex.h"

#include <stdlib.h> 
#include <iostream>
#include "BPhysxAODTools/BPhysFunctions.h"
using namespace xAOD;

TruthParticleNode::TruthParticleNode(int PDG, const std::string &name) :
   m_name(name), m_PDG(PDG), m_count(0)   { }

//destructor deletes all the sub-nodes
TruthParticleNode::~TruthParticleNode() {
  for(size_t i=0;i<m_count;i++)
    delete m_children[i];
}
//-------------------------------------------------------------------------------------
TruthParticleNode* TruthParticleNode::addChild(int PDG, const std::string &name) {
  if(m_count<max_n_children2) {
    m_count++;
    m_children[m_count-1] = new TruthParticleNode(PDG, name);
    return m_children[m_count-1];
  }else{
    return nullptr;
  }
}



bool TruthParticleNode::CheckParticleUsed(size_t startindex, size_t endindex, const Truth_Particle* part){
     bool used =false;
     for(size_t i = startindex; i<endindex;i++){
          used |= m_children[i]->m_temp == part;
     }
     return used;
}

bool TruthParticleNode::FillChildren(const Truth_Particle *part, bool IgnoreCharge, bool exclusiveDecay){

      int partPDG = IgnoreCharge ? abs(part->pdgId()) : part->pdgId();
      int mypdg = IgnoreCharge ? abs(m_PDG) : m_PDG;
      if(mypdg!=partPDG) return false;
      children.clear();
      for (size_t i_child = 0; i_child < part->nChildren(); i_child++) {
        auto child = part->child(i_child);
        if (!child) continue;
        if (child->barcode() > 100000) continue; // skip G4 particles
        children.push_back(child);
      }
      m_tempFoundChildren = children.size();
      if(exclusiveDecay && m_tempFoundChildren != m_count) return false;
      if(m_count > m_tempFoundChildren) return false;

      for(size_t i=0 ; i<m_count;i++){

        bool matchedThischild = false;
        for(size_t j=0; j<m_tempFoundChildren;j++){
          //Skip already matched children
          auto child = children[j];
          if(child==nullptr) continue;
          if (CheckParticleUsed(0, i, child)) continue;
          matchedThischild = m_children[i]->FillChildren(child, IgnoreCharge, exclusiveDecay);
          if(matchedThischild){
            break;
          }
        }
        if(matchedThischild == false) return false;
      }

      if(exclusiveDecay){
       for(size_t j=0; j<m_tempFoundChildren;j++){
        auto child = part->child(j);
        if (!CheckParticleUsed(0, m_count, child) &&
            child->pdgId() != 22)
          return false;
       }
      }
      m_temp = part;
      return true;      
}

bool TruthParticleNode::DebugInternalState() {
	return DebugInternalState(m_part.size());
}

bool TruthParticleNode::DebugInternalState(size_t size) {

	if (m_temp != nullptr) return false;
	if (m_part.size() != size) return false;
	if(m_foundchildren.size()!=size) return false;
	for (auto p : m_part) if (p == nullptr) return false;
	for (size_t i = 0; i < m_count; i++) if (m_children[i]->DebugInternalState(size) == false) return false;
	return true;
}

void TruthParticleNode::ConfirmTemp(){
   m_part.push_back(m_temp);
   m_temp=nullptr; 
   m_foundchildren.push_back(m_tempFoundChildren);
   m_tempFoundChildren=0;
   for(size_t i=0;i<m_count;i++) m_children[i]->ConfirmTemp();
}

int TruthParticleNode::SegmentsWithExtraChildren(size_t i){
   int extra = !DecaySegmentWasExclusive(i);
   for(size_t i=0;i<m_count;i++) extra += m_children[i]->SegmentsWithExtraChildren(i);
   return extra;
}


int TruthParticleNode::FindTruth(const Truth_Particle_Container *TruthContainer, bool IgnoreCharge, bool exclusiveDecay){
     
	if(!m_part.empty()) ClearFound(true);
    Truth_Particle_Container::const_iterator itr = TruthContainer->begin();
    Truth_Particle_Container::const_iterator end = TruthContainer->end();
    int mypdg = IgnoreCharge ? abs(m_PDG) : m_PDG;
     
    for(; itr!=end ; ++itr){
        const Truth_Particle *part = *itr;
        int partPDG = IgnoreCharge ? abs(part->pdgId()) : part->pdgId();
        
        if(partPDG == mypdg){
           bool FillSucessFull  = FillChildren(part, IgnoreCharge, exclusiveDecay);
           if(!FillSucessFull) PopTemp();
           else ConfirmTemp();
           
        }
     }
     return NumofTruthDecaysFound();
}

void TruthParticleNode::ClearFound(bool andChildren){
     m_part.clear();
     m_foundchildren.clear();
     PopTemp();
     if(andChildren){
        for(size_t i=0;i<m_count;i++) m_children[i]->ClearFound(true);
     }
}


//return the first children NODE with the given pdg
TruthParticleNode* TruthParticleNode::getChildNodeByPdg(int pdg, bool ignoreCharge) {
  for(size_t i=0;i<m_count;i++) {
    int    PDG = ignoreCharge ? std::abs(pdg) : pdg;
    int genPDG = ignoreCharge ? std::abs(m_children[i]->m_PDG) : m_children[i]->m_PDG;
    if(genPDG == PDG)
      return m_children[i];
  }
  return NULL;
}

//-------------------------------------------------------------------------------------
TruthParticleNode* TruthParticleNode::getChildNode(size_t index) {
  if(index < m_count)
    return m_children[index];
  else
    return NULL;
}
//-------------------------------------------------------------------------------------
void TruthParticleNode::print(const std::string &offset) const {
  std::cout <<offset<< "TruthParticleNode Pdg: " << m_PDG << std::endl;
  for(size_t i=0; i<m_count; i++) {
    m_children[i]->print(offset+"  ");
  }
}

void TruthParticleNode::printResults(const std::string &offset) const {

  std::cout <<offset<< "TruthParticleNode set to find Pdg: " << m_PDG << std::endl;
  std::cout <<offset<< "Have #: " << NumofTruthDecaysFound() << std::endl;
  for(size_t i =0;i<m_part.size();i++){
     if(m_part[i])
     std::cout <<offset<< i <<  " particle: " << m_part[i]->pdgId() << std::endl;
     else std::cout << i << "not found " <<std::endl;
  }
  for(size_t i=0; i<m_count; i++) {
    m_children[i]->printResults(offset+"  ");
  }
}

//-------------------------------------------------------------------------------------
const Track_Particle* TruthParticleNode::getBestTrack(const Track_Particle_Container* trkColl, size_t cand) {
  auto tracks = findTracks(trkColl,cand);
  if(tracks.size() == 1) return tracks[0];
  return findBestMatch(tracks , cand);
}
//-------------------------------------------------------------------------------------
std::vector<const Track_Particle*> TruthParticleNode::findTracks(const Track_Particle_Container* trkColl, size_t TruthCandidate) {

  
  std::vector<const Track_Particle*> tracks;
  if(TruthCandidate >= m_part.size()) return tracks;
  auto &parttoFind = m_part[TruthCandidate];
  Track_Particle_Container::const_iterator trkItr = trkColl->cbegin();
  for(; trkItr!=trkColl->cend(); ++trkItr) {
    if(parttoFind == BPhysFunctions::getTrkGenParticle(*trkItr))
      tracks.push_back(*trkItr);
  }
  return tracks;
}
//-------------------------------------------------------------------------------------
const Track_Particle* TruthParticleNode::findBestMatch(const std::vector<const Track_Particle*> &theTracks, size_t cand) {

  double dRLast = 99999.0;
  const Track_Particle* theLastTrack = NULL;
  auto truecand = m_part[cand];
  for (auto trkItr = theTracks.cbegin(); trkItr != theTracks.cend(); ++trkItr) {
    
    double phigen = truecand->phi();
    double etagen = truecand->eta();
    double phirec = (*trkItr)->phi0();
    double etarec = (*trkItr)->eta();
    double dEta = etarec - etagen;
    double dPhi = phirec - phigen;
    while ( fabs(dPhi) > M_PI ) dPhi += ( dPhi > 0 ) ? -2*M_PI : 2*M_PI;
    double dRsq = dEta*dEta + dPhi*dPhi;
    double dR = (dRsq>0.) ? std::sqrt(dRsq) : 0;
    if (dR < dRLast) {
      dRLast = dR;
      theLastTrack = *trkItr;
    }
  }
  return theLastTrack;

}



