void TriggerListATestRun(const std::string& submitDir)
  {
  //===========================================
  // FOR ROOT6 WE DO NOT PUT THIS LINE 
  // (ROOT6 uses Cling instead of CINT)
  // Load the libraries for all packages
  // gROOT->Macro("$ROOTCOREDIR/scripts/load_packages.C");
  // Instead on command line do:
  // > root -l '$ROOTCOREDIR/scripts/load_packages.C' 'ATestRun.cxx ("submitDir")'
  // The above works for ROOT6 and ROOT5
  //==========================================

  xAOD::Init().ignore();
  SH::SampleHandler sh;

  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  // inputFilePath needsto be changed to your local input file 

  const char *inputFilePath = "/scratch/ab/temp/user.abarton.mc15_13TeV.300438.Bs_Jpsimu3p5mu3p5_phi.e4922_a766_a807_r6282.phy5.5_EXT0";
  // SH::ScanDir().sampleDepth(0).samplePattern("*.root*").scan(sh, inputFilePath);
  // SH::ScanDir().sampleDepth(0).samplePattern("user.abarton.6535775.EXT0._007877.DAOD_BPHY2.v3.pool.root").scan(sh, inputFilePath);
  SH::ScanDir().sampleDepth(3).samplePattern("*").scan(sh, inputFilePath);

//  const char *inputFilePath = gSystem->ExpandPathName("/scratch/ab/xAODBplusWork/user.abarton.data15_13TeV.periodA.AOD.repro19_v01.BPHYS2.18/");
//  const char *inputFilePath2 = gSystem->ExpandPathName("/scratch/ab/xAODBplusWork/user.abarton.data15_13TeV.periodC.AOD.repro19_v01.BPHYS2.21/user.abarton.data15_13TeV.periodC.AOD.repro19_v01.BPHYS2.21_EXT0");
//  const char *inputFilePath3 = gSystem->ExpandPathName("/scratch/ab/xAODBplusWork/user.abarton.data15_13TeV.periodD.AOD.repro19_v01.BPHYS2.20");


  SH::DiskListLocal list (inputFilePath);
//  SH::DiskListLocal list2 (inputFilePath2);
//  SH::DiskListLocal list3 (inputFilePath3);
  SH::scanSingleDir(sh, "periodA", list, "*.root*");
//  SH::scanSingleDir(sh, "periodC", list2, "*.root*");
//  SH::scanSingleDir(sh, "periodD", list3, "*.root*");



  sh.setMetaString("nc_tree", "CollectionTree");
  sh.print();

  EL::Job job;
  job.sampleHandler(sh);
  // job.options()->setDouble(EL::Job::optSkipEvents, 1);
  // job.options()->setDouble(EL::Job::optMaxEvents, 10);

  const char *output_name = "DefaultOutput";
  EL::OutputStream output(output_name);
  job.outputAdd(output);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc(output_name);
  job.algsAdd(ntuple);

  TriggerList *alg = new TriggerList();

  job.algsAdd(alg);
  alg->m_miminalList = false;
  EL::DirectDriver driver;
  driver.submit(job, submitDir);
  }
