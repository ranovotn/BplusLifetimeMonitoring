#include "TChain.h"
#include "BPhysxAODTools/TriggerListObject.h"
#include "TString.h"
#include <list>
#include <vector>
#include <iostream>
#include <algorithm>
#define dopassed true


using namespace std;

#if dopassed
bool sortfunction (const TriggerListObject &i,const TriggerListObject &j) { return (i.passed<j.passed); }
#endif


int main(int argc, char** argv) {


    TChain *tchain = new TChain("TriggerList", "Trigger list");
    tchain->SetCacheSize();
    for(int k = 1; k<argc; k++) {
        std::string argStr = argv[k];


        for (size_t i=0,n; i <= argStr.length(); i=n+1)
        {
            n = argStr.find_first_of(',',i);
            if (n == std::string::npos)
                n = argStr.length();
            tchain->Add(argStr.substr(i,n-i).c_str());
        }
    }


    size_t events = tchain->GetEntries();
    cout << "found events: " << events << endl;
    TString *name=nullptr;
    int count;
    ULong64_t hash;
#if dopassed
    int passed;
    tchain->SetBranchAddress("passed", &passed);
#endif    
    tchain->SetBranchAddress("TriggerName", &name);
    tchain->SetBranchAddress("count", &count);
    tchain->SetBranchAddress("hash", &hash);
    std::vector<TriggerListObject> TriggerObjectList;
    TriggerObjectList.reserve(200);

    size_t lastindex = 0;
    for(size_t i=0; i<events; i++) {
        tchain->GetEntry(i);
        bool alreadyexists = TriggerListObject::FindCollection(name->Data(), TriggerObjectList, lastindex,
                             lastindex);
        if(alreadyexists) {
            TriggerObjectList.at(lastindex).count+= count;
#if dopassed            
            TriggerObjectList[lastindex].passed+= passed;
#endif

        } else {
            TriggerObjectList.emplace_back(name->Data());
            TriggerObjectList.back().count = count;
#if dopassed      
            TriggerObjectList.back().passed = passed;
#endif
        }
    }
    
    delete tchain; tchain=nullptr;
    
   cout << "Unique triggers " << TriggerObjectList.size() << endl;

#if dopassed
     //Sort in reverse
     std::sort (TriggerObjectList.rbegin(), TriggerObjectList.rend(), sortfunction);
#endif

    cout << "Trigger_List Events_Present Events_fired" << endl;
    for(const auto &r : TriggerObjectList) {
        cout << r.trigger_name << "   " << r.count;
#if dopassed
        cout <<  "   " << r.passed;
#endif        
        cout << "\n";
    }

    cout << "\n \n input for code \n" << endl;
    for(const auto &r : TriggerObjectList) {
        cout << "alg->triggers.push_back(\"" << r.trigger_name << "\");  ////" << r.passed << '\n';
    }
    cout << '\n';
    cout << "Unique triggers " << TriggerObjectList.size() << endl;
    return 0;

}


