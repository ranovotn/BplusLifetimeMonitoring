#ifndef xAODBPHYS_TruthNtuple_H
#define xAODBPHYS_TruthNtuple_H

#include "BPhysxAODTools/TrueDecayFinder.h"
#include <vector>
class TTree;
namespace xAOD {

   class TruthNtupleNode : public TruthParticleNode {

   public:
       TruthNtupleNode(int PDG, const std::string &name);

       void Book(TTree* mytree);
///Use this method to include run lumi event numbers
       void Book(TTree* mytree, uint32_t *run, uint32_t *lumi, unsigned long long *event);

       virtual TruthNtupleNode*    addChild(int PDG, const std::string &name) override;

       virtual ~TruthNtupleNode();
////Call this method then call ->Fill on the TTree you booked with
       void PrepareForFill();

///If you want to force this particle to record lifetime information set this to true before booking
///By default this is true for the most senior particle and false for all others
       bool AddLifetimeInfo;

    
    //accessing children nodes
    TruthNtupleNode*  getChildNode(size_t index) {
          return static_cast<TruthNtupleNode*> (TruthParticleNode::getChildNode(index));  };


    TruthNtupleNode*  getChildNodeByPdg(int pdg, bool ignoreCharge=false){
          return static_cast<TruthNtupleNode*> (TruthParticleNode::getChildNodeByPdg(pdg, ignoreCharge)); 
     }


//    TruthNtupleNode**  getChildrenNodes() { return static_cast<TruthNtupleNode**> (m_children); }

   
   protected:

  

       TruthNtupleNode(int PDG, const std::string &name, TruthNtupleNode* parent);

       TruthNtupleNode* parentLink;

       const TruthNtupleNode* GetHighestParent() const;

    private:

       std::vector<int>*   b_pdg                ;
       std::vector<long>*  b_barcode            ;
       std::vector<float>* b_mass               ;
       std::vector<float>* b_pt                 ;
       std::vector<float>* b_eta                ;
       std::vector<float>* b_rap                ;
       std::vector<float>* b_phi                ;
       std::vector<float>* b_x                ;
       std::vector<float>* b_y                ;
       std::vector<float>* b_z                ;
       std::vector<float>* b_lxy              ;
       std::vector<float>* b_lifetime              ;
       std::vector<float>* b_ctau              ;
       std::string getVarName() const;
       void ClearNtupleTrees();
   };

}

#endif
