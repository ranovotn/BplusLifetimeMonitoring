#ifndef BPhysxAODTools_BranchVariable_H
#define BPhysxAODTools_BranchVariable_H

#include <map>
#include <string>
#include <vector>
#include <list>

#include <TTree.h>


class IBranchVariable
  {
  public:
    // constructor
    IBranchVariable(const char *br_name, void *master, char group = 0);

    virtual ~IBranchVariable();

    // reset all created branches
    static void ResetAllBranches(void *master, char group = 0);

    // set address for all 
    // created branches
    static void SetAllBranches(TTree *tree, TTree *tree_single, void *master, char group = 0);

    // set single tree element index to store
    // for all created branches
    static void SetAllElementsToStore(size_t i, void *master, char group = 0);


  protected:
    // reset the variable
    // after processing an
    // event
    virtual void Reset() = 0;

    // define it within trees
    virtual void SetBranch(TTree *tree, TTree *tree_single) = 0;

    // set single tree element index to store
    virtual void SetElementToStore(size_t i) = 0;
    


    // list of all created branches (per each NtupleMaker pointer, per each group)
    static std::map<void *, std::map<char, std::list<IBranchVariable *> > > m_branches; //!

    // name of the branch
    std::string m_br_name; //!

    // list I'm being kept in
    std::list<IBranchVariable *> *m_list; //!

    // iterator pointing to myself in that list
    std::list<IBranchVariable *>::iterator m_me;
  };

template<typename T>
class ScalarBranchVariable : public IBranchVariable
  {
  public:
    T value; //!

    // constructor
    ScalarBranchVariable(const char *br_name, void *master, char group = 0) : IBranchVariable(br_name, master, group) {}

    ~ScalarBranchVariable() override {}
    T MainValue(){
        return value;
    }
    // conversion operator
    operator T() const {return value;}
    // assignment
    T &operator=(T v) {value = v; return value;}

  protected:
    void Reset() override {}
    void SetBranch(TTree *tree, TTree *tree_single) override
      {
      if (tree)
        tree       ->Branch(m_br_name.c_str(), &value);
      if (tree_single)
        tree_single->Branch(m_br_name.c_str(), &value);
      }
    void SetElementToStore(size_t) override {}
  };

template<typename T>
class VectorBranchVariable : public IBranchVariable, public std::vector<T>
  {
  public:
    // constructor
    VectorBranchVariable(const char *br_name, void *master, char group = 0) : IBranchVariable(br_name, master, group) {}

     ~VectorBranchVariable() override {}

    T MainValue(){
        return m_single_value;
    }

  protected:
    void Reset() override {std::vector<T>::clear();}
    void SetBranch(TTree *tree, TTree *tree_single) override
      {
      if (tree)
        tree       ->Branch(m_br_name.c_str(), static_cast<std::vector<T> *>(this));
      if (tree_single)
        tree_single->Branch(m_br_name.c_str(), &m_single_value);
      }
    void SetElementToStore(size_t i) override
      {
      m_single_value = std::vector<T>::at(i);
      }

    T m_single_value;
  };

// vector branch with its single element
// mapped by a separate vector
// (intended to be used for Jpsi branches 
// mapped by B_Jpsi_index)
template<typename T1, typename T2>
class VectorBranchVariableMapped : public VectorBranchVariable<T1>
  {
  public:
    // constructor
    VectorBranchVariableMapped(const char *br_name, std::vector<T2> *map, void *master, char group = 0)
      : VectorBranchVariable<T1>(br_name, master, group), m_map(map){}

    virtual ~VectorBranchVariableMapped() {}

  private:
    void SetElementToStore(size_t i) override
      {
      VectorBranchVariable<T1>::m_single_value = std::vector<T1>::at(m_map->at(i));
      }

    std::vector<T2> *m_map;
  };


#endif
