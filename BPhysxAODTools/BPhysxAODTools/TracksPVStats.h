#ifndef BPhysxAODTools_TracksPVStats_H
#define BPhysxAODTools_TracksPVStats_H

#include <EventLoop/Algorithm.h>
class TTree;

class TracksPVStats : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  std::string output_name;
  TTree *m_tree;//!
  // this is a standard constructor
  TracksPVStats ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(TracksPVStats, 1);
  
private:

    unsigned long long           eventNumber;//!
    uint32_t        lumiBlock;//!
    uint32_t        runNumber;//!
    uint32_t        TrackCount;//!
    uint32_t        PVCount;//!
  
};

#endif
