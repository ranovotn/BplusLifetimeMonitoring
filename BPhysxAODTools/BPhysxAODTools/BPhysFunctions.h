#ifndef xAODBPHYS_BPhysFunctions_H
#define xAODBPHYS_BPhysFunctions_H

#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTracking/TrackParticle.h"
#include <array>

template<typename TYPE,std::size_t SIZE>
bool collectionsMatch(const std::array<const TYPE*, SIZE>&coll1, const std::array<const TYPE*, SIZE>&coll2 ) {
    bool found = true;
    for(size_t i=0;i<SIZE;i++){
       found &= std::find(coll2.cbegin(), coll2.cend(), coll1[i]) != coll2.cend();
    }
    return found;
}


class BPhysFunctions{
public:

    static double SumPtSq(const xAOD::Vertex * vtx);
    static double SumPt(const xAOD::Vertex * vtx);

    static void track_to_vertex(const xAOD::TrackParticle *trk , const xAOD::Vertex * vtx,       double *d0V,       double *z0V);

    static void track_to_vertex(const xAOD::TrackParticle *trk , const xAOD::Vertex * vtx,       float *d0V,       float *z0V){
        double d, z;
        track_to_vertex(trk, vtx, &d, &z);
        *d0V =d;
        *z0V =z;
    }

////RETURNS DOUBLES BUT MANY PARAMETERS USED ARE FLOATS, EXPECT SUB DOUBLE ACCURACY

    //get mother particle if it exists. Otherwise return NULL
    static const xAOD::TruthParticle*  getParentParticle(const xAOD::TruthParticle* part);
    static double ctau(const xAOD::TruthParticle* particle,
          const xAOD::TruthVertex *firstvertex);

    static double tau(const xAOD::TruthParticle* particle,
          const xAOD::TruthVertex *firstvertex);

    static double lxy(const xAOD::TruthParticle* particle,
          const xAOD::TruthVertex *firstvertex);

    static double ctau(const xAOD::TruthParticle* particle);

    static double tau(const xAOD::TruthParticle* particle);

    static double lxy(const xAOD::TruthParticle* particle);

    // pointer to the topology node matched to this true particle
    static const xAOD::TruthParticle* getTrkGenParticle(const xAOD::TrackParticle *trk);

};



#endif

