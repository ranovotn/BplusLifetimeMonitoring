#ifndef BPhysxAODTools_TriggerList_H
#define BPhysxAODTools_TriggerList_H

#include <EventLoop/Algorithm.h>
#include <BPhysxAODTools/TriggerListObject.h>
#include <vector>

class TTree;
namespace Trig
  {
  class TrigDecisionTool;
  }
namespace TrigConf
  {
  class xAODConfigTool;
  }



class TriggerList : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!
  TTree *m_tree;//!
  std::vector<TriggerListObject> TriggerObjectList;//!
  Trig::TrigDecisionTool *m_trigDecisionTool; //!
  TrigConf::xAODConfigTool *m_trigConfigTool; //!
  std::string print_trigger_regexp;//!
  std::string output_name;//!
  size_t m_event_counter;//!
  uint32_t m_lastrun;//!
  bool m_miminalList;
  // this is a standard constructor
  TriggerList ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(TriggerList, 1);
};

#endif
