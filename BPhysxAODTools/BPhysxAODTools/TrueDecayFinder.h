#ifndef xAODBPHYS_TruthParticleNode_H
#define xAODBPHYS_TruthParticleNode_H

#include <vector>
#include <string>
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include <algorithm>

#define max_n_children2 10

///Author Adam Barton, inspired by run-1 TruthFinder version by Dan Scherich



namespace xAOD {

//class TruthParticle;
//class TrackParticle;
//class TrackParticleContainer;
//class TruthParticleContainer;
//class TruthVertex;

//Defining types here for more portability
typedef xAOD::TruthParticle Truth_Particle;
typedef xAOD::TrackParticle Track_Particle;
typedef xAOD::TrackParticleContainer Track_Particle_Container;
typedef xAOD::TruthParticleContainer Truth_Particle_Container;
typedef xAOD::TruthVertex Truth_Vertex;




///This class allows defined decay chains to be quickly identified in a truthparticle collection
///Options include demanding exclusive decay chains (ignoring decay chains that feature additional children in addition to those asked for)
///And ignoring charge - Allowing collection of anti particles at the same time
///If there are multiple chains this will find all of them
class TruthParticleNode {

protected:
	///In the event mutiple tracks are associated with a truth particle this will select the most appropriate
    const Track_Particle* findBestMatch(const std::vector<const Track_Particle*> &theTracks, size_t cand);
	///Once it is determined that a particle matches the decay chain it is added to m_part.
    void ConfirmTemp();
    
    const std::string m_name;///Name of particle - may be used for ntuple dumping
    int m_PDG;///PDG code of the wanted particle in this node
    size_t m_count; ///The number of children owned by this node
    const Truth_Particle* m_temp; ///This hold the identified particle for this node while other nodes are check
    size_t m_tempFoundChildren;
    std::vector<const Truth_Particle *> children;
    ///This holds the found candidates of truth particles. Each node assoicated should have the same number
    std::vector<const Truth_Particle*> m_part; 
    std::vector<size_t> m_foundchildren;
	
    bool FillChildren(const Truth_Particle *part, bool IgnoreCharge, bool exclusiveDecay);

    bool CheckParticleUsed(size_t startindex, size_t endindex, const Truth_Particle* part);
    ///Holds links to children nodes, the highest node owns the children nodes and will clean it up when deleted
	///DO NOT DELETE children NODES - WILL LEAD TO DOUBLE DELETES AND UNDEFINED BEHAVIOUS
    TruthParticleNode* m_children[max_n_children2];
    void PopTemp() { m_temp=nullptr; m_tempFoundChildren=0; for(size_t i=0;i<m_count;i++) m_children[i]->PopTemp();}


public:
    //constructors Constructs first node that should be the most senior particle in your chain
    TruthParticleNode(int PDG, const std::string &name);

    //destructor deletes all the sub-nodes
    virtual ~TruthParticleNode();

    //adding children - children are owned by parent node - DO NOT DELETE CHILDREN - only delete top most parent
	//Adds a children node to selected node, requiring the give PDG code
    virtual TruthParticleNode*                       addChild(int PDG, const std::string &name);


    //accessing children nodes
    TruthParticleNode*                       getChildNode(size_t index);
    TruthParticleNode*                       getChildNodeByPdg(int pdg, bool ignoreCharge=false);
    TruthParticleNode**                      getChildrenNodes() { return m_children; }

    //number of childrens
    size_t                                   getNChildren() const { return m_count; }

	//Get the indended PDG code for this node (the one you set when constructing)
    int getIntendedPDG() { return m_PDG;}

    // get this node's HepMC particle
    const std::vector<const Truth_Particle*>& getParticles() { return m_part; }
    const Truth_Particle* getTrueParticle(size_t i) { return m_part.at(i); }
    size_t getTrueParticleNumberChildren(size_t i) { return m_foundchildren.at(i); }
    bool DecaySegmentWasExclusive(size_t i) { return m_foundchildren.at(i) == m_count;}
    int SegmentsWithExtraChildren(size_t i);
    
    //retrieve truth-mathced TrackParticles from the collection if there are multiple matches this returns the most appropriate
    const Track_Particle*                     getBestTrack(const Track_Particle_Container* trkColl, size_t cand_index);
	//retrieve truth-mathced TrackParticles from the collection returns a vector of all matches
    std::vector<const Track_Particle*>  findTracks(const Track_Particle_Container* trkColl, size_t TruthCandidate);

    //print decay trees
    void print(const std::string &offset) const;
    void print() const { print(""); }
    void printResults(const std::string &offset) const;
    void printResults() const { printResults(""); }
    
	///This is the main method to call, this finds all the instances of the requested chain in TruthContainer
	/// Set IgnoreCharge to true to consider any charge of the particles in you chain
	///Set exclusive decay to true to ignore chains that have additional children to the ones you requested
    int FindTruth(const Truth_Particle_Container *TruthContainer, bool IgnoreCharge, bool exclusiveDecay);


	///Resets the found decays (will be called automatically when FindTruth is called)
    void ClearFound(bool andchildren=true);
    size_t NumofTruthDecaysFound() const { return m_part.size();  }
    bool DebugInternalState();
    
private:
    bool DebugInternalState(size_t size);

};

}
/////////////////////////////////////////////////////////////////////////////


#endif
