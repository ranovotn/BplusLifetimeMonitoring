#ifndef BPhysxAODTools_TriggerListObject_H
#define BPhysxAODTools_TriggerListObject_H

#include <string>
#include <vector>


class TriggerListObject{
   public:
     std::string trigger_name;
     size_t hashcode;
     int count;
     int passed;
     TriggerListObject(const std::string &name);
     

    TriggerListObject(TriggerListObject&& o) noexcept : trigger_name(std::move(o.trigger_name)), hashcode(o.hashcode),
 count(o.count), passed(o.passed)  {};
 
 TriggerListObject& operator=(TriggerListObject&& other) {
    trigger_name = std::move(other.trigger_name);
    hashcode = other.hashcode;
    count = other.count;
    passed = other.passed;    
    return *this;
}
 

     TriggerListObject(const TriggerListObject& o) : trigger_name(o.trigger_name), hashcode(o.hashcode), 
  count(o.count), passed(o.passed)  {};
     
     TriggerListObject&  operator= ( const TriggerListObject& o) {
      trigger_name = o.trigger_name;
      hashcode = o.hashcode;
      count = o.count;
       passed = o.passed;
       return *this;
     }
     
     static bool FindCollection(const std::string &trigger,
               const std::vector<TriggerListObject> &collection, size_t &i , size_t hint );
     
     static bool FindCollection(const std::string &trigger,
               const std::vector<TriggerListObject> &collection, size_t &i );

};

#endif

