#ifndef UTILS
#define UTILS

#include "TFile.h"
#include "TTimeStamp.h"
#include "TApplication.h"

Bool_t rootFileExists(const TString &fileName);
Bool_t inOptions(const TString &options, const TString &str);
void splitString(const TString &inputString, const Char_t *delimiter, vector<TString> &elements);
vector<TString> splitString(const TString &inputString, const Char_t *delimiter = " ");
TString d2string(Double_t x, const Char_t *format = "%.6g");
TString printTime(const time_t &t);
void checkDir(const TString &name) { if ( system("[ -d $(dirname "+name+") ] || mkdir $(dirname "+name+")") ) gApplication->Terminate(); };

#endif