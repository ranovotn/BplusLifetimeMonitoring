#include "ModelPDF.h"

ModelPDF::~ModelPDF() {
    TIterator *pars_it = m_pars->createIterator();
    for ( RooRealVar *p = (RooRealVar*)(pars_it->Next()); p != NULL ; p = (RooRealVar*)(pars_it->Next()) ) {
//         TString name = p->GetName();
        delete p;
//         cout << "INFO: Parameter \"" << name << "\" deleted." << endl;
    }
    delete pars_it;

    TIterator *model_it = m_totalPDF->getComponents()->createIterator();
    std::vector<TObject *> model_vec;
    for ( TObject *m = model_it->Next(); m != NULL ; m = model_it->Next() ) {
        TString title = m->GetTitle();
        if ( ! title.Contains("convoluted with basis function") && ! title.Contains("Recursive Fraction") ) model_vec.push_back(m);
    }
    delete model_it;
    for ( Size_t i = 0; i < model_vec.size(); i++ ) {
//         TString name = model_vec.at(i)->GetName();
        delete model_vec.at(i);

//         cout << "INFO: Model \"" << name << "\" deleted." << endl;
    }

//     cout << "INFO: Model \"" << m_name << "\" deleted." << endl;
}

void ModelPDF::setMassSignalPDF(const TString &name, const TString &pdf) {
    if ( pdf == "gauss" ) {
        RooRealVar *mean      = new RooRealVar (m_name+"_"+name+"_mean", "Mean of Gaussian", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma     = new RooRealVar (m_name+"_"+name+"_sigma", "Sigma of Gaussian", 1e-9, RooNumber::infinity());
        m_massSignalCorePDF   = new RooGaussian(m_name+"_"+name, "Mass Signal Gaussian", *m_observables->get(MASS), *mean, *sigma);
    } else if ( pdf == "doubleGauss" ) {
        RooRealVar *mean     = new RooRealVar (m_name+"_"+name+"_mean", "Mean of Gaussian", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma1   = new RooRealVar (m_name+"_"+name+"_sigma1", "Sigma1 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma2   = new RooRealVar (m_name+"_"+name+"_sigma2", "Sigma2 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *fraction = new RooRealVar (m_name+"_"+name+"_fraction", "Fraction of DoubleGaussian", 0., 1.);
        RooGaussian *gauss1  = new RooGaussian(m_name+"_"+name+"_gauss1", "Gaussian 1", *m_observables->get(MASS), *mean, *sigma1);
        RooGaussian *gauss2  = new RooGaussian(m_name+"_"+name+"_gauss2", "Gaussian 2", *m_observables->get(MASS), *mean, *sigma2);
        m_massSignalCorePDF  = new RooAddPdf  (m_name+"_"+name, "Mass Signal DoubleGaussian", RooArgList(*gauss1, *gauss2), *fraction);
    } else if ( pdf == "tripleGauss" ) {
        RooRealVar *mean      = new RooRealVar (m_name+"_"+name+"_mean", "Mean of Gaussian", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma1    = new RooRealVar (m_name+"_"+name+"_sigma1", "Sigma1 of TripleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma2    = new RooRealVar (m_name+"_"+name+"_sigma2", "Sigma2 of TripleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma3    = new RooRealVar (m_name+"_"+name+"_sigma3", "Sigma3 of TripleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *fraction1 = new RooRealVar (m_name+"_"+name+"_fraction1", "Fraction1 of TripleGaussian", 0., 1.);
        RooRealVar *fraction2 = new RooRealVar (m_name+"_"+name+"_fraction2", "Fraction2 of TripleGaussian", 0., 1.);
        RooGaussian *gauss1   = new RooGaussian(m_name+"_"+name+"_gauss1", "Gaussian 1", *m_observables->get(MASS), *mean, *sigma1);
        RooGaussian *gauss2   = new RooGaussian(m_name+"_"+name+"_gauss2", "Gaussian 2", *m_observables->get(MASS), *mean, *sigma2);
        RooGaussian *gauss3   = new RooGaussian(m_name+"_"+name+"_gauss3", "Gaussian 3", *m_observables->get(MASS), *mean, *sigma3);
        m_massSignalCorePDF   = new RooAddPdf  (m_name+"_"+name, "Mass Signal TripleGaussian", RooArgList(*gauss1, *gauss2, *gauss3), RooArgList(*fraction1, *fraction2), kTRUE);
    } else if ( pdf == "gaussPCE" ) {
        RooRealVar *mean        = new RooRealVar (m_name+"_"+name+"_mean", "Mean of Gaussian", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *scaleFactor = new RooRealVar (m_name+"_"+name+"_scaleFactor", "SF of Gaussian", 1., 0.1, 10.);
        RooProduct *sigma       = new RooProduct (m_name+"_"+name+"_sigma", "massErr * Scale Factor", RooArgList(*m_observables->get(MASSERROR), *scaleFactor));
        m_massSignalCorePDF     = new RooGaussian(m_name+"_"+name, "Mass Signal Gaussian with per Candidate Error", *m_observables->get(MASS), *mean, *sigma);
        m_conditionalObservables.add(*m_observables->get(MASSERROR));
        m_useProjWDataMass = true;
        m_massSignalPDFs.add(*m_massSignalCorePDF);
    } else if ( pdf == "relBWSwave" ) {
        RooRealVar *mean    = new RooRealVar(m_name+"_"+name+"_mean", "Mean of RelBW", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *gamma   = new RooRealVar(m_name+"_"+name+"_gamma", "Gamma of RelBW", 0., RooNumber::infinity());
        RooRealVar *massA   = new RooRealVar(m_name+"_"+name+"_massA", "MassA of RelBW", 0., RooNumber::infinity());
        RooRealVar *massB   = new RooRealVar(m_name+"_"+name+"_massB", "MassB of RelBW", 0., RooNumber::infinity());
        m_massSignalCorePDF = new RooRelBW  (m_name+"_"+name, "Relativistic Breit-Wigner (S-wave)", *m_observables->get(MASS), *mean, *gamma, *massA, *massB, (RooAbsReal&)RooFit::RooConst(0));
    } else if ( pdf == "relBWPwave" ) {
        RooRealVar *mean    = new RooRealVar(m_name+"_"+name+"_mean", "Mean of RelBW", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *gamma   = new RooRealVar(m_name+"_"+name+"_gamma", "Gamma of RelBW", 0., RooNumber::infinity());
        RooRealVar *massA   = new RooRealVar(m_name+"_"+name+"_massA", "MassA of RelBW", 0., RooNumber::infinity());
        RooRealVar *massB   = new RooRealVar(m_name+"_"+name+"_massB", "MassB of RelBW", 0., RooNumber::infinity());
        m_massSignalCorePDF = new RooRelBW  (m_name+"_"+name, "Relativistic Breit-Wigner (P-wave)", *m_observables->get(MASS), *mean, *gamma, *massA, *massB, (RooAbsReal&)RooFit::RooConst(1));
    } else if ( pdf == "relBWPythiaCorrected" ) {
        RooRealVar *mean    = new RooRealVar(m_name+"_"+name+"_mean", "Mean of RelBW", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *gamma   = new RooRealVar(m_name+"_"+name+"_gamma", "Gamma of RelBW", 0., RooNumber::infinity());
        RooRealVar *massA   = new RooRealVar(m_name+"_"+name+"_massA", "MassA of RelBW", 0., RooNumber::infinity());
        RooRealVar *massB   = new RooRealVar(m_name+"_"+name+"_massB", "MassB of RelBW", 0., RooNumber::infinity());
        m_massSignalCorePDF = new RooRelBW  (m_name+"_"+name+"_relBW", "Relativistic Breit-Wigner (Pythia corrected Gamma)", *m_observables->get(MASS), *mean, *gamma, *massA, *massB, (RooAbsReal&)RooFit::RooConst(-1));
    } else if ( pdf == "relBWPythiaOrig" ) {
        RooRealVar *mean    = new RooRealVar(m_name+"_"+name+"_mean", "Mean of RelBW", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *gamma   = new RooRealVar(m_name+"_"+name+"_gamma", "Gamma of RelBW", 0., RooNumber::infinity());
        RooRealVar *massA   = new RooRealVar(m_name+"_"+name+"_massA", "MassA of RelBW", 0., RooNumber::infinity());
        RooRealVar *massB   = new RooRealVar(m_name+"_"+name+"_massB", "MassB of RelBW", 0., RooNumber::infinity());
        m_massSignalCorePDF = new RooRelBW  (m_name+"_"+name+"_relBW", "Relativistic Breit-Wigner (Pythia original Gamma)", *m_observables->get(MASS), *mean, *gamma, *massA, *massB, (RooAbsReal&)RooFit::RooConst(-2));
    } else if ( pdf == "relBWStaticGamma" ) {
        RooRealVar *mean    = new RooRealVar(m_name+"_"+name+"_mean", "Mean of RelBW", 1e-9, RooNumber::infinity()); // to avoid RooAbsReal::logEvalError
        RooRealVar *gamma   = new RooRealVar(m_name+"_"+name+"_gamma", "Gamma of RelBW", 1e-9, RooNumber::infinity()); // to avoid RooAbsReal::logEvalError
        RooRealVar *massA   = new RooRealVar(m_name+"_"+name+"_massA", "MassA of RelBW", 0., RooNumber::infinity());
        RooRealVar *massB   = new RooRealVar(m_name+"_"+name+"_massB", "MassB of RelBW", 0., RooNumber::infinity());
        m_massSignalCorePDF = new RooRelBW  (m_name+"_"+name+"_relBW", "Relativistic Breit-Wigner (static Gamma)", *m_observables->get(MASS), *mean, *gamma, *massA, *massB, (RooAbsReal&)RooFit::RooConst(-9));
    } else if ( pdf == "doubleGaussAndMisRecGauss" ) {
        RooRealVar *mean       = new RooRealVar (m_name+"_"+name+"_mean",   "Mean of Gaussian",        -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *meanMR     = new RooRealVar (m_name+"_"+name+"_meanMR", "Mean of MisRec Gaussian", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma1     = new RooRealVar (m_name+"_"+name+"_sigma1",  "Sigma1 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma2     = new RooRealVar (m_name+"_"+name+"_sigma2",  "Sigma2 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigmaMR    = new RooRealVar (m_name+"_"+name+"_sigmaMR", "Sigma of MisRec Gaussian", 1e-9, RooNumber::infinity());
        RooRealVar *fraction   = new RooRealVar (m_name+"_"+name+"_fraction",   "Fraction of DoubleGaussian",  0., 1.);
        RooRealVar *fractionMR = new RooRealVar (m_name+"_"+name+"_fractionMR", "Fraction of MisRec Gaussian", 0., 1.);
        RooGaussian *gauss1    = new RooGaussian(m_name+"_"+name+"_gauss1",  "Gaussian 1",      *m_observables->get(MASS), *mean,   *sigma1);
        RooGaussian *gauss2    = new RooGaussian(m_name+"_"+name+"_gauss2",  "Gaussian 2",      *m_observables->get(MASS), *mean,   *sigma2);
        RooGaussian *gaussMR   = new RooGaussian(m_name+"_"+name+"_gaussMR", "MisRec Gaussian", *m_observables->get(MASS), *meanMR, *sigmaMR);
        m_massSignalCorePDF    = new RooAddPdf  (m_name+"_"+name, "Mass Signal DoubleGaussian and MisRec Gaussian", RooArgList(*gaussMR, *gauss1, *gauss2), RooArgList(*fractionMR, *fraction), kTRUE);
    } else if ( pdf == "doubleGaussAndMisRecDoubleGauss" ) {
        RooRealVar *mean       = new RooRealVar (m_name+"_"+name+"_mean",   "Mean of Gaussian",        -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *meanMR     = new RooRealVar (m_name+"_"+name+"_meanMR", "Mean of MisRec Gaussian", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma1     = new RooRealVar (m_name+"_"+name+"_sigma1",  "Sigma1 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma2     = new RooRealVar (m_name+"_"+name+"_sigma2",  "Sigma2 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma1MR    = new RooRealVar (m_name+"_"+name+"_sigma1MR", "Sigma1 of MisRec Gaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma2MR    = new RooRealVar (m_name+"_"+name+"_sigma2MR", "Sigma2 of MisRec Gaussian", 1e-9, RooNumber::infinity());
        RooRealVar *fraction   = new RooRealVar (m_name+"_"+name+"_fraction",   "Fraction of DoubleGaussian",  0., 1.);
        RooRealVar *fractionMR = new RooRealVar (m_name+"_"+name+"_fractionMR", "Fraction of MisRec Gaussian", 0., 1.);
        RooGaussian *gauss1    = new RooGaussian(m_name+"_"+name+"_gauss1",  "Gaussian 1",      *m_observables->get(MASS), *mean,   *sigma1);
        RooGaussian *gauss2    = new RooGaussian(m_name+"_"+name+"_gauss2",  "Gaussian 2",      *m_observables->get(MASS), *mean,   *sigma2);
        RooGaussian *gauss1MR   = new RooGaussian(m_name+"_"+name+"_gauss1MR", "MisRec Gaussian1", *m_observables->get(MASS), *meanMR, *sigma1MR);
        RooGaussian *gauss2MR   = new RooGaussian(m_name+"_"+name+"_gauss2MR", "MisRec Gaussian2", *m_observables->get(MASS), *meanMR, *sigma2MR);
        
        RooRealVar *fractionSigToMR   = new RooRealVar (m_name+"_"+name+"_fractionSigToMR",   "Fraction of Signnal to MisRec signal",  0., 1.);
        
        RooAddPdf *gaussSignal = new RooAddPdf  (m_name+"_"+name+"_gaussSignal", "Mass Signal DoubleGaussian", RooArgList(*gauss1, *gauss2), *fraction, kTRUE);
        RooAddPdf *gaussMR = new RooAddPdf  (m_name+"_"+name+"_gaussMR", "Mass MisRec DoubleGaussian", RooArgList(*gauss1MR, *gauss2MR),*fractionMR, kTRUE);
        
        m_massSignalCorePDF    = new RooAddPdf  (m_name+"_"+name, "Mass Signal DoubleGaussian and MisRec Gaussian", RooArgList(*gaussSignal,*gaussMR),*fractionSigToMR, kTRUE);
    }else if ( pdf == "doubleGaussAndMisRecCB" ) {
        RooRealVar *mean       = new RooRealVar (m_name+"_"+name+"_mean",   "Mean of Gaussian",        -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *meanMR     = new RooRealVar (m_name+"_"+name+"_meanMR", "Mean of MisRec CBshape", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma1     = new RooRealVar (m_name+"_"+name+"_sigma1",  "Sigma1 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma2     = new RooRealVar (m_name+"_"+name+"_sigma2",  "Sigma2 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigmaMR    = new RooRealVar (m_name+"_"+name+"_sigmaMR", "Sigma of MisRec CBshape", 1e-9, RooNumber::infinity());
        RooRealVar *fraction   = new RooRealVar (m_name+"_"+name+"_fraction",   "Fraction of DoubleGaussian",  0., 1.);
        RooRealVar *fractionMR = new RooRealVar (m_name+"_"+name+"_fractionMR", "Fraction of MisRec Gaussian", 0., 1.);
        RooGaussian *gauss1    = new RooGaussian(m_name+"_"+name+"_gauss1",  "Gaussian 1",      *m_observables->get(MASS), *mean,   *sigma1);
        RooGaussian *gauss2    = new RooGaussian(m_name+"_"+name+"_gauss2",  "Gaussian 2",      *m_observables->get(MASS), *mean,   *sigma2);
        RooRealVar *alphaMR    = new RooRealVar (m_name+"_"+name+"_alphaMR",   "alpha of MisRec CBshape",        -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *nMR    = new RooRealVar (m_name+"_"+name+"_nMR",   "n of MisRec CBshape",        -RooNumber::infinity(), RooNumber::infinity());
        RooCBShape *crystalBallMR = new RooCBShape(m_name+"_"+name+"_crystalBallMR", "MisRec CBshape", *m_observables->get(MASS), *meanMR, *sigmaMR, *alphaMR, *nMR);
        
        RooAddPdf *gaussSignal = new RooAddPdf  (m_name+"_"+name+"_gaussSignal", "Mass Signal DoubleGaussian", RooArgList(*gauss1, *gauss2), *fraction, kTRUE);
        m_massSignalCorePDF    = new RooAddPdf  (m_name+"_"+name, "Mass Signal DoubleGaussian and MisRec Gaussian", RooArgList(*gaussSignal,*crystalBallMR), *fractionMR, kTRUE);
    }else if ( pdf == "doubleGaussAndMisRecDSCB" ) {
        RooRealVar *mean       = new RooRealVar (m_name+"_"+name+"_mean",   "Mean of Gaussian",        -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *meanMR     = new RooRealVar (m_name+"_"+name+"_meanMR", "Mean of MisRec DSCBshape", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma1     = new RooRealVar (m_name+"_"+name+"_sigma1",  "Sigma1 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma2     = new RooRealVar (m_name+"_"+name+"_sigma2",  "Sigma2 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigmaMR    = new RooRealVar (m_name+"_"+name+"_sigmaMR", "Sigma of MisRec DSCBshape", 1e-9, RooNumber::infinity());
        RooRealVar *fraction   = new RooRealVar (m_name+"_"+name+"_fraction",   "Fraction of DoubleGaussian",  0., 1.);
        RooRealVar *fractionMR = new RooRealVar (m_name+"_"+name+"_fractionMR", "Fraction of MisRec Gaussian", 0., 1.);
        RooGaussian *gauss1    = new RooGaussian(m_name+"_"+name+"_gauss1",  "Gaussian 1",      *m_observables->get(MASS), *mean,   *sigma1);
        RooGaussian *gauss2    = new RooGaussian(m_name+"_"+name+"_gauss2",  "Gaussian 2",      *m_observables->get(MASS), *mean,   *sigma2);
        RooRealVar *alphaLMR    = new RooRealVar (m_name+"_"+name+"_alphaLMR",   "alphaL of MisRec DSCBshape",        -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *alphaRMR    = new RooRealVar (m_name+"_"+name+"_alphaRMR",   "alphaR of MisRec DSCBshape",        -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *nMR    = new RooRealVar (m_name+"_"+name+"_nMR",   "n of MisRec DSCBshape",        -RooNumber::infinity(), RooNumber::infinity());
        RooDSCB *crystalBallMR = new RooDSCB(m_name+"_"+name+"_crystalBallMR", "MisRec DSCBshape", *m_observables->get(MASS), *meanMR, *sigmaMR, *alphaLMR,*alphaRMR, *nMR);
        
        RooAddPdf *gaussSignal = new RooAddPdf  (m_name+"_"+name+"_gaussSignal", "Mass Signal DoubleGaussian", RooArgList(*gauss1, *gauss2), *fraction, kTRUE);
        m_massSignalCorePDF    = new RooAddPdf  (m_name+"_"+name, "Mass Signal DoubleGaussian and MisRec Gaussian", RooArgList(*gaussSignal,*crystalBallMR), *fractionMR, kTRUE);
    }else if ( pdf == "tripleGaussAndMisRecDSCB" ) {
        RooRealVar *mean       = new RooRealVar (m_name+"_"+name+"_mean",   "Mean of Gaussian",        -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *meanMR     = new RooRealVar (m_name+"_"+name+"_meanMR", "Mean of MisRec DSCBshape", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma1     = new RooRealVar (m_name+"_"+name+"_sigma1",  "Sigma1 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma2     = new RooRealVar (m_name+"_"+name+"_sigma2",  "Sigma2 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma3     = new RooRealVar (m_name+"_"+name+"_sigma3",  "Sigma3 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigmaMR    = new RooRealVar (m_name+"_"+name+"_sigmaMR", "Sigma of MisRec DSCBshape", 1e-9, RooNumber::infinity());
        RooRealVar *fraction1   = new RooRealVar (m_name+"_"+name+"_fraction1",   "Fraction1 of DoubleGaussian",  0., 1.);
        RooRealVar *fraction2   = new RooRealVar (m_name+"_"+name+"_fraction2",   "Fraction2 of DoubleGaussian",  0., 1.);
        RooRealVar *fractionMR = new RooRealVar (m_name+"_"+name+"_fractionMR", "Fraction of MisRec Gaussian", 0., 1.);
        RooGaussian *gauss1    = new RooGaussian(m_name+"_"+name+"_gauss1",  "Gaussian 1",      *m_observables->get(MASS), *mean,   *sigma1);
        RooGaussian *gauss2    = new RooGaussian(m_name+"_"+name+"_gauss2",  "Gaussian 2",      *m_observables->get(MASS), *mean,   *sigma2);
        RooGaussian *gauss3    = new RooGaussian(m_name+"_"+name+"_gauss3",  "Gaussian 3",      *m_observables->get(MASS), *mean,   *sigma3);
        RooRealVar *alphaLMR    = new RooRealVar (m_name+"_"+name+"_alphaLMR",   "alphaL of MisRec DSCBshape",        -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *alphaRMR    = new RooRealVar (m_name+"_"+name+"_alphaRMR",   "alphaR of MisRec DSCBshape",        -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *nMR    = new RooRealVar (m_name+"_"+name+"_nMR",   "n of MisRec DSCBshape",        -RooNumber::infinity(), RooNumber::infinity());
        RooDSCB *crystalBallMR = new RooDSCB(m_name+"_"+name+"_crystalBallMR", "MisRec DSCBshape", *m_observables->get(MASS), *meanMR, *sigmaMR, *alphaLMR,*alphaRMR, *nMR);
        
        RooAddPdf *gaussSignal = new RooAddPdf  (m_name+"_"+name+"_gaussSignal", "Mass Signal DoubleGaussian", RooArgList(*gauss1, *gauss2, *gauss3), RooArgList(*fraction1,*fraction2), kTRUE);
        m_massSignalCorePDF    = new RooAddPdf  (m_name+"_"+name, "Mass Signal DoubleGaussian and MisRec Gaussian", RooArgList(*gaussSignal,*crystalBallMR), *fractionMR, kTRUE);
    }else  if ( pdf == "CBshape" ) {
        RooRealVar *mean     = new RooRealVar (m_name+"_"+name+"_mean", "Mean of CBshape", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma    = new RooRealVar (m_name+"_"+name+"_sigma", "Sigma of CBshape", 1e-9, RooNumber::infinity());
        RooRealVar *alpha    = new RooRealVar (m_name+"_"+name+"_alpha",   "alpha of CBshape",        -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *n        = new RooRealVar (m_name+"_"+name+"_n",   "n of CBshape",        -RooNumber::infinity(), RooNumber::infinity());
        m_massSignalCorePDF    = new RooCBShape(m_name+"_"+name, "Mass Signal CBshape", *m_observables->get(MASS), *mean, *sigma, *alpha, *n);
    } else  if ( pdf == "DSCBshape" ) {
        RooRealVar *mean     = new RooRealVar (m_name+"_"+name+"_mean", "Mean of DSCBshape", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma    = new RooRealVar (m_name+"_"+name+"_sigma", "Sigma of DSCBshape", 1e-9, RooNumber::infinity());
        RooRealVar *alphaL    = new RooRealVar (m_name+"_"+name+"_alphaL",   "alphaL of DSCBshape",        -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *alphaR    = new RooRealVar (m_name+"_"+name+"_alphaR",   "alphaR of DSCBshape",        -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *n        = new RooRealVar (m_name+"_"+name+"_n",   "n of DSCBshape",        -RooNumber::infinity(), RooNumber::infinity());
        m_massSignalCorePDF    = new RooDSCB(m_name+"_"+name, "Mass Signal DSCBshape", *m_observables->get(MASS), *mean, *sigma, *alphaL, *alphaR, *n);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_massSignal[0] = name;
    m_massSignal[1] = pdf;
}

void ModelPDF::setMassResolutionPDF(const TString &name, const TString &pdf) {
    if ( pdf == "gauss" ) {
        RooRealVar *mean      = new RooRealVar   (m_name+"_"+name+"_mean", "Mean of Gaussian", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma     = new RooRealVar   (m_name+"_"+name+"_sigma", "Sigma of Gaussian", 1e-9, RooNumber::infinity());
        m_massResolutionPDF   = new RooGaussModel(m_name+"_"+name, "Mass Signal Gaussian", *m_observables->get(MASS), *mean, *sigma);
    } else if ( pdf == "doubleGauss" ) {
        RooRealVar *mean     = new RooRealVar (m_name+"_"+name+"_mean", "Mean of Gaussian", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma1   = new RooRealVar (m_name+"_"+name+"_sigma1", "Sigma1 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma2   = new RooRealVar (m_name+"_"+name+"_sigma2", "Sigma2 of DoubleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *fraction = new RooRealVar (m_name+"_"+name+"_fraction", "Fraction of DoubleGaussian", 0., 1.);
        RooGaussian *gauss1  = new RooGaussian(m_name+"_"+name+"_gauss1", "Gaussian 1", *m_observables->get(MASS), *mean, *sigma1);
        RooGaussian *gauss2  = new RooGaussian(m_name+"_"+name+"_gauss2", "Gaussian 2", *m_observables->get(MASS), *mean, *sigma2);
        m_massResolutionPDF  = new RooAddModel(m_name+"_"+name, "Mass Signal DoubleGaussian", RooArgList(*gauss1, *gauss2), *fraction);
    } else if ( pdf == "tripleGauss" ) {
        RooRealVar *mean      = new RooRealVar (m_name+"_"+name+"_mean", "Mean of Gaussian", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *sigma1    = new RooRealVar (m_name+"_"+name+"_sigma1", "Sigma1 of TripleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma2    = new RooRealVar (m_name+"_"+name+"_sigma2", "Sigma2 of TripleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *sigma3    = new RooRealVar (m_name+"_"+name+"_sigma3", "Sigma3 of TripleGaussian", 1e-9, RooNumber::infinity());
        RooRealVar *fraction1 = new RooRealVar (m_name+"_"+name+"_fraction1", "Fraction1 of TripleGaussian", 0., 1.);
        RooRealVar *fraction2 = new RooRealVar (m_name+"_"+name+"_fraction2", "Fraction2 of TripleGaussian", 0., 1.);
        RooGaussian *gauss1   = new RooGaussian(m_name+"_"+name+"_gauss1", "Gaussian 1", *m_observables->get(MASS), *mean, *sigma1);
        RooGaussian *gauss2   = new RooGaussian(m_name+"_"+name+"_gauss2", "Gaussian 2", *m_observables->get(MASS), *mean, *sigma2);
        RooGaussian *gauss3   = new RooGaussian(m_name+"_"+name+"_gauss3", "Gaussian 3", *m_observables->get(MASS), *mean, *sigma3);
        m_massResolutionPDF   = new RooAddModel(m_name+"_"+name, "Mass Signal TripleGaussian", RooArgList(*gauss1, *gauss2, *gauss3), RooArgList(*fraction1, *fraction2), kTRUE);
    } else if ( pdf == "gaussPCE" ) {
        RooRealVar *mean        = new RooRealVar   (m_name+"_"+name+"_mean", "Mean (0.0) of Mass Resolution", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *scaleFactor = new RooRealVar   (m_name+"_"+name+"_scaleFactor", "SF of Mass Error", 1., 1e-9, RooNumber::infinity());
        m_massResolutionPDF     = new RooGaussModel(m_name+"_"+name, "Mass Resolution Gaussian (for both Sig and Bck)", *m_observables->get(MASS), *mean, *scaleFactor, *m_observables->get(MASSERROR));
        m_conditionalObservables.add(*m_observables->get(MASSERROR));
        m_useProjWDataMass = true;
        // m_massResolutionPDFs.add(*m_massResolutionPDF);
        m_massSignalPDFs.add(*m_massResolutionPDF);
    } else if ( pdf == "delta" ) {
        m_massResolutionPDF = new RooTruthModel(m_name+"_"+name, "Mass Resolution Delta (for both Sig and Bck)", *m_observables->get(MASS));
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_massResolution[0] = name;
    m_massResolution[1] = pdf;
}

void ModelPDF::setMassBackgroundPDF(const TString &name, const TString &pdf) {
    if ( pdf == "exponentialAndConstant" ) {
        RooRealVar *slope           = new RooRealVar    (m_name+"_"+name+"_slope", "Slope of Exponential", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *fraction        = new RooRealVar    (m_name+"_"+name+"_fraction", "Fraction of Exponential", 0., 1.);
        RooPolynomial *constant     = new RooPolynomial (m_name+"_"+name+"_constant", "Constant", *m_observables->get(MASS));
        RooExponential *exponential = new RooExponential(m_name+"_"+name+"_exponential", "Exponential", *m_observables->get(MASS), *slope);
        m_massBackgroundPDF         = new RooAddPdf     (m_name+"_"+name, "Mass Background Exponential+Constant", RooArgList(*exponential, *constant), *fraction);
        m_massBackgroundPDFs.add(*exponential);
        m_massBackgroundPDFs.add(*constant);
        m_massBackgroundFracs.add(*fraction);
    } else if ( pdf == "linear" ) {
        RooRealVar *shape           = new RooRealVar    (m_name+"_"+name+"_shape", "Shape of Linear", -RooNumber::infinity(), RooNumber::infinity());
        m_massBackgroundPDF         = new RooPolynomial (m_name+"_"+name, "Mass Background Linear", *m_observables->get(MASS), *shape);
        m_massBackgroundPDFs.add(*m_massBackgroundPDF);
    }else if ( pdf == "exponential" ) {
        RooRealVar *slope           = new RooRealVar    (m_name+"_"+name+"_slope", "Slope of Exponential", -RooNumber::infinity(), RooNumber::infinity());
        m_massBackgroundPDF = new RooExponential(m_name+"_"+name, "Exponential", *m_observables->get(MASS), *slope);
        m_massBackgroundPDFs.add(*m_massBackgroundPDF);
    } else if ( pdf == "linearAndTanh" ) {
        RooRealVar *shape           = new RooRealVar    (m_name+"_"+name+"_shape", "Shape of Linear", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *scale           = new RooRealVar    (m_name+"_"+name+"_scale", "Scale of Tanh", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *offset          = new RooRealVar    (m_name+"_"+name+"_offset", "Offset of Tanh", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *fraction        = new RooRealVar    (m_name+"_"+name+"_fraction", "Fraction of Linear", 0., 1.);
        RooPolynomial *linear       = new RooPolynomial (m_name+"_"+name+"_linear", "Linear", *m_observables->get(MASS), *shape);
        RooTanhPdf *tanh            = new RooTanhPdf    (m_name+"_"+name+"_tanh", "Tanh", *m_observables->get(MASS), *scale, *offset);
        m_massBackgroundPDF         = new RooAddPdf     (m_name+"_"+name, "Mass Background Linear+Tanh", RooArgList(*linear, *tanh), *fraction);
        m_massBackgroundPDFs.add(*linear);
        m_massBackgroundPDFs.add(*tanh);
        m_massBackgroundFracs.add(*fraction);
    } else if ( pdf == "exponentialAndTanh" ) {
        RooRealVar *slope           = new RooRealVar    (m_name+"_"+name+"_slope", "Slope of Exponential", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *scale           = new RooRealVar    (m_name+"_"+name+"_scale", "Scale of Tanh", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *offset          = new RooRealVar    (m_name+"_"+name+"_offset", "Offset of Tanh", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *fraction        = new RooRealVar    (m_name+"_"+name+"_fraction", "Fraction of Exponential", 0., 1.);
        RooExponential *exponential = new RooExponential(m_name+"_"+name+"_exponential", "Exponential", *m_observables->get(MASS), *slope);
        RooTanhPdf *tanh            = new RooTanhPdf    (m_name+"_"+name+"_tanh", "Tanh", *m_observables->get(MASS), *scale, *offset);
        m_massBackgroundPDF         = new RooAddPdf     (m_name+"_"+name, "Mass Background Expo+Tanh", RooArgList(*exponential, *tanh), *fraction, kTRUE);
        m_massBackgroundPDFs.add(*exponential);
        m_massBackgroundPDFs.add(*tanh);
        m_massBackgroundFracs.add(*fraction);
    }else if ( pdf == "exponentialConstantAndTanh" ) {
        RooRealVar *slope           = new RooRealVar    (m_name+"_"+name+"_slope", "Slope of Exponential", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *scale           = new RooRealVar    (m_name+"_"+name+"_scale", "Scale of Tanh", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *offset          = new RooRealVar    (m_name+"_"+name+"_offset", "Offset of Tanh", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *fraction1       = new RooRealVar    (m_name+"_"+name+"_fraction1", "Fraction of Exponential", 0., 1.);
        RooRealVar *fraction2       = new RooRealVar    (m_name+"_"+name+"_fraction2", "Fraction to Tanh", 0., 1.);
        RooExponential *exponential = new RooExponential(m_name+"_"+name+"_exponential", "Exponential", *m_observables->get(MASS), *slope);
        RooPolynomial *constant     = new RooPolynomial (m_name+"_"+name+"_constant", "Constant", *m_observables->get(MASS));
        RooTanhPdf *tanh            = new RooTanhPdf    (m_name+"_"+name+"_tanh", "Tanh", *m_observables->get(MASS), *scale, *offset);
        m_massBackgroundPDF         = new RooAddPdf     (m_name+"_"+name, "Mass Background Expo+Tanh", RooArgList(*exponential, *constant, *tanh), RooArgList(*fraction1,*fraction2), kTRUE);
        m_massBackgroundPDFs.add(*exponential);
        m_massBackgroundPDFs.add(*constant);
        m_massBackgroundPDFs.add(*tanh);
        m_massBackgroundFracs.add(*fraction1);
        m_massBackgroundFracs.add(*fraction2);
    } else if ( pdf == "expPol" ) {
        RooRealVar *mass0   = new RooRealVar(m_name+"_"+name+"_mass0", "Mass0 of ExpPol", 0., RooNumber::infinity());
        RooRealVar *norm    = new RooRealVar(m_name+"_"+name+"_norm", "Norm of ExpPol", 0., RooNumber::infinity());
        RooRealVar *a       = new RooRealVar(m_name+"_"+name+"_a", "A of ExpPol", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *p1      = new RooRealVar(m_name+"_"+name+"_p1", "P1 of ExpPol", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *p2      = new RooRealVar(m_name+"_"+name+"_p2", "P2 of ExpPol", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *p3      = new RooRealVar(m_name+"_"+name+"_p3", "P3 of ExpPol", -RooNumber::infinity(), RooNumber::infinity());
        m_massBackgroundPDF = new RooExpPol (m_name+"_"+name, "ExpPol", *m_observables->get(MASS), *mass0, *norm, *a, *p1, *p2, *p3);
        m_massBackgroundPDFs.add(*m_massBackgroundPDF);
    } else if ( pdf == "rooAtlas" ) {
        RooRealVar *mass0   = new RooRealVar(m_name+"_"+name+"_mass0", "Mass0 of Atlas", 0., RooNumber::infinity());
        RooRealVar *norm    = new RooRealVar(m_name+"_"+name+"_norm", "Norm of Atlas", 0., RooNumber::infinity());
        RooRealVar *a       = new RooRealVar(m_name+"_"+name+"_a", "A of Atlas", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *p0      = new RooRealVar(m_name+"_"+name+"_p0", "P0 of Atlas", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *p1      = new RooRealVar(m_name+"_"+name+"_p1", "P1 of Atlas", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *p2      = new RooRealVar(m_name+"_"+name+"_p2", "P2 of Atlas", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *p3      = new RooRealVar(m_name+"_"+name+"_p3", "P3 of Atlas", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *p4      = new RooRealVar(m_name+"_"+name+"_p4", "P4 of Atlas", -RooNumber::infinity(), RooNumber::infinity());
        m_massBackgroundPDF = new RooATLAS  (m_name+"_"+name, "RooAtlas", *m_observables->get(MASS), *mass0, *norm, *a, *p4, *p3, *p2, *p1, *p0);
        m_massBackgroundPDFs.add(*m_massBackgroundPDF);
    } else if ( pdf.Contains("chebychev") ) {
        TString s1(pdf);
        TPRegexp("chebychev\\((\\d{1})\\)").Substitute(s1, "$1");
        string s2(s1);
        Int_t order = atoi(s2.c_str());
        if ( order > 7) {
            cout << "ERROR: Chebychev > 7" << endl;
            gApplication->Terminate();
        }
        RooRealVar *p[7];
        RooArgList pList;
        for ( Int_t i = 0; i < order; i++ ) {
            TString iStr = "";
            iStr += (i+1);
            p[i] = new RooRealVar(m_name+"_"+name+"_p"+iStr, "P"+iStr+" of Chebychev", -RooNumber::infinity(), RooNumber::infinity());
            pList.add(*p[i]);
        }
        m_massBackgroundPDF = new RooChebychev(m_name+"_"+name, "Chebychev Polynomial ("+s1+")", *m_observables->get(MASS), pList);
        m_massBackgroundPDFs.add(*m_massBackgroundPDF);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_massBackground[0] = name;
    m_massBackground[1] = pdf;
}

void ModelPDF::setTimeSignalPDF(const TString &name, const TString &pdf) {
    if ( pdf == "singleExponential" ) {
        if ( m_timeResolutionPDF == nullptr ) {
            RooRealVar *mean        = new RooRealVar   (m_name+"_"+name+"_mean", "Mean (0.0) of Time Resolution", 0.);
            RooRealVar *scaleFactor = new RooRealVar   (m_name+"_"+name+"_scaleFactor", "SF of Time Error", 1., 0.1, 10.);
            m_timeResolutionPDF     = new RooGaussModel(m_name+"_"+name+"_resolution", "Lifetime Resolution Gaussian (for both Sig and Bck)", *m_observables->get(TIME), *mean, *scaleFactor, *m_observables->get(TIMEERROR));
            m_conditionalObservables.add(*m_observables->get(TIMEERROR));
            m_useProjWDataTime = true;
        }
        RooRealVar *tau = new RooRealVar(m_name+"_"+name+"_tau", "Lifetime Signal Tau", -RooNumber::infinity(), RooNumber::infinity());
        m_timeSignalPDF = new RooDecay  (m_name+"_"+name+"_decay", "Lifetime Signal Decay", *m_observables->get(TIME), *tau, *m_timeResolutionPDF, RooDecay::SingleSided);
        m_timeSignalPDFs.add(*m_timeSignalPDF);
    } else if ( pdf == "singleExponentialMC" ) {
        if ( m_timeResolutionPDF == nullptr ) {
            m_timeResolutionPDF = new RooTruthModel(m_name+"_"+name+"_resolution", "Lifetime Resolution Delta (for both Sig and Bck)", *m_observables->get(TIME));
        }
        RooRealVar *tau = new RooRealVar(m_name+"_"+name+"_tau", "Lifetime Signal Tau", -RooNumber::infinity(), RooNumber::infinity());
        m_timeSignalPDF = new RooDecay  (m_name+"_"+name+"_decay", "Lifetime Signal Decay", *m_observables->get(TIME), *tau, *m_timeResolutionPDF, RooDecay::SingleSided);
    } else if ( pdf == "doubleExponentialMC" ) {
        if ( m_timeResolutionPDF == nullptr ) {
            m_timeResolutionPDF = new RooTruthModel(m_name+"_"+name+"_resolution", "Lifetime Resolution Delta (for both Sig and Bck)", *m_observables->get(TIME));
        }
        RooRealVar *tau1     = new RooRealVar(m_name+"_"+name+"_tau1", "Lifetime Signal Tau 1", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *tau2     = new RooRealVar(m_name+"_"+name+"_tau2", "Lifetime Signal Tau 2", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *fraction = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of Expo", 0., 1.);
        RooDecay *decay1     = new RooDecay  (m_name+"_"+name+"_decay1", "Lifetime Signal Decay 1", *m_observables->get(TIME), *tau1, *m_timeResolutionPDF, RooDecay::SingleSided);
        RooDecay *decay2     = new RooDecay  (m_name+"_"+name+"_decay2", "Lifetime Signal Decay 2", *m_observables->get(TIME), *tau2, *m_timeResolutionPDF, RooDecay::SingleSided);
        m_timeSignalPDF      = new RooAddPdf (m_name+"_"+name+"_total", "Lifetime Signal Double", RooArgList(*decay1, *decay2), *fraction);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_timeSignal[0] = name;
    m_timeSignal[1] = pdf;
}

void ModelPDF::setTimeBackgroundPDF(const TString &name, const TString &pdf) {
    if ( pdf == "promptExponentialPosNeg" ) {
        if ( m_timeResolutionPDF == nullptr ) {
            RooRealVar *mean        = new RooRealVar   (m_name+"_"+name+"_mean", "Mean (0.0) of Time Resolution", 0.);
            RooRealVar *scaleFactor = new RooRealVar   (m_name+"_"+name+"_scaleFactor", "SF of Time Error", 1., 0.1, 10.);
            m_timeResolutionPDF     = new RooGaussModel(m_name+"_"+name+"_resolution", "Lifetime Resolution Gaussian (for both Sig and Bck)", *m_observables->get(TIME), *mean, *scaleFactor, *m_observables->get(TIMEERROR));
            m_conditionalObservables.add(*m_observables->get(TIMEERROR));
            m_useProjWDataTime = true;
        }
        RooRealVar *tauPos      = new RooRealVar(m_name+"_"+name+"_tauPos", "Bck Tau Pos", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *tauNeg      = new RooRealVar(m_name+"_"+name+"_tauNeg", "Bck Tau Neg", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *fractionPos = new RooRealVar(m_name+"_"+name+"_fractionPos", "Fraction of PositiveExpo", 0., 1.);
        RooRealVar *fractionNeg = new RooRealVar(m_name+"_"+name+"_fractionNeg", "Fraction of NegativeExpo", 0., 1.);
        RooDecay *decayPos      = new RooDecay  (m_name+"_"+name+"_decayPos", "Decay Pos", *m_observables->get(TIME), *tauPos, *m_timeResolutionPDF, RooDecay::SingleSided);
        RooDecay *decayNeg      = new RooDecay  (m_name+"_"+name+"_decayNeg", "Decay Neg", *m_observables->get(TIME), *tauNeg,  *m_timeResolutionPDF, RooDecay::Flipped);
        m_timeBackgroundPDF     = new RooAddPdf (m_name+"_"+name+"_total", "Lifetime Background (Prompt+NegativeExpo+PositiveExpo)", RooArgList(*decayPos, *decayNeg, *m_timeResolutionPDF), RooArgList(*fractionPos, *fractionNeg), kTRUE);
        m_timeBackgroundPDFs.add(*decayPos);
        m_timeBackgroundPDFs.add(*decayNeg);
        m_timeBackgroundPDFs.add(*m_timeResolutionPDF);
        m_timeBackgroundFracs.add(*fractionPos);
        m_timeBackgroundFracs.add(*fractionNeg);
    } else if ( pdf == "promptExponential2PosNeg" ) {
        if ( m_timeResolutionPDF == nullptr ) {
            RooRealVar *mean        = new RooRealVar   (m_name+"_"+name+"_mean", "Mean (0.0) of Time Resolution", 0.);
            RooRealVar *scaleFactor = new RooRealVar   (m_name+"_"+name+"_scaleFactor", "SF of Time Error", 1., 0.1, 10.);
            m_timeResolutionPDF     = new RooGaussModel(m_name+"_"+name+"_resolution", "Lifetime Resolution Gaussian (for both Sig and Bck)", *m_observables->get(TIME), *mean, *scaleFactor, *m_observables->get(TIMEERROR));
            m_conditionalObservables.add(*m_observables->get(TIMEERROR));
            m_useProjWDataTime = true;
        }
        RooRealVar *tauPos1      = new RooRealVar(m_name+"_"+name+"_tauPos1", "Bck Tau Pos 1", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *tauPos2      = new RooRealVar(m_name+"_"+name+"_tauPos2", "Bck Tau Pos 2", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *tauNeg       = new RooRealVar(m_name+"_"+name+"_tauNeg",  "Bck Tau Neg",   -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *fractionPos1 = new RooRealVar(m_name+"_"+name+"_fractionPos1", "Fraction of PositiveExpo1", 0., 1.);
        RooRealVar *fractionPos2 = new RooRealVar(m_name+"_"+name+"_fractionPos2", "Fraction of PositiveExpo2", 0., 1.);
        RooRealVar *fractionNeg  = new RooRealVar(m_name+"_"+name+"_fractionNeg",  "Fraction of NegativeExpo",  0., 1.);
        RooDecay *decayPos1      = new RooDecay  (m_name+"_"+name+"_decayPos1", "Decay Pos 1", *m_observables->get(TIME), *tauPos1, *m_timeResolutionPDF, RooDecay::SingleSided);
        RooDecay *decayPos2      = new RooDecay  (m_name+"_"+name+"_decayPos2", "Decay Pos 2", *m_observables->get(TIME), *tauPos2, *m_timeResolutionPDF, RooDecay::SingleSided);
        RooDecay *decayNeg       = new RooDecay  (m_name+"_"+name+"_decayNeg",  "Decay Neg",   *m_observables->get(TIME), *tauNeg,  *m_timeResolutionPDF, RooDecay::Flipped);
        m_timeBackgroundPDF      = new RooAddPdf (m_name+"_"+name+"_total", "Lifetime Background (Prompt+NegativeExpo+2PositiveExpo)", RooArgList(*decayPos1, *decayPos2, *decayNeg, *m_timeResolutionPDF), RooArgList(*fractionPos1, *fractionPos2, *fractionNeg), kTRUE);
        m_timeBackgroundPDFs.add(*decayPos1);
        m_timeBackgroundPDFs.add(*decayPos2);
        m_timeBackgroundPDFs.add(*decayNeg);
        m_timeBackgroundPDFs.add(*m_timeResolutionPDF);
        m_timeBackgroundFracs.add(*fractionPos1);
        m_timeBackgroundFracs.add(*fractionPos2);
        m_timeBackgroundFracs.add(*fractionNeg);
    } else if ( pdf == "promptExponential2PosDS" ) {
        if ( m_timeResolutionPDF == nullptr ) {
            RooRealVar *mean        = new RooRealVar   (m_name+"_"+name+"_mean", "Mean (0.0) of Time Resolution", 0.);
            RooRealVar *scaleFactor = new RooRealVar   (m_name+"_"+name+"_scaleFactor", "SF of Time Error", 1., 0.1, 10.);
            m_timeResolutionPDF     = new RooGaussModel(m_name+"_"+name+"_resolution", "Lifetime Resolution Gaussian (for both Sig and Bck)", *m_observables->get(TIME), *mean, *scaleFactor, *m_observables->get(TIMEERROR));
            m_conditionalObservables.add(*m_observables->get(TIMEERROR));
            m_useProjWDataTime = true;
        }
        RooRealVar *tauPos1      = new RooRealVar(m_name+"_"+name+"_tauPos1", "Bck Tau Pos 1", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *tauPos2      = new RooRealVar(m_name+"_"+name+"_tauPos2", "Bck Tau Pos 2", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *tauDS        = new RooRealVar(m_name+"_"+name+"_tauDS",   "Bck Tau DS",    -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *fractionPos1 = new RooRealVar(m_name+"_"+name+"_fractionPos1", "Fraction of PositiveExpo1", 0., 1.);
        RooRealVar *fractionPos2 = new RooRealVar(m_name+"_"+name+"_fractionPos2", "Fraction of PositiveExpo2", 0., 1.);
        RooRealVar *fractionDS   = new RooRealVar(m_name+"_"+name+"_fractionDS",   "Fraction of DSExpo",  0., 1.);
        RooDecay *decayPos1      = new RooDecay  (m_name+"_"+name+"_decayPos1", "Decay Pos 1", *m_observables->get(TIME), *tauPos1, *m_timeResolutionPDF, RooDecay::SingleSided);
        RooDecay *decayPos2      = new RooDecay  (m_name+"_"+name+"_decayPos2", "Decay Pos 2", *m_observables->get(TIME), *tauPos2, *m_timeResolutionPDF, RooDecay::SingleSided);
        RooDecay *decayDS        = new RooDecay  (m_name+"_"+name+"_decayDS",   "Decay DS",   *m_observables->get(TIME), *tauDS,  *m_timeResolutionPDF, RooDecay::DoubleSided);
        m_timeBackgroundPDF      = new RooAddPdf (m_name+"_"+name+"_total", "Lifetime Background (Prompt+DSExpo+2PositiveExpo)", RooArgList(*decayPos1, *decayPos2, *decayDS, *m_timeResolutionPDF), RooArgList(*fractionPos1, *fractionPos2, *fractionDS), kTRUE);
        m_timeBackgroundPDFs.add(*decayPos1);
        m_timeBackgroundPDFs.add(*decayPos2);
        m_timeBackgroundPDFs.add(*decayDS);
        m_timeBackgroundPDFs.add(*m_timeResolutionPDF);
        m_timeBackgroundFracs.add(*fractionPos1);
        m_timeBackgroundFracs.add(*fractionPos2);
        m_timeBackgroundFracs.add(*fractionDS);
    } else if ( pdf == "promptExponentialPosDS" ) {
        if ( m_timeResolutionPDF == nullptr ) {
            RooRealVar *mean        = new RooRealVar   (m_name+"_"+name+"_mean", "Mean (0.0) of Time Resolution", 0.);
            RooRealVar *scaleFactor = new RooRealVar   (m_name+"_"+name+"_scaleFactor", "SF of Time Error", 1., 0.1, 10.);
            m_timeResolutionPDF     = new RooGaussModel(m_name+"_"+name+"_resolution", "Lifetime Resolution Gaussian (for both Sig and Bck)", *m_observables->get(TIME), *mean, *scaleFactor, *m_observables->get(TIMEERROR));
            m_conditionalObservables.add(*m_observables->get(TIMEERROR));
            m_useProjWDataTime = true;
        }
        RooRealVar *tauPos      = new RooRealVar(m_name+"_"+name+"_tauPos", "Bck Tau Pos", -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *tauDS       = new RooRealVar(m_name+"_"+name+"_tauDS",  "Bck Tau DS",  -RooNumber::infinity(), RooNumber::infinity());
        RooRealVar *fractionPos = new RooRealVar(m_name+"_"+name+"_fractionPos", "Fraction of PositiveExpo", 0., 1.);
        RooRealVar *fractionDS  = new RooRealVar(m_name+"_"+name+"_fractionDS",  "Fraction of DSExpo",       0., 1.);
        RooDecay *decayPos      = new RooDecay  (m_name+"_"+name+"_decayPos", "Decay Pos", *m_observables->get(TIME), *tauPos, *m_timeResolutionPDF, RooDecay::SingleSided);
        RooDecay *decayDS       = new RooDecay  (m_name+"_"+name+"_decayDS",  "Decay DS",  *m_observables->get(TIME), *tauDS,  *m_timeResolutionPDF, RooDecay::DoubleSided);
        m_timeBackgroundPDF     = new RooAddPdf (m_name+"_"+name+"_total", "Lifetime Background (Prompt+DSExpo+PositiveExpo)", RooArgList(*decayPos, *decayDS, *m_timeResolutionPDF), RooArgList(*fractionPos, *fractionDS), kTRUE);
        m_timeBackgroundPDFs.add(*decayPos);
        m_timeBackgroundPDFs.add(*decayDS);
        m_timeBackgroundPDFs.add(*m_timeResolutionPDF);
        m_timeBackgroundFracs.add(*fractionPos);
        m_timeBackgroundFracs.add(*fractionDS);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_timeBackground[0] = name;
    m_timeBackground[1] = pdf;
}

void ModelPDF::setMassSignalErrorPDF(const TString &name, const TString &pdf) {
    if ( pdf == "1Gamma" ) {
        RooRealVar *a1           = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1           = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1           = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        m_massSignalErrorPDF  = new Roo3Gamma (m_name+"_"+name, "Mass Signal Error 1Gamma", *m_observables->get(MASSERROR), *a1, *b1, *c1, (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1));
    } else if ( pdf == "2Gamma" ) {
        RooRealVar *a1           = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1           = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1           = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2           = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2           = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2           = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *fraction     = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 2 Gamma", 0., 1.);
        m_massSignalErrorPDF  = new Roo3Gamma (m_name+"_"+name, "Mass Signal Error 2Gamma", *m_observables->get(MASSERROR), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1));
    } else if ( pdf == "3Gamma" ) {
        RooRealVar *a1        = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1        = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1        = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2        = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2        = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2        = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *a3        = new RooRealVar(m_name+"_"+name+"_a3", "a3", 0., RooNumber::infinity());
        RooRealVar *b3        = new RooRealVar(m_name+"_"+name+"_b3", "b3", 0., RooNumber::infinity());
        RooRealVar *c3        = new RooRealVar(m_name+"_"+name+"_c3", "c3", 0., RooNumber::infinity());
        RooRealVar *fraction  = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 3 Gamma", 0., 1.);
        RooRealVar *fraction2 = new RooRealVar(m_name+"_"+name+"_fraction2", "Fraction2 of 3 Gamma", 0., 1.);
        m_massSignalErrorPDF  = new Roo3Gamma (m_name+"_"+name, "Mass Signal Error 3Gamma", *m_observables->get(MASSERROR), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, *fraction2, *a3, *b3, *c3);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_massSignalError[0] = name;
    m_massSignalError[1] = pdf;
}

void ModelPDF::setMassBackgroundErrorPDF(const TString &name, const TString &pdf) {
    if ( pdf == "1Gamma" ) {
        RooRealVar *a1            = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1            = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1            = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        m_massBackgroundErrorPDF  = new Roo3Gamma (m_name+"_"+name, "Mass Background Error 1Gamma", *m_observables->get(MASSERROR), *a1, *b1, *c1, (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1));
    } else if ( pdf == "2Gamma" ) {
        RooRealVar *a1           = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1           = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1           = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2           = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2           = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2           = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *fraction     = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 2 Gamma", 0., 1.);
        m_massBackgroundErrorPDF = new Roo3Gamma (m_name+"_"+name, "Mass Background Error 2Gamma", *m_observables->get(MASSERROR), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1));
    } else if ( pdf == "3Gamma" ) {
        RooRealVar *a1           = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1           = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1           = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2           = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2           = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2           = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *a3           = new RooRealVar(m_name+"_"+name+"_a3", "a3", 0., RooNumber::infinity());
        RooRealVar *b3           = new RooRealVar(m_name+"_"+name+"_b3", "b3", 0., RooNumber::infinity());
        RooRealVar *c3           = new RooRealVar(m_name+"_"+name+"_c3", "c3", 0., RooNumber::infinity());
        RooRealVar *fraction     = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 3 Gamma", 0., 1.);
        RooRealVar *fraction2    = new RooRealVar(m_name+"_"+name+"_fraction2", "Fraction2 of 3 Gamma", 0., 1.);
        m_massBackgroundErrorPDF = new Roo3Gamma (m_name+"_"+name, "Mass Background Error 3Gamma", *m_observables->get(MASSERROR), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, *fraction2, *a3, *b3, *c3);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_massBackgroundError[0] = name;
    m_massBackgroundError[1] = pdf;
}

void ModelPDF::setTimeSignalErrorPDF(const TString &name, const TString &pdf) {
    if ( pdf == "1Gamma" ) {
        RooRealVar *a1        = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1        = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1        = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        m_timeSignalErrorPDF  = new Roo3Gamma (m_name+"_"+name, "Lifetime Signal Error 1Gamma", *m_observables->get(TIMEERROR), *a1, *b1, *c1, (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1));
    } else if ( pdf == "2Gamma" ) {
        RooRealVar *a1        = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1        = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1        = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2        = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2        = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2        = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *fraction  = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 2 Gamma", 0., 1.);
        m_timeSignalErrorPDF  = new Roo3Gamma (m_name+"_"+name, "Lifetime Signal Error 2Gamma", *m_observables->get(TIMEERROR), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1));
    } else if ( pdf == "3Gamma" ) {
        RooRealVar *a1        = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1        = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1        = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2        = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2        = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2        = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *a3        = new RooRealVar(m_name+"_"+name+"_a3", "a3", 0., RooNumber::infinity());
        RooRealVar *b3        = new RooRealVar(m_name+"_"+name+"_b3", "b3", 0., RooNumber::infinity());
        RooRealVar *c3        = new RooRealVar(m_name+"_"+name+"_c3", "c3", 0., RooNumber::infinity());
        RooRealVar *fraction  = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 3 Gamma", 0., 1.);
        RooRealVar *fraction2 = new RooRealVar(m_name+"_"+name+"_fraction2", "Fraction2 of 3 Gamma", 0., 1.);
        m_timeSignalErrorPDF  = new Roo3Gamma (m_name+"_"+name, "Lifetime Signal Error 3Gamma", *m_observables->get(TIMEERROR), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, *fraction2, *a3, *b3, *c3);
    }else if ( pdf == "2Lognormal" ) {
        RooRealVar *mean1           = new RooRealVar(m_name+"_"+name+"_mean1", "mean1", 0., RooNumber::infinity());
        RooRealVar *sigma1        = new RooRealVar(m_name+"_"+name+"_sigma1", "sigma1", 0., RooNumber::infinity());
        RooRealVar *mean2           = new RooRealVar(m_name+"_"+name+"_mean2", "mean2", 0., RooNumber::infinity());
        RooRealVar *sigma2        = new RooRealVar(m_name+"_"+name+"_sigma2", "sigma2", 0., RooNumber::infinity());
        RooRealVar *fraction     = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of two lognormal distributions", 0., 1.);
        
        m_timeSignalErrorPDF = new Roo2Lognormal (m_name+"_"+name, "Lifetime error signal 2Lognormal", *m_observables->get(TIMEERROR), *mean1, *sigma1, *mean2, *sigma2, *fraction);
    
    } else if ( pdf == "GammaPt" ) {
        RooRealVar *type     = new RooRealVar (m_name+"_"+name+"_type", "Type of 3GammaPt", 2.); // Time Sig
        m_timeSignalErrorPDF = new Roo3GammaPt(m_name+"_"+name, "Lifetime Signal Error 3GammaPt", *m_observables->get(TIMEERROR), *type, *m_observables->get(PT));
    } else if ( pdf.Contains("LognormalPt") ) {
        TString config(pdf);
        TPRegexp("LognormalPt\\((.*)\\)").Substitute(config, "$1");
        m_timeSignalErrorPDF = new Roo2LognormalPt(m_name+"_"+name, "Lifetime Signal Error 2LognormalPt", *m_observables->get(TIMEERROR), *m_observables->get(PT), config);
    } else if ( pdf == "lognormal" ) {
        RooRealVar *mean = new RooRealVar(m_name+"_"+name+"_mean", "mean", 1e-9, RooNumber::infinity());
        RooRealVar *sigma = new RooRealVar(m_name+"_"+name+"_sigma", "sigma", 1e-9, RooNumber::infinity());
        m_timeSignalErrorPDF  = new RooLognormal (m_name+"_"+name, "Lifetime Signal Error Lognormal", *m_observables->get(TIMEERROR), *mean, *sigma);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_timeSignalError[0] = name;
    m_timeSignalError[1] = pdf;
}

void ModelPDF::setTimeBackgroundErrorPDF(const TString &name, const TString &pdf) {
    if ( pdf == "1Gamma" ) {
        RooRealVar *a1           = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1           = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1           = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        m_timeBackgroundErrorPDF = new Roo3Gamma (m_name+"_"+name, "Lifetime Background Error 1Gamma", *m_observables->get(TIMEERROR), *a1, *b1, *c1, (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1));
    } else if ( pdf == "2Gamma" ) {
        RooRealVar *a1           = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1           = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1           = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2           = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2           = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2           = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *fraction     = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 2 Gamma", 0., 1.);
        m_timeBackgroundErrorPDF = new Roo3Gamma (m_name+"_"+name, "Lifetime Background Error 2Gamma", *m_observables->get(TIMEERROR), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1));
    } else if ( pdf == "3Gamma" ) {
        RooRealVar *a1           = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1           = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1           = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2           = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2           = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2           = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *a3           = new RooRealVar(m_name+"_"+name+"_a3", "a3", 0., RooNumber::infinity());
        RooRealVar *b3           = new RooRealVar(m_name+"_"+name+"_b3", "b3", 0., RooNumber::infinity());
        RooRealVar *c3           = new RooRealVar(m_name+"_"+name+"_c3", "c3", 0., RooNumber::infinity());
        RooRealVar *fraction     = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 3 Gamma", 0., 1.);
        RooRealVar *fraction2    = new RooRealVar(m_name+"_"+name+"_fraction2", "Fraction2 of 3 Gamma", 0., 1.);
        m_timeBackgroundErrorPDF = new Roo3Gamma (m_name+"_"+name, "Lifetime Background Error 3Gamma", *m_observables->get(TIMEERROR), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, *fraction2, *a3, *b3, *c3);
    } else if ( pdf == "GammaPt" ) {
        RooRealVar *type         = new RooRealVar (m_name+"_"+name+"_type", "Type of 3GammaPt", 1.); // Time Bck
        m_timeBackgroundErrorPDF = new Roo3GammaPt(m_name+"_"+name, "Lifetime Background Error 3GammaPt", *m_observables->get(TIMEERROR), *type, *m_observables->get(PT));
    } else if ( pdf.Contains("LognormalPt") ) {
        TString config(pdf);
        TPRegexp("LognormalPt\\((.*)\\)").Substitute(config, "$1");
        m_timeBackgroundErrorPDF = new Roo2LognormalPt(m_name+"_"+name, "Lifetime Background Error 2LognormalPt", *m_observables->get(TIMEERROR), *m_observables->get(PT), config);
    } else if ( pdf == "lognormal" ) {
        RooRealVar *mean = new RooRealVar(m_name+"_"+name+"_mean", "mean", 1e-9, RooNumber::infinity());
        RooRealVar *sigma = new RooRealVar(m_name+"_"+name+"_sigma", "sigma", 1e-9, RooNumber::infinity());
        m_timeBackgroundErrorPDF  = new RooLognormal (m_name+"_"+name, "Lifetime Background Error Lognormal", *m_observables->get(TIMEERROR), *mean, *sigma);
    }else if ( pdf == "2Lognormal" ) {
        RooRealVar *mean1           = new RooRealVar(m_name+"_"+name+"_mean1", "mean1", 0., RooNumber::infinity());
        RooRealVar *sigma1        = new RooRealVar(m_name+"_"+name+"_sigma1", "sigma1", 0., RooNumber::infinity());
        RooRealVar *mean2           = new RooRealVar(m_name+"_"+name+"_mean2", "mean2", 0., RooNumber::infinity());
        RooRealVar *sigma2        = new RooRealVar(m_name+"_"+name+"_sigma2", "sigma2", 0., RooNumber::infinity());
        RooRealVar *fraction     = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of two lognormal distributions", 0., 1.);
        
        //RooLognormal* lognormal1 = new RooLognormal (m_name+"_"+name+"_lognormal1", "Lognormal1 distribution", *m_observables->get(TIMEERROR), *mean1, *sigma1);
        //RooLognormal* lognormal2 = new RooLognormal (m_name+"_"+name+"_lognormal2", "Lognormal2 distribution", *m_observables->get(TIMEERROR), *mean2, *sigma2);
        
        //m_timeBackgroundErrorPDF = new RooAddPdf (m_name+"_"+name, "Lifetime error background 2Lognormal", RooArgList(*lognormal1, *lognormal2), *fraction, kTRUE );
        m_timeBackgroundErrorPDF = new Roo2Lognormal (m_name+"_"+name, "Lifetime error background 2Lognormal", *m_observables->get(TIMEERROR), *mean1, *sigma1, *mean2, *sigma2, *fraction);
        
    } else if ( pdf == "2lognormalPt" ) {
        RooRealVar *mean1 = new RooRealVar(m_name+"_"+name+"_mean1", "mean1", 1e-9, RooNumber::infinity());
        RooRealVar *sigma1 = new RooRealVar(m_name+"_"+name+"_sigma1", "sigma1", 1e-9, RooNumber::infinity());
        RooRealVar *mean2 = new RooRealVar(m_name+"_"+name+"_mean2", "mean2", 1e-9, RooNumber::infinity());
        RooRealVar *sigma2 = new RooRealVar(m_name+"_"+name+"_sigma2", "sigma2", 1e-9, RooNumber::infinity());
        RooRealVar *fraction = new RooRealVar (m_name+"_"+name+"_fraction", "Fraction of 2Lognormal", 0., 1.);
        RooLognormal *lognormal1 = new RooLognormal (m_name+"_"+name+"_lognormal1", "Lifetime Background Error Lognormal 1", *m_observables->get(TIMEERROR), *mean1, *sigma1);
        RooLognormal *lognormal2 = new RooLognormal (m_name+"_"+name+"_lognormal2", "Lifetime Background Error Lognormal 2", *m_observables->get(TIMEERROR), *mean2, *sigma2);
        m_timeBackgroundErrorPDF  = new RooAddPdf (m_name+"_"+name, "Lifetime Background Error 2Lognormal", RooArgList(*lognormal1, *lognormal2), *fraction);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_timeBackgroundError[0] = name;
    m_timeBackgroundError[1] = pdf;
}

void ModelPDF::setPtSignalPDF(const TString &name, const TString &pdf) {
    if ( pdf == "1Gamma" ) {
        RooRealVar *a1    = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1    = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1    = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        m_ptSignalPDF     = new Roo3Gamma (m_name+"_"+name, "Pt Signal 1Gamma", *m_observables->get(PT), *a1, *b1, *c1, (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1));
    } else if ( pdf == "2Gamma" ) {
        RooRealVar *a1       = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1       = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1       = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2       = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2       = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2       = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *fraction = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 2 Gamma", 0., 1.);
        m_ptSignalPDF        = new Roo3Gamma (m_name+"_"+name, "Pt Signal 2Gamma", *m_observables->get(PT), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1));
    } else if ( pdf == "3Gamma" ) {
        RooRealVar *a1        = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1        = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1        = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2        = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2        = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2        = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *a3        = new RooRealVar(m_name+"_"+name+"_a3", "a3", 0., RooNumber::infinity());
        RooRealVar *b3        = new RooRealVar(m_name+"_"+name+"_b3", "b3", 0., RooNumber::infinity());
        RooRealVar *c3        = new RooRealVar(m_name+"_"+name+"_c3", "c3", 0., RooNumber::infinity());
        RooRealVar *fraction  = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 3 Gamma", 0., 1.);
        RooRealVar *fraction2 = new RooRealVar(m_name+"_"+name+"_fraction2", "Fraction2 of 3 Gamma", 0., 1.);
        m_ptSignalPDF         = new Roo3Gamma (m_name+"_"+name, "Pt Signal 3Gamma", *m_observables->get(PT), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, *fraction2, *a3, *b3, *c3);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_ptSignal[0] = name;
    m_ptSignal[1] = pdf;
}

void ModelPDF::setPtBackgroundPDF(const TString &name, const TString &pdf) {
    if ( pdf == "1Gamma" ) {
        RooRealVar *a1    = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1    = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1    = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        m_ptBackgroundPDF = new Roo3Gamma (m_name+"_"+name, "Pt Background 1Gamma", *m_observables->get(PT), *a1, *b1, *c1, (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1));
    } else if ( pdf == "2Gamma" ) {
        RooRealVar *a1       = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1       = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1       = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2       = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2       = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2       = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *fraction = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 2 Gamma", 0., 1.);
        m_ptBackgroundPDF = new Roo3Gamma (m_name+"_"+name, "Pt Background 2Gamma", *m_observables->get(PT), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1), (RooAbsReal&)RooFit::RooConst(1));
    } else if ( pdf == "3Gamma" ) {
        RooRealVar *a1        = new RooRealVar(m_name+"_"+name+"_a1", "a1", 0., RooNumber::infinity());
        RooRealVar *b1        = new RooRealVar(m_name+"_"+name+"_b1", "b1", 0., RooNumber::infinity());
        RooRealVar *c1        = new RooRealVar(m_name+"_"+name+"_c1", "c1", 0., RooNumber::infinity());
        RooRealVar *a2        = new RooRealVar(m_name+"_"+name+"_a2", "a2", 0., RooNumber::infinity());
        RooRealVar *b2        = new RooRealVar(m_name+"_"+name+"_b2", "b2", 0., RooNumber::infinity());
        RooRealVar *c2        = new RooRealVar(m_name+"_"+name+"_c2", "c2", 0., RooNumber::infinity());
        RooRealVar *a3        = new RooRealVar(m_name+"_"+name+"_a3", "a3", 0., RooNumber::infinity());
        RooRealVar *b3        = new RooRealVar(m_name+"_"+name+"_b3", "b3", 0., RooNumber::infinity());
        RooRealVar *c3        = new RooRealVar(m_name+"_"+name+"_c3", "c3", 0., RooNumber::infinity());
        RooRealVar *fraction  = new RooRealVar(m_name+"_"+name+"_fraction", "Fraction of 3 Gamma", 0., 1.);
        RooRealVar *fraction2 = new RooRealVar(m_name+"_"+name+"_fraction2", "Fraction2 of 3 Gamma", 0., 1.);
        m_ptBackgroundPDF     = new Roo3Gamma (m_name+"_"+name, "Pt Background 3Gamma", *m_observables->get(PT), *a1, *b1, *c1, *fraction, *a2, *b2, *c2, *fraction2, *a3, *b3, *c3);
    } else if ( pdf == "lognormal" ) {
        RooRealVar *mean = new RooRealVar(m_name+"_"+name+"_mean", "mean", 1e-9, RooNumber::infinity());
        RooRealVar *sigma = new RooRealVar(m_name+"_"+name+"_sigma", "sigma", 1e-9, RooNumber::infinity());
        m_ptBackgroundPDF  = new RooLognormal (m_name+"_"+name, "Pt Background Lognormal", *m_observables->get(PT), *mean, *sigma);
    } else if ( pdf == "2lognormal" ) {
        RooRealVar *mean1 = new RooRealVar(m_name+"_"+name+"_mean1", "mean1", 1e-9, RooNumber::infinity());
        RooRealVar *sigma1 = new RooRealVar(m_name+"_"+name+"_sigma1", "sigma1", 1e-9, RooNumber::infinity());
        RooRealVar *mean2 = new RooRealVar(m_name+"_"+name+"_mean2", "mean2", 1e-9, RooNumber::infinity());
        RooRealVar *sigma2 = new RooRealVar(m_name+"_"+name+"_sigma2", "sigma2", 1e-9, RooNumber::infinity());
        RooRealVar *fraction = new RooRealVar (m_name+"_"+name+"_fraction", "Fraction of 2Lognormal", 0., 1.);
        RooLognormal *lognormal1 = new RooLognormal (m_name+"_"+name+"_lognormal1", "Pt Background Lognormal 1", *m_observables->get(PT), *mean1, *sigma1);
        RooLognormal *lognormal2 = new RooLognormal (m_name+"_"+name+"_lognormal2", "Pt Background Lognormal 2", *m_observables->get(PT), *mean2, *sigma2);
        m_ptBackgroundPDF  = new RooAddPdf (m_name+"_"+name, "Pt Background 2Lognormal", RooArgList(*lognormal1, *lognormal2), *fraction);
    } else {
        cout << "ERROR: Unknown PDF (" << pdf << ")" << endl;
        gApplication->Terminate();
    }
    m_ptBackground[0] = name;
    m_ptBackground[1] = pdf;
}

void ModelPDF::createModel(void) {
    cout << "INFO: Creating model \"" << m_name << "\"" << endl;

    RooRealVar *nEvents   = new RooRealVar(m_name+"_nEvents",   "Number of Events", m_dataset->numEntries());
    RooRealVar *fitStatus = new RooRealVar(m_name+"_fitStatus", "Fit Status",       -1);
    RooRealVar *fitEDM    = new RooRealVar(m_name+"_fitEDM",    "Fit EDM",          0.);
    RooRealVar *fitNLL    = new RooRealVar(m_name+"_fitNLL",    "Fit NLL",          0.);

    if ( m_totalPDF != nullptr ) {
        cout << "ERROR: totalPDF problem!" << endl;
        gApplication->Terminate();
    }

    if ( m_massResolutionPDF != nullptr && m_massSignalCorePDF != nullptr ) {
        if ( m_massSignalErrorPDF != nullptr || m_massBackgroundErrorPDF != nullptr ) {
            cout << "ERROR: Convolution with conditional observables has not yet been implemented..." << endl;
            gApplication->Terminate();
        }
        m_observables->get(MASS)->setBins(10000, "fft"); // "fft" -> "cache" ?
//         if ( m_xMax > m_xMin ) {
//             cout << "INFO: Setting range" << endl;
//             m_observables->get(MASS)->setMin("fft", m_xMin);
//             m_observables->get(MASS)->setMax("fft", m_xMax);
// //             m_observables->get(MASS)->setMin("fft", 5500.);
// //             m_observables->get(MASS)->setMax("fft", 5750.);
//         }
        m_massSignalPDF = new RooFFTConvPdf("convMassSignal", "Convoluted Mass Signal PDF", *m_observables->get(MASS), *m_massSignalCorePDF, *m_massResolutionPDF);
    } else {
        m_massSignalPDF = m_massSignalCorePDF;
    }

    if ( m_massSignalPDF != nullptr && m_massBackgroundPDF != nullptr ) {
        if ( m_massSignalErrorPDF != nullptr && m_massBackgroundErrorPDF == nullptr ) {
            cout << "ERROR: Punzi for mass background is missing!" << endl;
            gApplication->Terminate();
        }
        if ( m_massSignalErrorPDF == nullptr && m_massBackgroundErrorPDF != nullptr ) {
            cout << "ERROR: Punzi for mass signal is missing!" << endl;
            gApplication->Terminate();
        }
    }

    if ( m_timeSignalPDF != nullptr && m_timeBackgroundPDF != nullptr ) {
        if ( m_timeSignalErrorPDF != nullptr && m_timeBackgroundErrorPDF == nullptr ) {
            cout << "ERROR: Punzi for time background is missing!" << endl;
            gApplication->Terminate();
        }
        if ( m_timeSignalErrorPDF == nullptr && m_timeBackgroundErrorPDF != nullptr ) {
            cout << "ERROR: Punzi for time signal is missing!" << endl;
            gApplication->Terminate();
        }
    }

// TODO: m_massSignalPDFs for errors!!!

    RooArgSet signalPDFs;
    if ( m_ptSignalPDF != nullptr ) {
        if ( m_massSignalErrorPDF != nullptr || m_timeSignalErrorPDF != nullptr ) {
            if ( m_massSignalErrorPDF != nullptr ) {
                m_totalMassSignalErrorPDF = new RooProdPdf("totalMassSignalErrorModel", "totalMassSignalErrorModel", RooArgSet(*m_ptSignalPDF), RooFit::Conditional(*m_massSignalErrorPDF, *m_observables->get(MASSERROR)));
                if ( m_massSignalPDF != nullptr ) {
                    if ( m_massSignalPDFs.getSize() == 0 || m_massSignalPDFs.getSize() != m_massSignalFracs.getSize() + 1 ) {
                        cout << "ERROR: Can't create conditional product!" << endl;
                        gApplication->Terminate();
                    }
                    m_conditionalObservables.remove(*m_observables->get(MASSERROR));
                    m_useProjWDataMass = false;
                    TIterator *pdfs_it = m_massSignalPDFs.createIterator();
                    for ( RooAbsPdf *pdf = (RooAbsPdf*)(pdfs_it->Next()); pdf != NULL ; pdf = (RooAbsPdf*)(pdfs_it->Next()) ) {
                        RooAbsPdf *condPDF = new RooProdPdf((TString)pdf->GetName()+"_condProd", (TString)pdf->GetTitle()+" (cond)", RooArgSet(*m_totalMassSignalErrorPDF), RooFit::Conditional(*pdf, *m_observables->get(MASS)));
                        if ( ! m_massSignalPDFs.replace(*pdf, *condPDF) ) {
                            cout << "ERROR: Can't create conditional product!" << endl;
                            gApplication->Terminate();
                        }
                    }
                    delete pdfs_it;
                    m_totalMassSignalPDF = new RooAddPdf("totalMassSignalModel", "totalMassSignalModel", m_massSignalPDFs, m_massSignalFracs, kTRUE);
                    signalPDFs.add(*m_totalMassSignalPDF);
                } else {
                    signalPDFs.add(*m_totalMassSignalErrorPDF);
                }
            }
            if ( m_timeSignalErrorPDF != nullptr ) {
                m_totalTimeSignalErrorPDF = new RooProdPdf("totalTimeSignalErrorModel", "totalTimeSignalErrorModel", RooArgSet(*m_ptSignalPDF), RooFit::Conditional(*m_timeSignalErrorPDF, *m_observables->get(TIMEERROR)));
                if ( m_timeSignalPDF != nullptr ) {
                    if ( m_timeSignalPDFs.getSize() == 0 || m_timeSignalPDFs.getSize() != m_timeSignalFracs.getSize() + 1 ) {
                        cout << "ERROR: Can't create conditional product!" << endl;
                        gApplication->Terminate();
                    }
                    m_conditionalObservables.remove(*m_observables->get(TIMEERROR));
                    m_useProjWDataTime = false;
                    TIterator *pdfs_it = m_timeSignalPDFs.createIterator();
                    for ( RooAbsPdf *pdf = (RooAbsPdf*)(pdfs_it->Next()); pdf != NULL ; pdf = (RooAbsPdf*)(pdfs_it->Next()) ) {
                        RooAbsPdf *condPDF = new RooProdPdf((TString)pdf->GetName()+"_condProd", (TString)pdf->GetTitle()+" (cond)", RooArgSet(*m_totalTimeSignalErrorPDF), RooFit::Conditional(*pdf, *m_observables->get(TIME)));
                        if ( ! m_timeSignalPDFs.replace(*pdf, *condPDF) ) {
                            cout << "ERROR: Can't create conditional product!" << endl;
                            gApplication->Terminate();
                        }
                    }
                    delete pdfs_it;
                    m_totalTimeSignalPDF = new RooAddPdf("totalTimeSignalModel", "totalTimeSignalModel", m_timeSignalPDFs, m_timeSignalFracs, kTRUE);
                    signalPDFs.add(*m_totalTimeSignalPDF);
                } else {
                    signalPDFs.add(*m_totalTimeSignalErrorPDF);
                }
            }
        } else {
            signalPDFs.add(*m_ptSignalPDF);
        }
    }
    if ( m_massSignalErrorPDF != nullptr || m_timeSignalErrorPDF != nullptr ) {
        if ( m_massSignalErrorPDF != nullptr && m_totalMassSignalErrorPDF == nullptr ) {
            m_totalMassSignalErrorPDF = m_massSignalErrorPDF;
            if ( m_massSignalPDF != nullptr ) {
                if ( m_massSignalPDFs.getSize() == 0 || m_massSignalPDFs.getSize() != m_massSignalFracs.getSize() + 1 ) {
                    cout << "ERROR: Can't create conditional product!" << endl;
                    gApplication->Terminate();
                }
                m_conditionalObservables.remove(*m_observables->get(MASSERROR));
                m_useProjWDataMass = false;
                TIterator *pdfs_it = m_massSignalPDFs.createIterator();
                for ( RooAbsPdf *pdf = (RooAbsPdf*)(pdfs_it->Next()); pdf != NULL ; pdf = (RooAbsPdf*)(pdfs_it->Next()) ) {
                    RooAbsPdf *condPDF = new RooProdPdf((TString)pdf->GetName()+"_condProd", (TString)pdf->GetTitle()+" (cond)", RooArgSet(*m_totalMassSignalErrorPDF), RooFit::Conditional(*pdf, *m_observables->get(MASS)));
                    if ( ! m_massSignalPDFs.replace(*pdf, *condPDF) ) {
                        cout << "ERROR: Can't create conditional product!" << endl;
                        gApplication->Terminate();
                    }
                }
                delete pdfs_it;
                m_totalMassSignalPDF = new RooAddPdf("totalMassSignalModel", "totalMassSignalModel", m_massSignalPDFs, m_massSignalFracs, kTRUE);
                signalPDFs.add(*m_totalMassSignalPDF);
            } else {
                signalPDFs.add(*m_totalMassSignalErrorPDF);
            }
        }
        if ( m_timeSignalErrorPDF != nullptr && m_totalTimeSignalErrorPDF == nullptr ) {
            m_totalTimeSignalErrorPDF = m_timeSignalErrorPDF;
            if ( m_timeSignalPDF != nullptr ) {
                if ( m_timeSignalPDFs.getSize() == 0 || m_timeSignalPDFs.getSize() != m_timeSignalFracs.getSize() + 1 ) {
                    cout << "ERROR: Can't create conditional product!" << endl;
                    gApplication->Terminate();
                }
                m_conditionalObservables.remove(*m_observables->get(TIMEERROR));
                m_useProjWDataTime = false;
                TIterator *pdfs_it = m_timeSignalPDFs.createIterator();
                for ( RooAbsPdf *pdf = (RooAbsPdf*)(pdfs_it->Next()); pdf != NULL ; pdf = (RooAbsPdf*)(pdfs_it->Next()) ) {
                    RooAbsPdf *condPDF = new RooProdPdf((TString)pdf->GetName()+"_condProd", (TString)pdf->GetTitle()+" (cond)", RooArgSet(*m_totalTimeSignalErrorPDF), RooFit::Conditional(*pdf, *m_observables->get(TIME)));
                    if ( ! m_timeSignalPDFs.replace(*pdf, *condPDF) ) {
                        cout << "ERROR: Can't create conditional product!" << endl;
                        gApplication->Terminate();
                    }
                }
                delete pdfs_it;
                m_totalTimeSignalPDF = new RooAddPdf("totalTimeSignalModel", "totalTimeSignalModel", m_timeSignalPDFs, m_timeSignalFracs, kTRUE);
                signalPDFs.add(*m_totalTimeSignalPDF);
            } else {
                signalPDFs.add(*m_totalTimeSignalErrorPDF);
            }
        }
    }
    if ( m_massSignalPDF != nullptr && m_totalMassSignalPDF == nullptr ) {
        m_totalMassSignalPDF = m_massSignalPDF;
        signalPDFs.add(*m_totalMassSignalPDF);
    }
    if ( m_timeSignalPDF != nullptr && m_totalTimeSignalPDF == nullptr ) {
        m_totalTimeSignalPDF = m_timeSignalPDF;
        signalPDFs.add(*m_totalTimeSignalPDF);
    }
    if ( signalPDFs.getSize() > 0 ) m_signalPDF = new RooProdPdf("signalModel", "Signal Model", signalPDFs);

    RooArgSet backgroundPDFs;
    if ( m_ptBackgroundPDF != nullptr ) {
        if ( m_massBackgroundErrorPDF != nullptr || m_timeBackgroundErrorPDF != nullptr ) {
            if ( m_massBackgroundErrorPDF != nullptr ) {
                m_totalMassBackgroundErrorPDF = new RooProdPdf("totalMassBackgroundErrorModel", "totalMassBackgroundErrorModel", RooArgSet(*m_ptBackgroundPDF), RooFit::Conditional(*m_massBackgroundErrorPDF, *m_observables->get(MASSERROR)));
                if ( m_massBackgroundPDF != nullptr ) {
                    if ( m_massBackgroundPDFs.getSize() == 0 || m_massBackgroundPDFs.getSize() != m_massBackgroundFracs.getSize() + 1 ) {
                        cout << "ERROR: Can't create conditional product!" << endl;
                        gApplication->Terminate();
                    }
                    m_conditionalObservables.remove(*m_observables->get(MASSERROR));
                    m_useProjWDataMass = false;
                    TIterator *pdfs_it = m_massBackgroundPDFs.createIterator();
                    for ( RooAbsPdf *pdf = (RooAbsPdf*)(pdfs_it->Next()); pdf != NULL ; pdf = (RooAbsPdf*)(pdfs_it->Next()) ) {
                        RooAbsPdf *condPDF = new RooProdPdf((TString)pdf->GetName()+"_condProd", (TString)pdf->GetTitle()+" (cond)", RooArgSet(*m_totalMassBackgroundErrorPDF), RooFit::Conditional(*pdf, *m_observables->get(MASS)));
                        if ( ! m_massBackgroundPDFs.replace(*pdf, *condPDF) ) {
                            cout << "ERROR: Can't create conditional product!" << endl;
                            gApplication->Terminate();
                        }
                    }
                    delete pdfs_it;
                    m_totalMassBackgroundPDF = new RooAddPdf("totalMassBackgroundModel", "totalMassBackgroundModel", m_massBackgroundPDFs, m_massBackgroundFracs, kTRUE);
                    backgroundPDFs.add(*m_totalMassBackgroundPDF);
                } else {
                    backgroundPDFs.add(*m_totalMassBackgroundErrorPDF);
                }
            }
            if ( m_timeBackgroundErrorPDF != nullptr ) {
                m_totalTimeBackgroundErrorPDF = new RooProdPdf("totalTimeBackgroundErrorModel", "totalTimeBackgroundErrorModel", RooArgSet(*m_ptBackgroundPDF), RooFit::Conditional(*m_timeBackgroundErrorPDF, *m_observables->get(TIMEERROR)));
                if ( m_timeBackgroundPDF != nullptr ) {
                    if ( m_timeBackgroundPDFs.getSize() == 0 || m_timeBackgroundPDFs.getSize() != m_timeBackgroundFracs.getSize() + 1 ) {
                        cout << "ERROR: Can't create conditional product!" << endl;
                        gApplication->Terminate();
                    }
                    m_conditionalObservables.remove(*m_observables->get(TIMEERROR));
                    m_useProjWDataTime = false;
                    TIterator *pdfs_it = m_timeBackgroundPDFs.createIterator();
                    for ( RooAbsPdf *pdf = (RooAbsPdf*)(pdfs_it->Next()); pdf != NULL ; pdf = (RooAbsPdf*)(pdfs_it->Next()) ) {
                        RooAbsPdf *condPDF = new RooProdPdf((TString)pdf->GetName()+"_condProd", (TString)pdf->GetTitle()+" (cond)", RooArgSet(*m_totalTimeBackgroundErrorPDF), RooFit::Conditional(*pdf, *m_observables->get(TIME)));
                        if ( ! m_timeBackgroundPDFs.replace(*pdf, *condPDF) ) {
                            cout << "ERROR: Can't create conditional product!" << endl;
                            gApplication->Terminate();
                        }
                    }
                    delete pdfs_it;
                    m_totalTimeBackgroundPDF = new RooAddPdf("totalTimeBackgroundModel", "totalTimeBackgroundModel", m_timeBackgroundPDFs, m_timeBackgroundFracs, kTRUE);
                    backgroundPDFs.add(*m_totalTimeBackgroundPDF);
                } else {
                    backgroundPDFs.add(*m_totalTimeBackgroundErrorPDF);
                }
            }
        } else {
            backgroundPDFs.add(*m_ptBackgroundPDF);
        }
    }
    if ( m_massBackgroundErrorPDF != nullptr || m_timeBackgroundErrorPDF != nullptr ) {
        if ( m_massBackgroundErrorPDF != nullptr && m_totalMassBackgroundErrorPDF == nullptr ) {
            m_totalMassBackgroundErrorPDF = m_massBackgroundErrorPDF;
            if ( m_massBackgroundPDF != nullptr ) {
                if ( m_massBackgroundPDFs.getSize() == 0 || m_massBackgroundPDFs.getSize() != m_massBackgroundFracs.getSize() + 1 ) {
                    cout << "ERROR: Can't create conditional product!" << endl;
                    gApplication->Terminate();
                }
                m_conditionalObservables.remove(*m_observables->get(MASSERROR));
                m_useProjWDataMass = false;
                TIterator *pdfs_it = m_massBackgroundPDFs.createIterator();
                for ( RooAbsPdf *pdf = (RooAbsPdf*)(pdfs_it->Next()); pdf != NULL ; pdf = (RooAbsPdf*)(pdfs_it->Next()) ) {
                    RooAbsPdf *condPDF = new RooProdPdf((TString)pdf->GetName()+"_condProd", (TString)pdf->GetTitle()+" (cond)", RooArgSet(*m_totalMassBackgroundErrorPDF), RooFit::Conditional(*pdf, *m_observables->get(MASS)));
                    if ( ! m_massBackgroundPDFs.replace(*pdf, *condPDF) ) {
                        cout << "ERROR: Can't create conditional product!" << endl;
                        gApplication->Terminate();
                    }
                }
                delete pdfs_it;
                m_totalMassBackgroundPDF = new RooAddPdf("totalMassBackgroundModel", "totalMassBackgroundModel", m_massBackgroundPDFs, m_massBackgroundFracs, kTRUE);
                backgroundPDFs.add(*m_totalMassBackgroundPDF);
            } else {
                backgroundPDFs.add(*m_totalMassBackgroundErrorPDF);
            }
        }
        if ( m_timeBackgroundErrorPDF != nullptr && m_totalTimeBackgroundErrorPDF == nullptr ) {
            m_totalTimeBackgroundErrorPDF = m_timeBackgroundErrorPDF;
            if ( m_timeBackgroundPDF != nullptr ) {
                if ( m_timeBackgroundPDFs.getSize() == 0 || m_timeBackgroundPDFs.getSize() != m_timeBackgroundFracs.getSize() + 1 ) {
                    cout << "ERROR: Can't create conditional product!" << endl;
                    gApplication->Terminate();
                }
                m_conditionalObservables.remove(*m_observables->get(TIMEERROR));
                m_useProjWDataTime = false;
                TIterator *pdfs_it = m_timeBackgroundPDFs.createIterator();
                for ( RooAbsPdf *pdf = (RooAbsPdf*)(pdfs_it->Next()); pdf != NULL ; pdf = (RooAbsPdf*)(pdfs_it->Next()) ) {
                    RooAbsPdf *condPDF = new RooProdPdf((TString)pdf->GetName()+"_condProd", (TString)pdf->GetTitle()+" (cond)", RooArgSet(*m_totalTimeBackgroundErrorPDF), RooFit::Conditional(*pdf, *m_observables->get(TIME)));
                    if ( ! m_timeBackgroundPDFs.replace(*pdf, *condPDF) ) {
                        cout << "ERROR: Can't create conditional product!" << endl;
                        gApplication->Terminate();
                    }
                }
                delete pdfs_it;
                m_totalTimeBackgroundPDF = new RooAddPdf("totalTimeBackgroundModel", "totalTimeBackgroundModel", m_timeBackgroundPDFs, m_timeBackgroundFracs, kTRUE);
                backgroundPDFs.add(*m_totalTimeBackgroundPDF);
            } else {
                backgroundPDFs.add(*m_totalTimeBackgroundErrorPDF);
            }
        }
    }
    if ( m_massBackgroundPDF != nullptr && m_totalMassBackgroundPDF == nullptr ) {
        m_totalMassBackgroundPDF = m_massBackgroundPDF;
        backgroundPDFs.add(*m_totalMassBackgroundPDF);
    }
    if ( m_timeBackgroundPDF != nullptr && m_totalTimeBackgroundPDF == nullptr ) {
        m_totalTimeBackgroundPDF = m_timeBackgroundPDF;
        backgroundPDFs.add(*m_totalTimeBackgroundPDF);
    }
    if ( backgroundPDFs.getSize() > 0 ) m_backgroundPDF = new RooProdPdf("backgroundModel", "Background Model", backgroundPDFs);

    if ( m_signalPDF != nullptr && m_backgroundPDF != nullptr ) {
        if ( m_extendedFit ) {
            RooRealVar *nSig            = new RooRealVar(m_name+"_nSig", "Number of Signal",     0, m_dataset->numEntries());
            RooRealVar *nBck            = new RooRealVar(m_name+"_nBck", "Number of Background", 0, m_dataset->numEntries());
            m_totalPDF = new RooAddPdf("totalModel", "", RooArgList(*m_signalPDF, *m_backgroundPDF), RooArgList(*nSig, *nBck));
        } else {
            RooRealVar *sigFrac         = new RooRealVar(m_name+"_sigFrac", "Signal Fraction", 0., 1.);
            m_totalPDF = new RooAddPdf("totalModel", "", RooArgList(*m_signalPDF, *m_backgroundPDF), *sigFrac);
        }
    } else if ( m_signalPDF != nullptr ) {
        m_totalPDF = m_signalPDF;
    } else if ( m_backgroundPDF != nullptr ) {
        m_totalPDF = m_backgroundPDF;
    } else {
        cout << "ERROR: totalPDF problem!" << endl;
        gApplication->Terminate();
    }

    m_pars = (RooArgSet*)m_totalPDF->getParameters(*m_dataset);
    m_pars->add(*nEvents);
    m_pars->add(*fitStatus);
    m_pars->add(*fitEDM);
    m_pars->add(*fitNLL);
}

RooAbsPdf* ModelPDF::getTotalMassSignalPDF(void) {
    if ( m_totalPDF == nullptr ) createModel();
    return m_totalMassSignalPDF;
}

RooArgSet* ModelPDF::getPars(void) {
    if ( m_totalPDF == nullptr ) createModel();
    if ( m_finalPars != nullptr ) return m_finalPars;
    else return m_pars;
}

RooRealVar* ModelPDF::getPars(const TString &varName) {
    if ( m_totalPDF == nullptr ) createModel();
    if ( m_finalPars != nullptr ) return (RooRealVar*)m_finalPars->find(m_name+"_"+varName);
    else return (RooRealVar*)m_pars->find(m_name+"_"+varName);
}

RooRealVar* ModelPDF::getInitialPars(const TString &varName) {
    RooRealVar *par = nullptr;
    if ( m_totalPDF == nullptr ) createModel();
    if ( m_initialPars != nullptr ) {
        par = (RooRealVar*)m_initialPars->find(m_name+"_"+varName);
    } else {
        cout << "ERROR: getInitialPars problem!" << endl;
        gApplication->Terminate();
    }
    return par;
}

void ModelPDF::setPars(ModelPDF *model, const Bool_t &fixed) {
    vector<TString> skip;
    skip.push_back(m_name+"_nEvents");
    skip.push_back(m_name+"_nSig");
    skip.push_back(m_name+"_nBck");
    skip.push_back(m_name+"_sigFrac");
    skip.push_back(m_name+"_fitStatus");
    skip.push_back(m_name+"_fitEDM");
    skip.push_back(m_name+"_fitNLL");

    if ( m_totalPDF == nullptr ) createModel();

    TIterator *pars_it = model->getPars()->createIterator();
    for ( RooRealVar *p = (RooRealVar*)(pars_it->Next()); p != NULL ; p = (RooRealVar*)(pars_it->Next()) ) {
        TString name = p->GetName();
        name.Remove(0, model->getName().Length()+1);
        if ( find(skip.begin(), skip.end(), m_name+"_"+name) != skip.end() ) continue;
        RooRealVar *varPtr = (RooRealVar*)m_pars->find(m_name+"_"+name);
        if ( varPtr ) {
            varPtr->setVal(p->getVal());
            if ( fixed ) varPtr->setConstant(kTRUE);
            else         varPtr->setConstant(kFALSE);
        }
    }
    delete pars_it;
}

void ModelPDF::setPars(const TString &varName, const Double_t &val, const Bool_t &fixed) {
    if ( m_totalPDF == nullptr ) createModel();

    RooRealVar *varPtr = (RooRealVar*)m_pars->find(m_name+"_"+varName);
    if ( varPtr ) {
        varPtr->setVal(val);
//         varPtr->setError(1e-6);
        if ( fixed ) varPtr->setConstant(kTRUE);
        else         varPtr->setConstant(kFALSE);
    }
}

void ModelPDF::setPars(const TString &varName, const Double_t &val, const Double_t &x1, const Double_t &x2) {
    if ( m_totalPDF == nullptr ) createModel();
    RooRealVar *varPtr = (RooRealVar*)m_pars->find(m_name+"_"+varName);
    if ( varPtr ) {
        varPtr->setVal(val);
        varPtr->setRange(x1, x2);
        varPtr->setConstant(kFALSE);
    }
}

void ModelPDF::drawPlotLog(const Observable &var, const TString &title) {
    drawPlotCore(true, var, m_observables->get(var)->getMin(), m_observables->get(var)->getMax(), 100, title);
}

void ModelPDF::drawPlotLog(const Observable &var, const Double_t &xMin, const Double_t &xMax, const TString &title) {
    drawPlotCore(true, var, xMin, xMax, 100, title);
}

void ModelPDF::drawPlotLog(const Observable &var, const Int_t &nBins, const TString &title) {
    drawPlotCore(true, var, m_observables->get(var)->getMin(), m_observables->get(var)->getMax(), nBins, title);
}

void ModelPDF::drawPlotLog(const Observable &var, const Double_t &xMin, const Double_t &xMax, const Int_t &nBins, const TString &title) {
    drawPlotCore(true, var, xMin, xMax, nBins, title);
}

void ModelPDF::drawPlot(const Observable &var, const TString &title) {
    drawPlotCore(false, var, m_observables->get(var)->getMin(), m_observables->get(var)->getMax(), 100, title);
}

void ModelPDF::drawPlot(const Observable &var, const Double_t &xMin, const Double_t &xMax, const TString &title) {
    drawPlotCore(false, var, xMin, xMax, 100, title);
}

void ModelPDF::drawPlot(const Observable &var, const Int_t &nBins, const TString &title) {
    drawPlotCore(false, var, m_observables->get(var)->getMin(), m_observables->get(var)->getMax(), nBins, title);
}

void ModelPDF::drawPlot(const Observable &var, const Double_t &xMin, const Double_t &xMax, const Int_t &nBins, const TString &title) {
    drawPlotCore(false, var, xMin, xMax, nBins, title);
}

void ModelPDF::drawPlotCore(const Bool_t &log, const Observable &var, const Double_t &xMin, const Double_t &xMax, const Int_t &nBins, const TString &title) {
    const time_t startTime = TTimeStamp().GetSec();
    // if ( m_totalPDF == nullptr ) createModel();

    TPad *pad = (TPad*)gPad;
    gStyle->SetLineScalePS();
    RooPlot *frame = m_observables->get(var)->frame(xMin, xMax, nBins);
    std::vector<TString> titles = splitString(title, ":");
    if ( titles.size() > 0 ) frame->SetTitle(titles.at(0).Data());
    if ( titles.size() > 1 ) frame->GetXaxis()->SetTitle(titles.at(1).Data());
    if ( titles.size() > 2 ) frame->GetYaxis()->SetTitle(titles.at(2).Data());

    frame->GetYaxis()->SetTitleOffset(1.1); // AS
    frame->GetXaxis()->SetTitleOffset(1.1); // AS
    frame->GetXaxis()->SetNoExponent(kTRUE);
    // frame->GetXaxis()->SetNdivisions(505);
    // frame->GetYaxis()->SetNdivisions(510); // AS

    TLegend *legend = new TLegend(0.69, 0.55, 0.89, 0.35); // 0.69, 0.85, 0.89, 0.70 // 0.40
    // TLegend *legend = new TLegend(0.62, 0.85, 0.82, 0.65); // Bs
    legend->SetFillColor(kWhite);
    legend->SetLineColor(kWhite);
    legend->SetTextFont(42);
    legend->SetTextSize(0.045);
    legend->SetShadowColor(kWhite);

    RooAbsPdf *sigPDF = nullptr;
    RooAbsPdf *bckPDF = nullptr;
    RooAbsPdf *totPDF = nullptr;

    if ( m_totalPDF != nullptr ) {
        switch( var ) {
            case MASS :
                bckPDF = m_totalMassBackgroundPDF;
                sigPDF = m_totalMassSignalPDF;
                break;
            case MASSERROR :
                bckPDF = m_totalMassBackgroundErrorPDF;
                sigPDF = m_totalMassSignalErrorPDF;
                break;
            case TIME :
                bckPDF = m_totalTimeBackgroundPDF;
                sigPDF = m_totalTimeSignalPDF;
                break;
            case TIMEERROR :
                bckPDF = m_totalTimeBackgroundErrorPDF;
                sigPDF = m_totalTimeSignalErrorPDF;
                break;
            case PT :
                bckPDF = m_ptBackgroundPDF;
                sigPDF = m_ptSignalPDF;
                break;
        }
    }

    m_dataset->plotOn(frame, RooFit::DataError(RooAbsData::SumW2), RooFit::Name("dataProj"));
    legend->AddEntry(frame->findObject("dataProj"), "Data", "p");

    // Replace: "Events / ( X Units )" -> "Events / X Units" (+ [[varUnit]])
    TString strTit(frame->GetYaxis()->GetTitle());
    TPRegexp("^Events / \\( (\\d+\\.?\\d*e?[-+]?\\d*\\s*)\\[\\[([-+/*]+\\s*\\d+\\.?\\d*e?[-+]?\\d*)?\\]\\]\\s*(\\w*) \\)$").Substitute(strTit, "Events / $1$3");
    frame->GetYaxis()->SetTitle(strTit);
    // Replace: "(Units)" -> "[Units]" (+ [[varUnit]])
    strTit = frame->GetXaxis()->GetTitle();
    TPRegexp("^(.*) \\(\\[\\[([-+/*]+\\s*\\d+\\.?\\d*e?[-+]?\\d*)?\\]\\]\\s*(\\w*)\\)$").Substitute(strTit, "$1 [$3]");
    frame->GetXaxis()->SetTitle(strTit);
    // End of replace

    if ( bckPDF != nullptr || sigPDF != nullptr ) {
        if ( m_extendedFit ) {
            if ( sigPDF == nullptr ) totPDF = bckPDF;
            else if ( bckPDF == nullptr ) totPDF = sigPDF;
            else totPDF = new RooAddPdf("totPDF", "", RooArgList(*sigPDF, *bckPDF), RooArgList(*getPars("nSig"), *getPars("nBck")));
        } else {
            if ( sigPDF == nullptr ) totPDF = bckPDF;
            else if ( bckPDF == nullptr ) totPDF = sigPDF;
            else totPDF = new RooAddPdf("totPDF", "", RooArgList(*sigPDF, *bckPDF), *getPars("sigFrac"));
        }

        TIterator *modelObservables_it = totPDF->getObservables(m_dataset)->createIterator();
        Int_t obsCount = 0;
        for ( RooAbsArg *obs = (RooAbsArg*)(modelObservables_it->Next()); obs != NULL ; obs = (RooAbsArg*)(modelObservables_it->Next()) ) {
            RooRealVar *obsVar = dynamic_cast<RooRealVar*>(obs);
            if ( ! obsVar ) continue;
            if ( obsVar == m_observables->get(var) ) continue;
            obsCount++;
        }
        delete modelObservables_it;

        Bool_t useProjWData = ( m_useProjWDataMass || m_useProjWDataTime || obsCount > 0 );

        cout << "INFO: Plotting " << m_observables->get(var)->GetName() << ( obsCount > 0 ? ", "+to_string(obsCount)+" other observables" : "" ) << ( useProjWData ? ", using ProjWData" : "" ) << endl;

        RooLinkedList drawOptions;
        drawOptions.Add(RooFit::LineWidth(3).Clone());
        drawOptions.Add(RooFit::PrintEvalErrors(-1).Clone());
        if ( useProjWData ) {
            drawOptions.Add(RooFit::ProjWData(*m_dataset, kTRUE).Clone());
            drawOptions.Add(RooFit::NumCPU(m_nCPU).Clone());
            // HACK to make ProjWData plotting the PDF with correct normalization: RooFit::Normalization(1./m_dataset->numEntries(),RooAbsReal::Relative)
            drawOptions.Add(RooFit::Normalization(1./m_dataset->numEntries(),RooAbsReal::Relative).Clone());
        }
        if ( sigPDF != nullptr ) {
            RooLinkedList sigDrawOptions(drawOptions);
            sigDrawOptions.Add(RooFit::Components(sigPDF->GetName()).Clone());
            sigDrawOptions.Add(RooFit::LineColor(kGreen).Clone());
            sigDrawOptions.Add(RooFit::Name("sigPDF").Clone());
            totPDF->plotOn(frame, sigDrawOptions);
            legend->AddEntry(frame->findObject("sigPDF"), "Signal", "l");
        }
        if ( bckPDF != nullptr ) {
            RooLinkedList bckDrawOptions(drawOptions);
            bckDrawOptions.Add(RooFit::Components(bckPDF->GetName()).Clone());
            bckDrawOptions.Add(RooFit::LineColor(kBlue).Clone());
            bckDrawOptions.Add(RooFit::Name("bckPDF").Clone());
            totPDF->plotOn(frame, bckDrawOptions);
            legend->AddEntry(frame->findObject("bckPDF"), "Background", "l");
        }
        if ( bckPDF != nullptr && sigPDF != nullptr ) {
            RooLinkedList totDrawOptions(drawOptions);
            totDrawOptions.Add(RooFit::Components(totPDF->GetName()).Clone());
            totDrawOptions.Add(RooFit::LineColor(kRed).Clone());
            totDrawOptions.Add(RooFit::Name("totPDF").Clone());
            totPDF->plotOn(frame, totDrawOptions);
            legend->AddEntry(frame->findObject("totPDF"), "Fit", "l");
        }
    }

    RooHist *pullHist;
    TPad* pad1;
    TPad* pad2;

    if ( m_totalPDF != nullptr ) {
        pullHist = frame->pullHist();
        pad->Divide(1,2);
        pad1 = new TPad("pad1", "pad1", 0., 0.29, 1., 1.);
        pad2 = new TPad("pad2", "pad2", 0., 0., 1., 0.29);
        pad1->Draw();
        pad2->Draw();

        // pad1->SetTopMargin(0.1);
        // pad2->SetTopMargin(0.1);
        // pad1->SetRightMargin(0.09);
        // pad2->SetRightMargin(0.09);
        // pad1->SetBottomMargin(0.13);
        // pad2->SetBottomMargin(0.21);
        // pad1->SetLeftMargin(0.15);
        // pad2->SetLeftMargin(0.15);
        pad1->SetTopMargin(0.1);
        pad1->SetRightMargin(0.09);
        pad1->SetBottomMargin(0.);
        pad1->SetLeftMargin(0.15);
        pad2->SetTopMargin(0.);
        pad2->SetRightMargin(0.09);
        pad2->SetBottomMargin(0.31);
        pad2->SetLeftMargin(0.15);

// Draw signal scaled by factor 20
// if ( bckPDF != nullptr && sigPDF != nullptr ) {
//     setPars("nSig", getPars("nSig")->getVal() * 20., true);
//     m_totalPDF->plotOn(frame, RooFit::Components(sigPDF->GetName()), RooFit::LineColor(kGreen+2), RooFit::LineStyle(kDashed), RooFit::LineWidth(3), RooFit::NumCPU(m_nCPU), RooFit::PrintEvalErrors(-1), RooFit::Name("sigPDFScaled"));
//     legend->AddEntry(frame->findObject("sigPDFScaled"), "Signal Scaled", "l");
// }

        pad2->cd();
        pad2->DrawFrame(xMin, -5.0, xMax, 5.0);
        pullHist->GetXaxis()->SetRange(xMin, xMax);
        pullHist->Draw();
        /*pad2->SetFillStyle(0);
        // if ( m_finalPars != nullptr ) {
            RooPlot* pullFrame = m_observables->get(var)->frame();
            pullFrame->addPlotable(pullHist,"X0 B");
            (pullFrame->getAttFill())->SetFillColor(kBlue);
            pullFrame->SetTickLength(0.1);
            pullFrame->GetXaxis()->SetRange(xMin, xMax);
            pullFrame->GetXaxis()->SetTitle(frame->GetXaxis()->GetTitle());
            pullFrame->GetXaxis()->SetTitleSize(.13);
            pullFrame->GetXaxis()->SetLabelSize(.13);
            pullFrame->GetXaxis()->SetTitleOffset(1.0); // AS
            pullFrame->GetYaxis()->SetTitle("(data-fit)/#sigma");
            pullFrame->GetYaxis()->SetTitleSize(.13);
            pullFrame->GetYaxis()->SetLabelSize(.10);
            pullFrame->GetYaxis()->SetTitleOffset(0.41); // AS
            pullFrame->GetYaxis()->SetNdivisions(510); // AS
            pullFrame->GetXaxis()->SetNoExponent(kTRUE); // MeV
            pullFrame->SetMinimum( ((int)floor(pullFrame->GetMinimum()) % 2 == 0) ? floor(pullFrame->GetMinimum())+0.01 : floor(pullFrame->GetMinimum())-0.99);
            pullFrame->SetMaximum( ((int)ceil(pullFrame->GetMaximum()) % 2 == 0) ? ceil(pullFrame->GetMaximum())-0.01 : ceil(pullFrame->GetMaximum())+0.99 );      
            pullFrame->Draw();
        // }*/
        pad2->Update();
    } else {
        pad1 = new TPad("pad1", "pad1", 0., 0., 1., 1.);
        pad1->Draw();
        pad1->SetTopMargin(0.1);
        pad1->SetRightMargin(0.09);
        pad1->SetBottomMargin(0.15);
        pad1->SetLeftMargin(0.15);
    }

    pad1->cd();
    if ( log ) {
        pad1->SetLogy();
        pad1->Update();
    }
    frame->Draw();
    legend->Draw();

    pad1->Update();
    gPad = pad;

    cout << "INFO: Plotting time " << printTime( TTimeStamp().GetSec() - startTime ) << endl;

    // delete pullHist;
    // delete legend;
    if ( m_totalPDF != nullptr ) delete totPDF;
    // delete frame;
    // delete pad1;
    // delete pad2;
}

void ModelPDF::fit(void) {
    const time_t startTime = TTimeStamp().GetSec();

    if ( m_totalPDF == nullptr ) createModel();

    if ( m_dataset->numEntries() == 0 ) {
        cout << "ERROR: No events to fit!" << endl;
        gApplication->Terminate();
    }

    cout << "INFO: Start of the fit \"" << m_name << "\", using " << m_nCPU << " cores" << endl;
    cout << "\n  ======================================================\n" << endl;
    cout << "  "; m_dataset->Print();
    cout << "  "; m_pars->Print();
    cout << "  "; m_totalPDF->Print();
    cout << "\n  ======================================================\n" << endl;

    m_initialPars = (RooArgSet*)m_pars->snapshot();

    RooLinkedList fitOptions;
    fitOptions.Add(RooFit::Save().Clone());
    fitOptions.Add(RooFit::NumCPU(m_nCPU).Clone());
    fitOptions.Add(RooFit::PrintEvalErrors(-1).Clone());
    fitOptions.Add(RooFit::Warnings(kFALSE).Clone());
    fitOptions.Add(RooFit::Verbose(kFALSE).Clone());
    fitOptions.Add(RooFit::PrintLevel(1).Clone());
    fitOptions.Add(RooFit::SumW2Error(kTRUE).Clone());
    fitOptions.Add(RooFit::InitialHesse(kTRUE).Clone());
    if ( m_conditionalObservables.getSize() > 0 ) {
        fitOptions.Add(RooFit::ConditionalObservables(m_conditionalObservables).Clone());
        cout << "INFO: Using ConditionalObservables "; m_conditionalObservables.Print();
    }
    if ( m_xMax > m_xMin ) fitOptions.Add(RooFit::Range(m_xMin, m_xMax).Clone());

    m_fitResult = m_totalPDF->fitTo(*m_dataset, fitOptions);

    // Bool_t statusOK = kTRUE;
    // if( m_fitResult->statusCodeHistory(0) ) cout << "\n\n\t\tERROR: 0\n\n" << endl; // statusOK = kFALSE;
    // if( m_fitResult->statusCodeHistory(1) ) cout << "\n\n\t\tERROR: 1\n\n" << endl; // statusOK = kFALSE;

    setPars("fitStatus", m_fitResult->status(), true);
    setPars("fitEDM",    m_fitResult->edm(),    true);
    setPars("fitNLL",    m_fitResult->minNll(), true);
    m_finalPars = (RooArgSet*)m_pars->snapshot();

    cout << "INFO: End of the fit \"" << m_name << "\", time " << printTime( TTimeStamp().GetSec() - startTime ) << endl;
}

void ModelPDF::saveResults(const TString &name) {
    cout << "INFO: Saving results to the file \"" << name << endl;
    m_finalPars->writeToFile(name);
}

void ModelPDF::readResults(const TString &name, TString prefix) {
    cout << "INFO: Reading parameters from the file \"" << name << endl;
    if ( m_totalPDF == nullptr ) createModel();

    if ( prefix.IsNull() ) prefix = m_name;

    ifstream file(name);
    string line;
    while ( getline(file, line) ) {
        std::vector<TString> dataVector = splitString(line);
        TString varName(dataVector[0]);
        TPRegexp("^"+prefix+"_(.*)").Substitute(varName, "$1");
        RooRealVar *varPtr = (RooRealVar*)m_pars->find(m_name+"_"+varName);
        if ( varPtr ) {
            varPtr->setVal(stod((string)dataVector[2]));
            cout << "INFO: Parameter " << varPtr->GetName() << " set to " << varPtr->getVal() << endl;
            // TODO: count and compare
            // TODO: setError()
            // TODO: options: ignore model name
        }
    }
// Code below does NOT work if 1eXYZ is present in the file
//     if ( ! m_pars->readFromFile(name) ) {
//         cout << "ERROR: Can't read parameters!" << endl;
//         gApplication->Terminate();
//     }
}

void ModelPDF::printModel(const TString &options) {
    cout << "INFO: Printing model" << endl;
    cout << "\n  ======================================================\n" << endl;
    cout << "  Model Name: " << m_name << endl;
    if ( m_massSignalCorePDF      != nullptr ) cout << "    " << m_massSignalPDF->GetTitle() << endl;
    if ( m_massSignalErrorPDF     != nullptr ) cout << "    " << m_massSignalErrorPDF->GetTitle() << endl;
    if ( m_massBackgroundPDF      != nullptr ) cout << "    " << m_massBackgroundPDF->GetTitle() << endl;
    if ( m_massBackgroundErrorPDF != nullptr ) cout << "    " << m_massBackgroundErrorPDF->GetTitle() << endl;
    if ( m_massResolutionPDF      != nullptr ) cout << "    " << m_massResolutionPDF->GetTitle() << endl;
    if ( m_timeSignalPDF          != nullptr ) cout << "    " << m_timeSignalPDF->GetTitle() << endl;
    if ( m_timeSignalErrorPDF     != nullptr ) cout << "    " << m_timeSignalErrorPDF->GetTitle() << endl;
    if ( m_timeBackgroundPDF      != nullptr ) cout << "    " << m_timeBackgroundPDF->GetTitle() << endl;
    if ( m_timeBackgroundErrorPDF != nullptr ) cout << "    " << m_timeBackgroundErrorPDF->GetTitle() << endl;
    if ( m_timeResolutionPDF      != nullptr ) cout << "    " << m_timeResolutionPDF->GetTitle() << endl;
    if ( m_ptSignalPDF            != nullptr ) cout << "    " << m_ptSignalPDF->GetTitle() << endl;
    if ( m_ptBackgroundPDF        != nullptr ) cout << "    " << m_ptBackgroundPDF->GetTitle() << endl;
    cout << "\n  ======================================================\n" << endl;
}

void ModelPDF::printResults(const TString &options) {
    if ( m_finalPars == nullptr ) return;
    Bool_t printConst = options.Contains("const");
    cout << "INFO: Printing results" << endl;
    cout << "\n  ======================================================\n" << endl;
    cout << "  Model   = " << m_name << endl;
    cout << "  Status  = " << getPars("fitStatus")->getVal() << endl;
    cout << "  EDM     = " << getPars("fitEDM")->getVal() << endl;
    cout << "  NLL     = " << getPars("fitNLL")->getVal() << endl;
    cout << "  #Events = " << getPars("nEvents")->getVal() << endl;
    if ( options.Contains("selection") ) cout << "  Cut     = " << m_dataset->GetTitle() << endl << endl;
    cout << "\n  ------------------------------------------------------\n" << endl;

    TIterator *pars_it = m_finalPars->createIterator();
    Int_t strSize = 0;
    for ( RooRealVar *p = (RooRealVar*)(pars_it->Next()); p != NULL ; p = (RooRealVar*)(pars_it->Next()) ) {
        TString name = p->GetName();
        Int_t tmpStrSize = name.Length() - ( m_name.Length() + 1 );
        if ( tmpStrSize > strSize ) strSize = tmpStrSize;
    }
    pars_it->Reset();
    for ( RooRealVar *p = (RooRealVar*)(pars_it->Next()); p != NULL ; p = (RooRealVar*)(pars_it->Next()) ) {
        TString name = p->GetName();
        name.Remove(0, m_name.Length()+1);
        if ( p->getError() != 0. ) {
            TString str = d2string(p->getError(), "%.3g");
            TPRegexp("(\\d{1,}).(\\d{0,})").Substitute(str, "$2");
            TString format = "%# .";
            format += str.Length();
            format += "f";
            printf("  %-*s = %-*s +/-%-*s  [ %#.3f | ", strSize, name.Data(), 9, d2string(p->getVal(), format.Data()).Data(), 9, d2string(p->getError(), format.Data()).Data(), getInitialPars(name)->getVal());
            ( p->getMin() == -RooNumber::infinity() ? printf("-inf.") : printf("%#.3f", p->getMin()) );
            printf(" -> ");
            ( p->getMax() == RooNumber::infinity() ? printf("inf.") : printf("%#.3f", p->getMax()) );
            printf(" ]\n");
        } else {
            if ( printConst ) printf("  %-*s = %-*s  [ const. ]\n", strSize, name.Data(), 22, d2string(p->getVal(), "%# .6g").Data());
        }
    }
    delete pars_it;
    cout << "\n  ======================================================\n" << endl;
}

/*Int_t ModelPDF::getNSigInRange(const Observable &var, const Double_t &x1, const Double_t &x2) {
    m_observables->get(var)->setRange("_nSigRange", x1, x2);
    RooAbsPdf *PDF = nullptr;
    switch( var ) {
        case MASS :
            PDF = m_totalMassSignalPDF;
            break;
        case MASSERROR :
            PDF = m_totalMassSignalErrorPDF;
            break;
        case TIME :
            PDF = m_totalTimeSignalPDF;
            break;
        case TIMEERROR :
            PDF = m_totalTimeSignalErrorPDF;
            break;
        case PT :
            PDF = m_ptSignalPDF;
            break;
    }
    RooAbsReal *integral = PDF->createIntegral(*m_observables->get(var), RooFit::NormSet(*m_observables->get(var)), RooFit::Range("_nSigRange"));
    Int_t n = (Int_t)(integral->getVal() * getPars("nSig")->getVal());
    delete integral;
    return n;
}

Int_t ModelPDF::getNBckInRange(const Observable &var, const Double_t &x1, const Double_t &x2) {
    m_observables->get(var)->setRange("_nBckRange", x1, x2);
    RooAbsPdf *PDF = nullptr;
    switch( var ) {
        case MASS :
            PDF = m_totalMassBackgroundPDF;
            break;
        case MASSERROR :
            PDF = m_totalMassBackgroundErrorPDF;
            break;
        case TIME :
            PDF = m_totalTimeBackgroundPDF;
            break;
        case TIMEERROR :
            PDF = m_totalTimeBackgroundErrorPDF;
            break;
        case PT :
            PDF = m_ptBackgroundPDF;
            break;
    }
    RooAbsReal *integral = PDF->createIntegral(*m_observables->get(var), RooFit::NormSet(*m_observables->get(var)), RooFit::Range("_nBckRange"));
    Int_t n = (Int_t)(integral->getVal() * getPars("nBck")->getVal());
    delete integral;
    return n;
}*/

RooRealVar* ModelPDF::getNSigInRange(const Double_t &x1, const Double_t &x2) {
    m_observables->get(MASS)->setRange("_nSigRange", x1, x2);
    RooAbsReal *integral = m_massSignalPDF->createIntegral(* m_observables->get(MASS), RooFit::NormSet(* m_observables->get(MASS)), RooFit::Range("_nSigRange"));
    RooRealVar *result = new RooRealVar("_nSigInRange", "Number of signal events in range", -RooNumber::infinity(), RooNumber::infinity());
    if(m_extendedFit){
        result->setVal(integral->getVal() * getPars("nSig")->getVal());
        result->setError(integral->getVal() * getPars("nSig")->getError());
    }else{
        result->setVal(integral->getVal() * getPars("nEvents")->getVal() * getPars("sigFrac")->getVal());
        result->setError(integral->getVal() * getPars("nEvents")->getVal() * getPars("sigFrac")->getError());
    }
    return result;
}

RooRealVar* ModelPDF::getNBkgInRange(const Double_t &x1, const Double_t &x2) {
    m_observables->get(MASS)->setRange("_nBckRange", x1, x2);
    RooAbsReal *integral = m_massBackgroundPDF->createIntegral(* m_observables->get(MASS), RooFit::NormSet(* m_observables->get(MASS)), RooFit::Range("_nBckRange"));
    RooRealVar *result = new RooRealVar("_nBkgInRange", "Number of background events in range", -RooNumber::infinity(), RooNumber::infinity());
    if(m_extendedFit){
        result->setVal(integral->getVal() * getPars("nBck")->getVal());
        result->setError(integral->getVal() * getPars("nBck")->getError());
    }else{
        result->setVal(integral->getVal() * getPars("nEvents")->getVal() * (1 - getPars("sigFrac")->getVal()));
        result->setError(integral->getVal() * getPars("nEvents")->getVal() * getPars("sigFrac")->getError());
    }
    return result;
}