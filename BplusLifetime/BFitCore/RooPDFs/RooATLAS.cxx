#include "TMath.h"

#include "RooATLAS.h"

//---

#ifdef __CINT__ /* HACK */
ClassImp(RooATLAS)
#endif /* __CINT__ HACK */

//---

RooATLAS::RooATLAS(const char *name, const char *title,
		   RooAbsReal &_x,
		   RooAbsReal &_x0, RooAbsReal &_N, RooAbsReal &_A,
		   RooAbsReal &_P4, RooAbsReal &_P3, RooAbsReal &_P2, RooAbsReal &_P1, RooAbsReal &_P0) : RooAbsPdf(name, title),
  x("x", "x", this, _x),
  x0("x0", "x0", this, _x0), N("N", "N", this, _N), A("A", "A", this, _A),
  P4("P4", "P4", this, _P4), P3("P3", "P3", this, _P3), P2("P2", "P2", this, _P2), P1("P1", "P1", this, _P1), P0("P0", "P0", this, _P0)
{
};


RooATLAS::RooATLAS(const RooATLAS &other, const char *name) : RooAbsPdf(other, name),
  x("x", this, other.x),
  x0("x0", this, other.x0), N("N", this, other.N), A("A", this, other.A),
  P4("P4", this, other.P4), P3("P3", this, other.P3), P2("P2", this, other.P2), P1("P1", this, other.P1), P0("P0", this, other.P0)
								    
{
};

//---

Double_t RooATLAS::evaluate() const
{
  Double_t X = (x - x0) / N;

  if(0.0 >= X)
    {
      return(0.0);
    }
  else
    {
    };

  Double_t X2 = X * X;
  Double_t X3 = X2 * X;
  Double_t X4 = X3 * X;

  Double_t Result = TMath::Power(X, A) * TMath::Exp((P4 * X4) + (P3 * X3) + (P2 * X2) + (P1 * X) + P0);

  if(true == TMath::Finite(Result))
    {
      return(Result);
    }
  else
    {
      return(0.0);
    };
};
