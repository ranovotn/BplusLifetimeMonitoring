#include "Observables.h"

Observables::~Observables() {
    TIterator *vars_it = m_vars.createIterator();
    for ( RooRealVar *v = (RooRealVar*)(vars_it->Next()); v != NULL ; v = (RooRealVar*)(vars_it->Next()) ) {
//         TString name = v->GetName();
        delete v;
//         cout << "INFO: Observable \"" << name << "\" deleted." << endl;
    }
    delete vars_it;
//     cout << "INFO: Observables \"" << m_name << "\" deleted." << endl;
}

void Observables::set(const Observable &var, const TString &varName, const TString &varTitle, const Double_t &varMin, const Double_t &varMax, const TString &varUnit) {
    TString name = varName;
    TString unit = varName;
    if ( TPRegexp("\\[\\[([-+/*]+\\s*\\d+\\.?\\d*e?[-+]?\\d*)?\\]\\]").Match(varUnit) == 0 ) {
        TPRegexp("(\\w+)\\s*([-+/*]+\\s*\\d+\\.?\\d*e?[-+]?\\d*)?").Substitute(name, "$1");
        TPRegexp("\\w+\\s*(([-+/*]+\\s*\\d+\\.?\\d*e?[-+]?\\d*)?)").Substitute(unit, "[[$1]]");
    } else {
        unit = "";
    }

    switch( var ) {
        case MASS :
            m_mass = new RooRealVar(name, varTitle, varMin, varMax, unit+( varUnit == "" ? "MeV" : varUnit ));
            m_vars.add(*m_mass);
            break;
        case MASSERROR :
            m_massError = new RooRealVar(name, varTitle, varMin, varMax, unit+( varUnit == "" ? "MeV" : varUnit ));
            m_vars.add(*m_massError);
            break;
        case TIME :
            m_time = new RooRealVar(name, varTitle, varMin, varMax, unit+( varUnit == "" ? "ps" : varUnit ));
            m_vars.add(*m_time);
            break;
        case TIMEERROR :
            m_timeError = new RooRealVar(name, varTitle, varMin, varMax, unit+( varUnit == "" ? "ps" : varUnit ));
            m_vars.add(*m_timeError);
            break;
        case PT :
            m_pt = new RooRealVar(name, varTitle, varMin, varMax, unit+( varUnit == "" ? "MeV" : varUnit ));
            m_vars.add(*m_pt);
            break;
    }
}

void Observables::set(const Observable &var, RooRealVar *rooVar) {
    switch( var ) {
        case MASS :
            m_mass = rooVar;
            m_vars.add(*m_mass);
            break;
        case MASSERROR :
            m_massError = rooVar;
            m_vars.add(*m_massError);
            break;
        case TIME :
            m_time = rooVar;
            m_vars.add(*m_time);
            break;
        case TIMEERROR :
            m_timeError = rooVar;
            m_vars.add(*m_timeError);
            break;
        case PT :
            m_pt = rooVar;
            m_vars.add(*m_pt);
            break;
    }
}

void Observables::add(RooRealVar *rooVar) {
    m_vars.add(*rooVar);
}

RooRealVar* Observables::get(const Observable &var) {
    switch( var ) {
        case MASS :
            return m_mass;
            break;
        case MASSERROR :
            return m_massError;
            break;
        case TIME :
            return m_time;
            break;
        case TIMEERROR :
            return m_timeError;
            break;
        case PT :
            return m_pt;
            break;
    }
}
