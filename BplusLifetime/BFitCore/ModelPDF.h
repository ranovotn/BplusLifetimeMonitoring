#ifndef MODELPDF
#define MODELPDF

#include <fstream>
#include <thread>

#include "TROOT.h"
#include "TStyle.h"
#include "TPRegexp.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TLegend.h"
#include "TApplication.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TF1.h"
#include "RooDataHist.h"
#include "RooPolynomial.h"
#include "RooChebychev.h"
#include "RooTruthModel.h"
#include "RooExponential.h"
#include "RooFFTConvPdf.h"
#include "RooFitResult.h"
#include "RooPlot.h"
#include "RooGaussian.h"
#include "RooDecay.h"
#include "RooAddition.h"
#include "RooAddPdf.h"
#include "RooAddModel.h"
#include "RooProdPdf.h"
#include "RooProduct.h"
#include "RooGaussModel.h"
#include "RooLognormal.h"
#include "RooHist.h"
#include "RooWorkspace.h"
#include "RooGenericPdf.h"
#include "RooLandau.h"
#include "RooCBShape.h"
// #include "RooTFnBinding.h"
// #include "RooClassFactory.h"

#include "RooPDFs/Roo2Lognormal.h"
#include "RooPDFs/Roo3Gamma.h"
#include "RooPDFs/Roo3GammaPt.h"
#include "RooPDFs/Roo2LognormalPt.h"
#include "RooPDFs/RooTanhPdf.h"
#include "RooPDFs/RooRelBW.h"
#include "RooPDFs/RooExpPol.h"
#include "RooPDFs/RooATLAS.h"
#include "RooPDFs/RooDSCB.h"

#include "Utils.h"

class ModelPDF {
public:
    ModelPDF(const TString &name, Data *data, Bool_t extendedFit = false) : m_name(name),m_data(data),m_extendedFit(extendedFit) {
        RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);
        TGaxis::SetMaxDigits(3);
        gStyle->SetLineScalePS(1.5);
    }
    ~ModelPDF();
    void setMassSignalPDF         (const TString &name, const TString &pdf);
    void setTimeSignalPDF         (const TString &name, const TString &pdf);
    void setPtSignalPDF           (const TString &name, const TString &pdf);
    void setMassSignalErrorPDF    (const TString &name, const TString &pdf);
    void setTimeSignalErrorPDF    (const TString &name, const TString &pdf);
    void setMassBackgroundPDF     (const TString &name, const TString &pdf);
    void setTimeBackgroundPDF     (const TString &name, const TString &pdf);
    void setPtBackgroundPDF       (const TString &name, const TString &pdf);
    void setMassBackgroundErrorPDF(const TString &name, const TString &pdf);
    void setTimeBackgroundErrorPDF(const TString &name, const TString &pdf);
    void setMassResolutionPDF     (const TString &name, const TString &pdf);
    TString* getMassSignalPDF         (void) { return m_massSignal; };
    TString* getTimeSignalPDF         (void) { return m_timeSignal; };
    TString* getPtSignalPDF           (void) { return m_ptSignal; };
    TString* getMassSignalErrorPDF    (void) { return m_massSignalError; };
    TString* getTimeSignalErrorPDF    (void) { return m_timeSignalError; };
    TString* getPtBackgroundPDF       (void) { return m_ptBackground; };
    TString* getMassBackgroundPDF     (void) { return m_massBackground; };
    TString* getTimeBackgroundPDF     (void) { return m_timeBackground; };
    TString* getMassBackgroundErrorPDF(void) { return m_massBackgroundError; };
    TString* getTimeBackgroundErrorPDF(void) { return m_timeBackgroundError; };
    TString* getMassResolutionPDF     (void) { return m_massResolution; };

// tmp
    RooAbsPdf* timeSignalErrorPDF    (void) { return m_totalTimeSignalErrorPDF; };
    RooAbsPdf* timeBackgroundErrorPDF(void) { return m_totalTimeBackgroundErrorPDF; };
// tmp

    RooAbsPdf* getTotalMassSignalPDF(void);
    void setMassSignalPDF         (TString *settings) { setMassSignalPDF(settings[0], settings[1]); };
    void setTimeSignalPDF         (TString *settings) { setTimeSignalPDF(settings[0], settings[1]); };
    void setPtSignalPDF           (TString *settings) { setPtSignalPDF(settings[0], settings[1]); };
    void setMassSignalErrorPDF    (TString *settings) { setMassSignalErrorPDF(settings[0], settings[1]); };
    void setTimeSignalErrorPDF    (TString *settings) { setTimeSignalErrorPDF(settings[0], settings[1]); };
    void setMassBackgroundPDF     (TString *settings) { setMassBackgroundPDF(settings[0], settings[1]); };
    void setTimeBackgroundPDF     (TString *settings) { setTimeBackgroundPDF(settings[0], settings[1]); };
    void setPtBackgroundPDF       (TString *settings) { setPtBackgroundPDF(settings[0], settings[1]); };
    void setMassBackgroundErrorPDF(TString *settings) { setMassBackgroundErrorPDF(settings[0], settings[1]); };
    void setTimeBackgroundErrorPDF(TString *settings) { setTimeBackgroundErrorPDF(settings[0], settings[1]); };
    void setMassResolutionPDF     (TString *settings) { setMassResolutionPDF(settings[0], settings[1]); };
    void fit(void);
    void drawPlot(const Observable &var, const Double_t &xMin, const Double_t &xMax, const Int_t &nBins, const TString &title = "");
    void drawPlot(const Observable &var, const Double_t &xMin, const Double_t &xMax, const TString &title = "");
    void drawPlot(const Observable &var, const Int_t &nBins, const TString &title = "");
    void drawPlot(const Observable &var, const TString &title = "");
    void drawPlotLog(const Observable &var, const Double_t &xMin, const Double_t &xMax, const Int_t &nBins, const TString &title = "");
    void drawPlotLog(const Observable &var, const Double_t &xMin, const Double_t &xMax, const TString &title = "");
    void drawPlotLog(const Observable &var, const Int_t &nBins, const TString &title = "");
    void drawPlotLog(const Observable &var, const TString &title = "");
    void drawPlotCore(const Bool_t &log, const Observable &var, const Double_t &xMin, const Double_t &xMax, const Int_t &nBins, const TString &title);
    void setPars(ModelPDF *model, const Bool_t &fixed = false);
    void setPars(const TString &varName, const Double_t &val, const Bool_t &fixed = false);
    void setPars(const TString &varName, const Double_t &val, const Double_t &x1, const Double_t &x2);
    void printResults(const TString &options = "");
    void printModel(const TString &options = "");
    Data* getData(void) { return m_data; }
    TString getName(void) { return m_name; }
    RooArgSet* getPars(void);
    RooArgSet* getInitialPars(void) { return m_initialPars; }
    RooRealVar* getPars(const TString &varName);
    RooRealVar* getInitialPars(const TString &varName);
    void setNCPU(const Int_t &nCPU) { m_nCPU = nCPU; }
    void saveResults(const TString &name);
    void readResults(const TString &name, TString prefix = "");
    void setRange(const Double_t &x1, const Double_t &x2) { m_xMin = x1; m_xMax = x2; }
    void saveToWorkspace(RooWorkspace *w) { w->import(*m_totalPDF); w->import(*m_dataset); } // Radek
    void addConditional(const Observable &var) { m_conditionalObservables.add(*m_observables->get(var)); }
    /*Int_t getNSigInRange(const Observable &var, const Double_t &x1, const Double_t &x2);
    Int_t getNBckInRange(const Observable &var, const Double_t &x1, const Double_t &x2);*/
    RooRealVar* getNSigInRange(const Double_t &x1, const Double_t &x2);
    RooRealVar* getNBkgInRange(const Double_t &x1, const Double_t &x2);
private:
    void createModel(void);
    TString m_name;
    Data *m_data;
    Observables *m_observables = m_data->getObservables();
    RooArgSet m_vars = m_observables->getVars();
    RooDataSet *m_dataset = m_data->getDataset();
    RooArgSet *m_pars        = nullptr;
    RooArgSet *m_initialPars = nullptr;
    RooArgSet *m_finalPars   = nullptr;
    RooArgList m_massSignalPDFs;
    RooArgList m_massSignalFracs;
    // RooArgList m_massResolutionPDFs;
    // RooArgList m_massResolutionFracs;
    RooArgList m_massBackgroundPDFs;
    RooArgList m_massBackgroundFracs;
    RooArgList m_timeSignalPDFs;
    RooArgList m_timeSignalFracs;
    RooArgList m_timeBackgroundPDFs;
    RooArgList m_timeBackgroundFracs;
    RooResolutionModel *m_massResolutionPDF  = nullptr;
    RooResolutionModel *m_timeResolutionPDF  = nullptr;
    RooAbsPdf *m_massSignalCorePDF           = nullptr;
    RooAbsPdf *m_massSignalPDF               = nullptr;
    RooAbsPdf *m_massSignalErrorPDF          = nullptr;
    RooAbsPdf *m_timeSignalPDF               = nullptr;
    RooAbsPdf *m_timeSignalErrorPDF          = nullptr;
    RooAbsPdf *m_ptSignalPDF                 = nullptr;
    RooAbsPdf *m_signalPDF                   = nullptr;
    RooAbsPdf *m_massBackgroundPDF           = nullptr;
    RooAbsPdf *m_massBackgroundErrorPDF      = nullptr;
    RooAbsPdf *m_timeBackgroundPDF           = nullptr;
    RooAbsPdf *m_timeBackgroundErrorPDF      = nullptr;
    RooAbsPdf *m_ptBackgroundPDF             = nullptr;
    RooAbsPdf *m_backgroundPDF               = nullptr;
    RooAbsPdf *m_totalPDF                    = nullptr;
    RooAbsPdf *m_totalMassSignalPDF          = nullptr;
    RooAbsPdf *m_totalTimeSignalPDF          = nullptr;
    RooAbsPdf *m_totalMassBackgroundPDF      = nullptr;
    RooAbsPdf *m_totalTimeBackgroundPDF      = nullptr;
    RooAbsPdf *m_totalMassSignalErrorPDF     = nullptr;
    RooAbsPdf *m_totalTimeSignalErrorPDF     = nullptr;
    RooAbsPdf *m_totalMassBackgroundErrorPDF = nullptr;
    RooAbsPdf *m_totalTimeBackgroundErrorPDF = nullptr;
    TString m_massSignal[2];
    TString m_timeSignal[2];
    TString m_ptSignal[2];
    TString m_massSignalError[2];
    TString m_timeSignalError[2];
    TString m_massBackground[2];
    TString m_timeBackground[2];
    TString m_ptBackground[2];
    TString m_massBackgroundError[2];
    TString m_timeBackgroundError[2];
    TString m_massResolution[2];
    Bool_t m_extendedFit;
    RooArgSet m_conditionalObservables; // HACK ?
    Bool_t m_useProjWDataMass = false;
    Bool_t m_useProjWDataTime = false;
    RooFitResult *m_fitResult = nullptr;
    Int_t m_nCPU = ( std::thread::hardware_concurrency() > 1 ? std::thread::hardware_concurrency() - 1 : 1 );
    Double_t m_xMin = +1.;
    Double_t m_xMax = -1.;
};

#endif
