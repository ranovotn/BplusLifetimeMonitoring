#ifndef INCLUDEALLSOURCES
#define INCLUDEALLSOURCES

#include "TSystem.h"
#include "Math/ChebyshevPol.h"

#include "Utils.h"
#include "Observables.h"
#include "Data.h"
#include "ModelPDF.h"

#include "Utils.cxx"
#include "Observables.cxx"
#include "Data.cxx"
#include "ModelPDF.cxx"

#include "RooPDFs/Roo3Gamma.h"
#include "RooPDFs/Roo3GammaPt.h"
#include "RooPDFs/Roo2LognormalPt.h"
#include "RooPDFs/RooTanhPdf.h"
#include "RooPDFs/RooRelBW.h"
#include "RooPDFs/RooExpPol.h"
#include "RooPDFs/RooATLAS.h"
#include "RooPDFs/RooDSCB.h"

#include "AtlasStyle/AtlasStyle.h"
#include "AtlasStyle/AtlasUtils.h"
#include "AtlasStyle/AtlasLabels.h"

// #if defined(__CLING__) && !defined(__ROOTCLING__)

#include "RooPDFs/Roo3Gamma.cxx"
#include "RooPDFs/Roo3GammaPt.cxx"
#include "RooPDFs/Roo2Lognormal.cxx"
#include "RooPDFs/Roo2LognormalPt.cxx"
#include "RooPDFs/RooTanhPdf.cxx"
#include "RooPDFs/RooRelBW.cxx"
#include "RooPDFs/RooExpPol.cxx"
#include "RooPDFs/RooATLAS.cxx"
#include "RooPDFs/RooDSCB.cxx"

#include "AtlasStyle/AtlasStyle.C"
#include "AtlasStyle/AtlasUtils.C"
#include "AtlasStyle/AtlasLabels.C"

// #endif /* defined(__CLING__) && !defined(__ROOTCLING__) */

#endif /* INCLUDEALLSOURCES */
