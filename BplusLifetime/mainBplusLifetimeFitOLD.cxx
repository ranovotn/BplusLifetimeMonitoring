#include "BFitCore/IncludeAllSources.h"

const Double_t PDGBplusMass = 5279.3; // (5279.25 +/- 0.26) MeV
const Double_t PDGBplusTau  = 1.64; // (1.638 +/- 0.004) ps

void BplusFitRD(const TString &name = "results/Bplus2015RD", TString cut = "", const TString &file = "/home/radek_novotny/analysis/CERN_git/data/BPlus/BPlustNtuple2017.root") {
    const time_t startTime = TTimeStamp().GetSec();
    checkDir(name);
    TString suffix = "test_";

    Bool_t fitIsOk = true;
    const TString defaultCut = "( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)<=1.05 ) && ( Jpsi_mass>2959 && Jpsi_mass<3229 ) ) || ( ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) && ( Jpsi_mass>2852 && Jpsi_mass<3332 ) ) || ( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) || ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)<=1.05 ) ) && ( Jpsi_mass>2913 && Jpsi_mass<3273 ) ) ) && B_mu1_pT>6000 && B_mu2_pT>6000 && B_trk_pT>3000 && B_chi2_ndof<3.0";

    const Bool_t massPCE  = false;
    const Bool_t massOnly = false;
    const Bool_t sample   = false;
    const TString massGausses = "double";
//     const TString massGausses = "triple";

    if ( ! cut.IsNull() ) cut += " && ";
    cut += defaultCut;

    Observables *observables = new Observables("observables");
    observables->set(MASS,      "B_mass", "#font[52]{m}(#font[52]{B^{#pm}})", 5000., 5700.); // 5560.
    observables->set(MASSERROR, "B_mass_err", "#font[52]{#sigma_{m}}(#font[52]{B^{#pm}})", 0., 100.);
    observables->set(TIME,      "B_tau_MinA0ConstM", "#font[52]{t}(#font[52]{B^{#pm}})", -2., 18.); // -> 30.
    observables->set(TIMEERROR, "B_tau_MinA0ConstM_err", "#font[52]{#sigma_{t}}(#font[52]{B^{#pm}})", .001, .251); // -> 1.
    
    Data *data = new Data("data", file, "BplusBestChi BplusTriggers", observables, cut);
    if(data->isEmpty()){
        cout << "ERROR: Empty dataset" <<endl;
        return;
    }
    if ( sample ) {
        Data *dataRandom = data->randomSample("dataRandomSample", 10000);
        data = dataRandom;
        suffix += "_sample";
    }
    Data *dataSidebands = data->reduce("dataSidebands", "(B_mass > 5460)");
    
    ModelPDF *rightSidebandBackgroundModel = new ModelPDF("rightSidebandBackgroundModel", dataSidebands);
    rightSidebandBackgroundModel->setMassBackgroundPDF("rSidebandBck", "exponential");
    rightSidebandBackgroundModel->setPars("rightSidebandBackgroundModel_rSidebandBck_slope", -0.01, -1.,0.0);
    rightSidebandBackgroundModel->fit();

    cout << rightSidebandBackgroundModel->getPars("rSidebandBck_slope")->getVal() <<endl;

    ModelPDF *simpleMassModel = new ModelPDF("simpleMass", data);
    if ( massPCE || ! massOnly ) {
        simpleMassModel->setMassSignalPDF("massSig", "doubleGauss");
        simpleMassModel->setMassBackgroundPDF("massBck", "exponentialAndTanh");

        Bool_t simpleMassFitSucceed = false;
        for(auto tmpScale :{-0.05, -0.018,-0.026}){
            for(auto tmpBckFrac :{0.9,0.8,0.7,0.6}){
                simpleMassModel->setPars("massBck_fraction", tmpBckFrac);
                for(auto tmpSigFrac :{0.4,0.5,0.6,0.7,0.8,0.3,0.2}){
                    simpleMassModel->setPars("sigFrac", tmpSigFrac);
                    simpleMassModel->setPars("massBck_scale", tmpScale,-1.0,1.0);
                    simpleMassModel->setPars("massSig_mean", PDGBplusMass, PDGBplusMass-50., PDGBplusMass+50.);
                    simpleMassModel->setPars("massSig_fraction", .2);
                    simpleMassModel->setPars("massSig_sigma1", 20.);
                    simpleMassModel->setPars("massSig_sigma2", 50.);
                    simpleMassModel->setPars("massBck_slope", rightSidebandBackgroundModel->getPars("rSidebandBck_slope")->getVal(),true);
                    simpleMassModel->setPars("massBck_offset", 5150.,5150.-100,5150.+100);
        
                    simpleMassModel->fit();
                    cout << "INFO: fit EDM =" << simpleMassModel->getPars("fitEDM")->getVal() <<endl;
                    if((simpleMassModel->getPars("fitEDM")->getVal() != 0.5) && (simpleMassModel->getPars("fitEDM")->getVal() < 10)){
                        simpleMassFitSucceed = true;
                        break;
                    }
                }
                if(simpleMassFitSucceed)break;
            }
            if(simpleMassFitSucceed)break;
        }
        if(!simpleMassFitSucceed){
            cout << "Error: failed to fit simple mass model"<< endl;
            fitIsOk = false;
            //return;
        }
            simpleMassModel->printResults();
    }

    TH1* hh_dataMass = (data->getDataset())->createHistogram("B_mass",50);
    hh_dataMass->GetXaxis()->SetRangeUser(5010, PDGBplusMass);
    int binmin = hh_dataMass->GetMinimumBin();
    double x = hh_dataMass->GetXaxis()->GetBinCenter(binmin);
    //hh_dataMass->Draw();
    cout <<"INFO: min mass value found: " <<x << endl;
    //Double_t massCutValue = x+50;
    Double_t massCutValue = x+30;
    Data *dataSignal = data->reduce("dataSignal", Form("(B_mass > %f)",massCutValue));
    ModelPDF *timeErrorBckModel = new ModelPDF("timeErrorBck", dataSidebands);    
    Bool_t timeErrorBckFitSucceed = false;
    
    TH1* hh_dataTimeErrorSideband = (dataSidebands->getDataset())->createHistogram("B_tau_MinA0ConstM_err",50);
    hh_dataTimeErrorSideband->GetXaxis()->SetRangeUser(.001, .251);
    int timeErroSidebandBinMax = hh_dataTimeErrorSideband->GetMaximumBin();
    double maximumTimeErrorValueSideband = hh_dataTimeErrorSideband->GetXaxis()->GetBinCenter(timeErroSidebandBinMax);
    //cout << maximumTimeErrorValueSideband <<endl;

    if ( ! massOnly ) {
        timeErrorBckModel->setTimeBackgroundErrorPDF("timeErrorBck", "2Lognormal");
        for(auto tmpMean :{maximumTimeErrorValueSideband,maximumTimeErrorValueSideband*0.8,maximumTimeErrorValueSideband*1.2}){
            for(auto tmpFraction :{0.9,0.8,0.7}){
                for(auto tmpSigma :{0.3,0.2,0.4}){
                    timeErrorBckModel->setPars("timeErrorBck_mean1", tmpMean,tmpMean*0.5,tmpMean*2.0);
                    timeErrorBckModel->setPars("timeErrorBck_mean2", tmpMean,tmpMean*0.5,tmpMean*2.0);
                    timeErrorBckModel->setPars("timeErrorBck_sigma1", tmpSigma*2.333, 0.1, 2.0);
                    timeErrorBckModel->setPars("timeErrorBck_sigma2", tmpSigma, 0.1, 2.0);
                    timeErrorBckModel->setPars("timeErrorBck_fraction", tmpFraction);
                    if(fitIsOk)timeErrorBckModel->fit();
            
                    if((timeErrorBckModel->getPars("fitEDM")->getVal() != 0.5) && (timeErrorBckModel->getPars("fitEDM")->getVal() < 100)){
                        timeErrorBckFitSucceed = true;
                        break;
                    }
                }
                if(timeErrorBckFitSucceed)break;
            }
            if(timeErrorBckFitSucceed)break;
        }
    }
    if(!timeErrorBckFitSucceed){
            cout << "Error: failed to fit background error time model: Fit EDM = "<< timeErrorBckModel->getPars("fitEDM")->getVal()<< endl;
            //return;
            fitIsOk = false;
        }

    Double_t nSig = simpleMassModel->getNSigInRange(massCutValue,5700)->getVal();
    Double_t nBck = simpleMassModel->getNBkgInRange(massCutValue,5700)->getVal();
    cout << "INFO: sigFracInRange = "<< nSig/(nSig+nBck)<<endl;
    
    ModelPDF *timeErrorModel = new ModelPDF("timeError", dataSignal);
    Bool_t timeErrorSigFitSucceed = false;
    if ( ! massOnly ) {
        timeErrorModel->setTimeBackgroundErrorPDF(timeErrorBckModel->getTimeBackgroundErrorPDF());
        timeErrorModel->setTimeSignalErrorPDF("timeErrorSig", "2Lognormal");
        timeErrorModel->setPars(timeErrorBckModel, true);
        timeErrorModel->setPars("sigFrac", nSig/(nSig+nBck), true);
            
        for(auto tmpMean :{maximumTimeErrorValueSideband,maximumTimeErrorValueSideband*0.8,maximumTimeErrorValueSideband*1.2}){
            for(auto tmpVariable :{0.9,0.8,0.7}){
                for(auto tmpSigma :{0.3,0.2,0.4}){
                    timeErrorModel->setPars("timeErrorSig_fraction", tmpVariable);
                    timeErrorModel->setPars("timeErrorSig_sigma1", tmpSigma*2.333, 0.1, 3.0);
                    timeErrorModel->setPars("timeErrorSig_sigma2", tmpSigma, 0.1, 3.0);
                    timeErrorModel->setPars("timeErrorSig_mean1", tmpMean,tmpMean*0.5,tmpMean*2.0);
                    timeErrorModel->setPars("timeErrorSig_mean2", tmpMean,tmpMean*0.5,tmpMean*2.0);
                    if(fitIsOk)timeErrorModel->fit();
                    if((timeErrorModel->getPars("fitEDM")->getVal() != 0.5) && (timeErrorModel->getPars("fitEDM")->getVal() < 300)){
                            timeErrorSigFitSucceed = true;
                            break;
                    }
                }
                if(timeErrorSigFitSucceed) break;   
            }
            if(timeErrorSigFitSucceed) break;   
        }

        if(!timeErrorSigFitSucceed){
            cout << "Error: failed to fit signal error time model: Fit EDM = "<< timeErrorModel->getPars("fitEDM")->getVal()<< endl;
            //return;
            fitIsOk = false;
        }

    }

    ModelPDF *massErrorBckModel = new ModelPDF("massErrorBck", dataSidebands);
    if ( massPCE ) {
        suffix += "_massPCE";
        massErrorBckModel->setMassBackgroundErrorPDF("massErrorBck", "2Gamma");
        massErrorBckModel->setPars("massErrorBck_a1", 2.);
        massErrorBckModel->setPars("massErrorBck_a2", 6.);
        massErrorBckModel->setPars("massErrorBck_a3", 10.);
        massErrorBckModel->setPars("massErrorBck_b1", 4.);
        massErrorBckModel->setPars("massErrorBck_b2", 6.);
        massErrorBckModel->setPars("massErrorBck_b3", 3.);
        massErrorBckModel->setPars("massErrorBck_c1", 13.);
        massErrorBckModel->setPars("massErrorBck_c2", 0.);
        massErrorBckModel->setPars("massErrorBck_c3", 1.);
        massErrorBckModel->fit();
    }

    ModelPDF *massErrorModel = new ModelPDF("massError", dataSignal);
    if ( massPCE ) {
        massErrorModel->setMassBackgroundErrorPDF(massErrorBckModel->getMassBackgroundErrorPDF());
        massErrorModel->setMassSignalErrorPDF("massErrorSig", "2Gamma");
        massErrorModel->setPars(massErrorBckModel, true);
        massErrorModel->setPars("massErrorSig_a1", 4.);
        massErrorModel->setPars("massErrorSig_a2", 8.);
        massErrorModel->setPars("massErrorSig_a3", 3.);
        massErrorModel->setPars("massErrorSig_b1", 69.);
        massErrorModel->setPars("massErrorSig_b2", 3.);
        massErrorModel->setPars("massErrorSig_b3", 3.);
        massErrorModel->setPars("massErrorSig_c1", 1.);
        massErrorModel->setPars("massErrorSig_c2", 0.);
        massErrorModel->setPars("massErrorSig_c3", 1.);
        massErrorModel->setPars("sigFrac", simpleMassModel->getPars("sigFrac")->getVal(), true);
        massErrorModel->fit();
    }

    ModelPDF *massTimeModel = new ModelPDF("massTime", dataSignal);
    if ( massPCE ) {
        massTimeModel->setMassSignalPDF("massSig", "gaussPCE");
        massTimeModel->setMassSignalErrorPDF(massErrorModel->getMassSignalErrorPDF());
        massTimeModel->setMassBackgroundErrorPDF(massErrorModel->getMassBackgroundErrorPDF());
    } else {
        massTimeModel->setMassSignalPDF("massSig", massGausses+"Gauss");
        suffix += "_"+massGausses+"Gauss";
    }
    massTimeModel->setMassBackgroundPDF("massBck", "linear");
    if ( ! massOnly ) {
        massTimeModel->setTimeSignalPDF("timeSig","singleExponential");
        massTimeModel->setTimeSignalErrorPDF(timeErrorModel->getTimeSignalErrorPDF());
        massTimeModel->setTimeBackgroundPDF("timeBck","promptExponential2PosNeg");
        massTimeModel->setTimeBackgroundErrorPDF(timeErrorModel->getTimeBackgroundErrorPDF());
    }
    

    if ( massPCE ) massTimeModel->setPars(massErrorModel, true);
    Bool_t fullFitSucceed = false;
    
    for(auto tmpTau :{1.5,1.6,1.7}){
            for(auto tmpFracScale :{1.0,0.8,1.2,1.4,1.6}){
            massTimeModel->setPars("sigFrac",tmpFracScale*nSig/(nSig+nBck),0.1,0.9);
            massTimeModel->setPars("timeSig_tau", tmpTau, 1., 2.);
            massTimeModel->setPars("timeBck_tauPos1", 1.5, .05, 3.);
            massTimeModel->setPars("timeBck_tauPos2",  .15, .01, 3.);
            massTimeModel->setPars("timeBck_tauNeg",  .06, .005, 3.);
            massTimeModel->setPars("timeSig_scaleFactor",  1.08, 0.9, 1.2);
            massTimeModel->setPars("massSig_mean", PDGBplusMass, PDGBplusMass-50., PDGBplusMass+50.);
            massTimeModel->setPars("massSig_fraction", simpleMassModel->getPars("massSig_fraction")->getVal());
            massTimeModel->setPars("massSig_sigma1", simpleMassModel->getPars("massSig_sigma1")->getVal());
            massTimeModel->setPars("massSig_sigma2", simpleMassModel->getPars("massSig_sigma2")->getVal());
            //massTimeModel->setPars("massBck_shape", simpleMassModel->getPars("massBck_shape")->getVal());
            //massTimeModel->setPars("massBck_shape", 0.0,-1.0,1.0);
            massTimeModel->setPars("massBck_shape", 0.0,-1.0,0.05);
    
            massTimeModel->setPars(timeErrorModel, true);
            if(fitIsOk)massTimeModel->fit();
            if((massTimeModel->getPars("fitEDM")->getVal() != 0.5) && (massTimeModel->getPars("fitEDM")->getVal() < 100)){
                    fullFitSucceed = true;
                    break;
            }
        }
        if(fullFitSucceed)break;
        }
    if(!fullFitSucceed){
        cout << "Error: Full fit did not converge"<<endl; 
    }
    if ( massPCE && ! massOnly ) {
        TCanvas* can1 = new TCanvas("can1", "Canvas 1", 4*600, 2*800);
        can1->Divide(4,2);
        can1->cd(1);
        timeErrorBckModel->drawPlot(TIMEERROR, 100, "Lifetime Error in Sidebands");
        can1->cd(2);
        timeErrorModel->drawPlot(TIMEERROR, 100, "Lifetime Error in All Data");
        can1->cd(3);
        massErrorBckModel->drawPlot(MASSERROR, 100, "Mass Error in Sidebands");
        can1->cd(4);
        massErrorModel->drawPlot(MASSERROR, 100, "Mass Error in All Data");
        can1->cd(5);
        simpleMassModel->drawPlot(MASS, "Simple Mass (sigFrac for Punzi)");
        can1->cd(6);
        massTimeModel->drawPlot(MASS, "Mass-Lifetime Fit (Mass)");
        can1->cd(7);
        massTimeModel->drawPlotLog(TIME, -1., 7., "Mass-Lifetime Fit (Lifetime)");
        can1->cd(8);
        massTimeModel->drawPlotLog(TIME, -.5, 1.5, "Mass-Lifetime Fit (Lifetime \"Zoom\")");
        can1->SaveAs(name+suffix+".png");
        delete can1;
    } else if ( massPCE && massOnly ) {
        suffix += "_massOnly";
        TCanvas* can1 = new TCanvas("can1", "Canvas 1", 2*600, 2*800);
        can1->Divide(2,2);
        can1->cd(1);
        massErrorBckModel->drawPlot(MASSERROR, 100, "Mass Error in Sidebands");
        can1->cd(2);
        massErrorModel->drawPlot(MASSERROR, 100, "Mass Error in All Data");
        can1->cd(3);
        simpleMassModel->drawPlot(MASS, "Simple Mass (sigFrac for Punzi)");
        can1->cd(4);
        massTimeModel->drawPlot(MASS, "Mass Fit");
        can1->SaveAs(name+suffix+".png");
        delete can1;
    } else if ( ! massPCE && massOnly ) {
        suffix += "_massOnly";
        TCanvas* can1 = new TCanvas("can1", "Canvas 1", 1*600, 1*800);
        massTimeModel->drawPlot(MASS, "Mass-Lifetime Fit (Mass)");
        can1->SaveAs(name+suffix+".png");
        delete can1;
    } else {
        TCanvas* can1 = new TCanvas("can1", "Canvas 1", 3*600, 2*800);
        can1->Divide(3,2);
        can1->cd(1);
        simpleMassModel->drawPlot(MASS, "Simple Mass (sigFrac for Punzi)");
        //rightSidebandBackgroundModel->drawPlot(MASS, "Simle right sideband bck");
        can1->cd(2);
        timeErrorBckModel->drawPlot(TIMEERROR, 100, "Lifetime Error in Sidebands");
        can1->cd(3);
        timeErrorModel->drawPlot(TIMEERROR, 100, "Lifetime Error in All Data");
        can1->cd(4);
        massTimeModel->drawPlot(MASS, "Mass-Lifetime Fit (Mass)");
        can1->cd(5);
        massTimeModel->drawPlotLog(TIME, -1., 12., "Mass-Lifetime Fit (Lifetime)");
        can1->cd(6);
        massTimeModel->drawPlotLog(TIME, -.5, 1.5, "Mass-Lifetime Fit (Lifetime \"Zoom\")");
        can1->SaveAs(name+suffix+".png");
        delete can1;
    }

    if ( massPCE ) {
        massErrorBckModel->printResults();
        massErrorModel->printResults();
    }
    if ( ! massOnly ) {
        timeErrorBckModel->printResults();
        timeErrorModel->printResults();
    }
    massTimeModel->printModel();
    massTimeModel->printResults();

    /*delete massErrorBckModel;
    delete massErrorModel;
    delete timeErrorBckModel;
    delete timeErrorModel;
    delete massTimeModel;
    delete simpleMassModel;
    delete data;
    delete observables;*/

    cout << "INFO: Total time " << printTime( TTimeStamp().GetSec() - startTime ) << endl;
}

void mainBplusLifetimeFitOLD() {
    std::list<TString> triggerList = {""};


    Int_t i =0;
    for(auto trigger : triggerList){
        BplusFitRD("results/Bplus2016RD_periodI_OLD","HLT_2mu6_bBmumuxv2_delayed == 1 && (run_number > 307124) && (run_number < 308084)","/data0/novotnyr/data/BPlus/data2016MergeRel20.root");     
    }
    return;
}

