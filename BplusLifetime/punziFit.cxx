#include "BFitCore/IncludeAllSources.h"

const Double_t PDGBplusMass = 5279.3; // (5279.31 +/- 0.15) MeV
const Double_t PDGBplusTau  = 1.64; // (1.638 +/- 0.004) ps

//void BplusFitRD(const TString &name = "results/Bplus2015RD", TString cut = "", const TString &file = "local.data15_13TeV.periodAtoJpart.bphy2.GRLinfoUpdate.ntuple.2.root") {
//void BplusFitRD(const TString &name = "results/Bplus2015RD", TString cut = "", const TString &file = "/data0/novotnyr/data/BPlus/BPlustNtuple2017.root") {
//void BplusFitRD(const TString &name = "results/Bplus2015RD", TString cut = "", const TString &file = "/data0/novotnyr/data/BPlus/BPlustNtuple2017.root") {
void BplusFitRD(const TString &name = "results/Bplus2015RD", TString cut = "", const TString &file = "/home/radek_novotny/analysis/CERN_git/data/BPlus/BPlustNtuple2017.root", Double_t paramLimit = 1.5) {
    const time_t startTime = TTimeStamp().GetSec();
    checkDir(name);
    TString suffix = "";

    //const TString defaultCut = "( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)<=1.05 ) && ( Jpsi_mass>2959 && Jpsi_mass<3229 ) ) || ( ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) && ( Jpsi_mass>2852 && Jpsi_mass<3332 ) ) || ( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) || ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)<=1.05 ) ) && ( Jpsi_mass>2913 && Jpsi_mass<3273 ) ) ) && pass_GRL && B_mu1_pT>4000 && B_mu2_pT>4000 && B_trk_pT>3000 && B_chi2_ndof<3.0";
    //const TString defaultCut = "( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)<=1.05 ) && ( Jpsi_mass>2959 && Jpsi_mass<3229 ) ) || ( ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) && ( Jpsi_mass>2852 && Jpsi_mass<3332 ) ) || ( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) || ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)<=1.05 ) ) && ( Jpsi_mass>2913 && Jpsi_mass<3273 ) ) ) && B_mu1_pT>4000 && B_mu2_pT>4000 && B_trk_pT>3000 && B_chi2_ndof<3.0";
    const TString defaultCut = "( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)<=1.05 ) && ( Jpsi_mass>2959 && Jpsi_mass<3229 ) ) || ( ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) && ( Jpsi_mass>2852 && Jpsi_mass<3332 ) ) || ( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) || ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)<=1.05 ) ) && ( Jpsi_mass>2913 && Jpsi_mass<3273 ) ) ) && B_mu1_pT>6000 && B_mu2_pT>6000 && B_trk_pT>3000 && B_chi2_ndof<3.0";
    //const TString defaultCut = "( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)<=1.05 ) && ( Jpsi_mass>2959 && Jpsi_mass<3229 ) ) || ( ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) && ( Jpsi_mass>2852 && Jpsi_mass<3332 ) ) || ( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) || ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)<=1.05 ) ) && ( Jpsi_mass>2913 && Jpsi_mass<3273 ) ) ) && B_mu1_pT>4000 && B_mu2_pT>4000 && B_trk_pT>3000 && B_chi2_ndof<3.0";

    const Bool_t massPCE  = false;
    const Bool_t massOnly = false;
    const Bool_t sample   = false;
//    const TString massGausses = "double";
     const TString massGausses = "triple";

    if ( ! cut.IsNull() ) cut += " && ";
    cut += defaultCut;

    Observables *observables = new Observables("observables");
    observables->set(MASS,      "B_mass", "#font[52]{m}(#font[52]{B^{#pm}})", 5000., 5700.); // 5560.
    observables->set(MASSERROR, "B_mass_err", "#font[52]{#sigma_{m}}(#font[52]{B^{#pm}})", 0., 100.);
    observables->set(TIME,      "B_tau_MinA0ConstM", "#font[52]{t}(#font[52]{B^{#pm}})", -2., 18.); // -> 30.
    observables->set(TIMEERROR, "B_tau_MinA0ConstM_err", "#font[52]{#sigma_{t}}(#font[52]{B^{#pm}})", .001, .251); // -> 1.

    Data *data = new Data("data", file, "BplusBestChi BplusTriggers", observables, cut);
    if ( sample ) {
        Data *dataRandom = data->randomSample("dataRandomSample", 10000);
        data = dataRandom;
        suffix += "_sample";
    }
    Data *dataSidebands = data->reduce("dataSidebands", "(B_mass < 5100 || B_mass > 5460)");

    ModelPDF *simpleMassModel = new ModelPDF("simpleMass", data);
    /*if ( massPCE || ! massOnly ) {
        simpleMassModel->setMassSignalPDF("massSig", "doubleGauss");
        simpleMassModel->setMassBackgroundPDF("massBck", "exponentialAndTanh");
        simpleMassModel->setPars("massSig_mean", PDGBplusMass, PDGBplusMass-50., PDGBplusMass+50.);
        simpleMassModel->setPars("massSig_fraction", .2);
        simpleMassModel->setPars("massSig_fraction1", .2);
        simpleMassModel->setPars("massSig_fraction2", .4);
        simpleMassModel->setPars("massSig_sigma1", 20.);
        simpleMassModel->setPars("massSig_sigma2", 50.);
        simpleMassModel->setPars("massSig_sigma3", 30.);
        simpleMassModel->setPars("massBck_slope", -0.002);
        simpleMassModel->setPars("massBck_scale", -0.025);
        simpleMassModel->setPars("massBck_offset", 5150.);
        simpleMassModel->setPars("massBck_fraction1", .8);
        simpleMassModel->setPars("massBck_fraction2", .08);
        simpleMassModel->setPars("sigFrac", .6);
        simpleMassModel->fit();
        simpleMassModel->printResults();
    }*/
    if ( massPCE || ! massOnly ) {
        /*simpleMassModel->setMassSignalPDF("massSig", "doubleGauss");
        simpleMassModel->setMassBackgroundPDF("massBck", "exponentialAndConstant");
        simpleMassModel->setPars("massSig_mean", PDGBplusMass, PDGBplusMass-50., PDGBplusMass+50.);
        simpleMassModel->setPars("massSig_fraction", .2);
        simpleMassModel->setPars("massSig_fraction1", .2);
        simpleMassModel->setPars("massSig_fraction2", .4);
        simpleMassModel->setPars("massSig_sigma1", 10.,0,70);
        simpleMassModel->setPars("massSig_sigma2", 50.,0,70);
        simpleMassModel->setPars("massSig_sigma3", 30.);
        simpleMassModel->setPars("massBck_slope", -0.002);
        simpleMassModel->setPars("massBck_fraction", .8);
        simpleMassModel->setPars("sigFrac", .6);
        simpleMassModel->fit();
        simpleMassModel->printResults();*/
        simpleMassModel->setMassSignalPDF("massSig", "doubleGauss");
        simpleMassModel->setMassBackgroundPDF("massBck", "exponentialAndTanh");
        simpleMassModel->setPars("massSig_mean", PDGBplusMass, PDGBplusMass-50., PDGBplusMass+50.);
        simpleMassModel->setPars("massSig_fraction", .2);
        simpleMassModel->setPars("massSig_fraction1", .2);
        simpleMassModel->setPars("massSig_fraction2", .4);
        simpleMassModel->setPars("massSig_sigma1", 20.);
        simpleMassModel->setPars("massSig_sigma2", 50.);
        simpleMassModel->setPars("massSig_sigma3", 30.);
        simpleMassModel->setPars("massBck_slope", -0.0012);
        simpleMassModel->setPars("massBck_scale", -0.025);
        simpleMassModel->setPars("massBck_offset", 5150.);
        simpleMassModel->setPars("massBck_fraction1", .9);
        simpleMassModel->setPars("massBck_fraction2", .08);
        simpleMassModel->setPars("sigFrac", .4);
        simpleMassModel->fit();
        simpleMassModel->printResults();
    }
    ModelPDF *timeErrorBckModel = new ModelPDF("timeErrorBck", dataSidebands);    
    /*if ( ! massOnly ) {
        timeErrorBckModel->setTimeBackgroundErrorPDF("timeErrorBck", "2Gamma");
        timeErrorBckModel->setPars("timeErrorBck_a1", 7.4);
        timeErrorBckModel->setPars("timeErrorBck_a2", 1.8);
        timeErrorBckModel->setPars("timeErrorBck_a3", 6.5);
        timeErrorBckModel->setPars("timeErrorBck_b1", .007);
        timeErrorBckModel->setPars("timeErrorBck_b2", .025);
        timeErrorBckModel->setPars("timeErrorBck_b3", .020);
        timeErrorBckModel->setPars("timeErrorBck_c1", .01);
        timeErrorBckModel->setPars("timeErrorBck_c2", .01);
        timeErrorBckModel->setPars("timeErrorBck_c3", .01);
        timeErrorBckModel->fit();
    }*/
    /*if ( ! massOnly ) {
       timeErrorBckModel->setTimeBackgroundErrorPDF("timeErrorBck", "2Gamma");
        timeErrorBckModel->setPars("timeErrorBck_a1", 6.4);
        timeErrorBckModel->setPars("timeErrorBck_a2", 0.8);
        timeErrorBckModel->setPars("timeErrorBck_a3", 0.01);
        timeErrorBckModel->setPars("timeErrorBck_b1", .007);
        timeErrorBckModel->setPars("timeErrorBck_b2", .025);
        timeErrorBckModel->setPars("timeErrorBck_b3", 1.020);
        timeErrorBckModel->setPars("timeErrorBck_c1", .01);
        timeErrorBckModel->setPars("timeErrorBck_c2", .01);
        timeErrorBckModel->setPars("timeErrorBck_c3", .4);
        timeErrorBckModel->fit();
    }*/
    if ( ! massOnly ) {
       timeErrorBckModel->setTimeBackgroundErrorPDF("timeErrorBck", "2Gamma");
        timeErrorBckModel->setPars("timeErrorBck_a1", 9.4);
        timeErrorBckModel->setPars("timeErrorBck_a2", 8.8);
        timeErrorBckModel->setPars("timeErrorBck_a3", 1.2);
        timeErrorBckModel->setPars("timeErrorBck_b1", .004);
        timeErrorBckModel->setPars("timeErrorBck_b2", .005);
        timeErrorBckModel->setPars("timeErrorBck_b3", .020);
        timeErrorBckModel->setPars("timeErrorBck_c1", .01,.0005,1);
        timeErrorBckModel->setPars("timeErrorBck_c2", .01,.0005,1);
        timeErrorBckModel->setPars("timeErrorBck_c3", .01,.0005,1);
        //timeErrorBckModel->setPars("timeErrorBck_c1", .01, 0,paramLimit);
        //timeErrorBckModel->setPars("timeErrorBck_c2", .01, 0,paramLimit);
        //timeErrorBckModel->setPars("timeErrorBck_c3", .04, 0,1);
        timeErrorBckModel->fit();
    }
    ModelPDF *timeErrorModel = new ModelPDF("timeError", data);
    if ( ! massOnly ) {
        timeErrorModel->setTimeBackgroundErrorPDF(timeErrorBckModel->getTimeBackgroundErrorPDF());
        timeErrorModel->setTimeSignalErrorPDF("timeErrorSig", "2Gamma");
        timeErrorModel->setPars(timeErrorBckModel, true);
        /*timeErrorModel->setPars("timeErrorSig_a1", 4.5);
        timeErrorModel->setPars("timeErrorSig_a2", 6.5);
        timeErrorModel->setPars("timeErrorSig_a3", 1.5);
        timeErrorModel->setPars("timeErrorSig_b1", .01);
        timeErrorModel->setPars("timeErrorSig_b2", .02);
        timeErrorModel->setPars("timeErrorSig_b3", .03);
        timeErrorModel->setPars("timeErrorSig_c1", .01);
        timeErrorModel->setPars("timeErrorSig_c2", .01);
        timeErrorModel->setPars("timeErrorSig_c3", .01);*/
        timeErrorModel->setPars("timeErrorSig_a1", 6.5);
        timeErrorModel->setPars("timeErrorSig_a2", 8.5);
        //timeErrorModel->setPars("timeErrorSig_a3", 1.5);
        timeErrorModel->setPars("timeErrorSig_b1", .005);
        timeErrorModel->setPars("timeErrorSig_b2", .004);
        //timeErrorModel->setPars("timeErrorSig_b3", .03);
        timeErrorModel->setPars("timeErrorSig_c1", .01);
        timeErrorModel->setPars("timeErrorSig_c2", .02);
        //timeErrorModel->setPars("timeErrorSig_c3", .01);
        timeErrorModel->setPars("sigFrac", simpleMassModel->getPars("sigFrac")->getVal(), true);
        timeErrorModel->fit();
    }

    ModelPDF *massErrorBckModel = new ModelPDF("massErrorBck", dataSidebands);
    if ( massPCE ) {
        suffix += "_massPCE";
        massErrorBckModel->setMassBackgroundErrorPDF("massErrorBck", "2Gamma");
        massErrorBckModel->setPars("massErrorBck_a1", 2.);
        massErrorBckModel->setPars("massErrorBck_a2", 6.);
        massErrorBckModel->setPars("massErrorBck_a3", 10.);
        massErrorBckModel->setPars("massErrorBck_b1", 4.);
        massErrorBckModel->setPars("massErrorBck_b2", 6.);
        massErrorBckModel->setPars("massErrorBck_b3", 3.);
        massErrorBckModel->setPars("massErrorBck_c1", 13.);
        massErrorBckModel->setPars("massErrorBck_c2", 0.);
        massErrorBckModel->setPars("massErrorBck_c3", 1.);
        massErrorBckModel->fit();
    }

    ModelPDF *massErrorModel = new ModelPDF("massError", data);
    if ( massPCE ) {
        massErrorModel->setMassBackgroundErrorPDF(massErrorBckModel->getMassBackgroundErrorPDF());
        massErrorModel->setMassSignalErrorPDF("massErrorSig", "2Gamma");
        massErrorModel->setPars(massErrorBckModel, true);
        massErrorModel->setPars("massErrorSig_a1", 4.);
        massErrorModel->setPars("massErrorSig_a2", 8.);
        massErrorModel->setPars("massErrorSig_a3", 3.);
        massErrorModel->setPars("massErrorSig_b1", 69.);
        massErrorModel->setPars("massErrorSig_b2", 3.);
        massErrorModel->setPars("massErrorSig_b3", 3.);
        massErrorModel->setPars("massErrorSig_c1", 1.);
        massErrorModel->setPars("massErrorSig_c2", 0.);
        massErrorModel->setPars("massErrorSig_c3", 1.);
        massErrorModel->setPars("sigFrac", simpleMassModel->getPars("sigFrac")->getVal(), true);
        massErrorModel->fit();
    }

    /*ModelPDF *massTimeModel = new ModelPDF("massTime", data);
    if ( massPCE ) {
        massTimeModel->setMassSignalPDF("massSig", "gaussPCE");
        massTimeModel->setMassSignalErrorPDF(massErrorModel->getMassSignalErrorPDF());
        massTimeModel->setMassBackgroundErrorPDF(massErrorModel->getMassBackgroundErrorPDF());
    } else {
        massTimeModel->setMassSignalPDF("massSig", massGausses+"Gauss");
        suffix += "_"+massGausses+"Gauss";
    }
    massTimeModel->setMassBackgroundPDF("massBck", "exponentialAndConstant");
    //massTimeModel->setMassBackgroundPDF("massBck", "exponentialAndTanh");
    if ( ! massOnly ) {
        massTimeModel->setTimeSignalPDF("timeSig","singleExponential");
        massTimeModel->setTimeSignalErrorPDF(timeErrorModel->getTimeSignalErrorPDF());
        massTimeModel->setTimeBackgroundPDF("timeBck","promptExponential2PosNeg");
        massTimeModel->setTimeBackgroundErrorPDF(timeErrorModel->getTimeBackgroundErrorPDF());
    }

    massTimeModel->setPars("massSig_mean", PDGBplusMass, PDGBplusMass-50., PDGBplusMass+50.);
    massTimeModel->setPars("massSig_fraction1", .2);
    massTimeModel->setPars("massSig_fraction2", .4);
    massTimeModel->setPars("massSig_sigma1", 20);
    massTimeModel->setPars("massSig_sigma2", 50);
    massTimeModel->setPars("massSig_sigma3",30);
    massTimeModel->setPars("massBck_slope", -0.002);
    massTimeModel->setPars("massBck_fraction", simpleMassModel->getPars("massBck_fraction")->getVal());
    
    if ( ! massOnly ) {
        massTimeModel->setPars("timeSig_tau", 1.5, 1., 2.);
        massTimeModel->setPars("timeBck_tauPos1", 1.2, .1, 3.);
        massTimeModel->setPars("timeBck_tauPos2",  .2, .1, 3.);
        massTimeModel->setPars("timeBck_tauNeg",  .15, .1, 3.);
        massTimeModel->setPars(timeErrorModel, true);
    }
    if ( massPCE ) massTimeModel->setPars(massErrorModel, true);
    massTimeModel->setPars("sigFrac", simpleMassModel->getPars("sigFrac")->getVal());
    massTimeModel->fit();*/

    if ( massPCE && ! massOnly ) {
        TCanvas* can1 = new TCanvas("can1", "Canvas 1", 4*600, 2*800);
        can1->Divide(2,2);
        can1->cd(1);
        timeErrorBckModel->drawPlot(TIMEERROR, 100, "Lifetime Error in Sidebands");
        can1->cd(2);
        timeErrorModel->drawPlot(TIMEERROR, 100, "Lifetime Error in All Data");
        can1->cd(3);
        massErrorBckModel->drawPlot(MASSERROR, 100, "Mass Error in Sidebands");
        can1->cd(4);
        massErrorModel->drawPlot(MASSERROR, 100, "Mass Error in All Data");
        /*can1->cd(5);
        simpleMassModel->drawPlot(MASS, "Simple Mass (sigFrac for Punzi)");
        can1->cd(6);
        massTimeModel->drawPlot(MASS, "Mass-Lifetime Fit (Mass)");
        can1->cd(7);
        massTimeModel->drawPlotLog(TIME, -1., 7., "Mass-Lifetime Fit (Lifetime)");
        can1->cd(8);
        massTimeModel->drawPlotLog(TIME, -.5, 1.5, "Mass-Lifetime Fit (Lifetime \"Zoom\")");*/
        can1->SaveAs(name+suffix+".png");
        delete can1;
    } else if ( massPCE && massOnly ) {
        suffix += "_massOnly";
        TCanvas* can1 = new TCanvas("can1", "Canvas 1", 2*600, 2*800);
        can1->Divide(1,2);
        can1->cd(1);
        massErrorBckModel->drawPlot(MASSERROR, 100, "Mass Error in Sidebands");
        can1->cd(2);
        massErrorModel->drawPlot(MASSERROR, 100, "Mass Error in All Data");
        /*can1->cd(3);
        simpleMassModel->drawPlot(MASS, "Simple Mass (sigFrac for Punzi)");
        can1->cd(4);
        massTimeModel->drawPlot(MASS, "Mass Fit");*/
        can1->SaveAs(name+suffix+".png");
        delete can1;
    } /*else if ( ! massPCE && massOnly ) {
        suffix += "_massOnly";
        TCanvas* can1 = new TCanvas("can1", "Canvas 1", 1*600, 1*800);
        massTimeModel->drawPlot(MASS, "Mass-Lifetime Fit (Mass)");
        can1->SaveAs(name+suffix+".png");
        delete can1;
    } */else {
        TCanvas* can1 = new TCanvas("can1", "Canvas 1", 3*600, 2*800);
        can1->Divide(2,2);
        can1->cd(1);
        simpleMassModel->drawPlot(MASS, "Simple Mass (sigFrac for Punzi)");
        can1->cd(2);
        timeErrorBckModel->drawPlot(TIMEERROR, 100, "Lifetime Error in Sidebands");
        can1->cd(3);
        timeErrorModel->drawPlot(TIMEERROR, 100, "Lifetime Error in All Data");
        /*can1->cd(4);
        massTimeModel->drawPlot(MASS, "Mass-Lifetime Fit (Mass)");
        can1->cd(5);
        massTimeModel->drawPlotLog(TIME, -1., 7., "Mass-Lifetime Fit (Lifetime)");
        can1->cd(6);
        massTimeModel->drawPlotLog(TIME, -.5, 1.5, "Mass-Lifetime Fit (Lifetime \"Zoom\")");*/
        can1->SaveAs(name+suffix+".png");
        delete can1;
    }

    if ( massPCE ) {
        massErrorBckModel->printResults();
        massErrorModel->printResults();
    }
    if ( ! massOnly ) {
        timeErrorBckModel->printResults();
        timeErrorModel->printResults();
    }
    /*massTimeModel->printModel();
    massTimeModel->printResults();
*/
    /*delete massErrorBckModel;
    delete massErrorModel;
    delete timeErrorBckModel;
    delete timeErrorModel;
    delete massTimeModel;
    delete simpleMassModel;
    delete data;
    delete observables;*/

    cout << "INFO: Total time " << printTime( TTimeStamp().GetSec() - startTime ) << endl;
}

void punziFit(const TString &name = "") {
    //std::list<TString> triggerList = {"HLT_mu11_mu6_bDimu", "HLT_mu11_mu6_bJpsimumu", "HLT_mu11_mu6_bBmumux_BpmumuKp", "HLT_2mu6_bJpsimumu","" };
    std::list<TString> triggerList = {"HLT_mu11_mu6_bDimu", "HLT_mu11_mu6_bJpsimumu", "HLT_mu11_mu6_bBmumux_BpmumuKp", "HLT_2mu6_bJpsimumu", "HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6" };
    //std::list<TString> triggerList = {"HLT_2mu6_bJpsimumu"};
    //std::list<TString> triggerList = {"HLT_mu11_mu6_bDimu"};
    Double_t paramArray[4]={1.5,2.0,2.0,1.5};
    Int_t i =0;
    for(auto trigger : triggerList){
        BplusFitRD("results/punzi2017RD_"+trigger,"("+trigger+" == 1)", "/home/radek_novotny/analysis/CERN_git/data/BPlus/BPlustNtuple2017.root",paramArray[i++] );    
    }
    return;
}

