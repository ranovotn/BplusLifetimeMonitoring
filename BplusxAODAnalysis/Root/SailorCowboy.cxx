#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODBPhys/BPhysHypoHelper.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackingPrimitives.h"
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <TrigConfxAOD/xAODConfigTool.h>
#include "BPhysxAODTools/BPhysFunctions.h"
#include "TSystem.h"
#include <TLorentzVector.h>
#include <TTree.h>
#include <TFile.h>
#include <BplusxAODAnalysis/SailorCowboy.h>

// this is needed to distribute the algorithm to the workers
ClassImp(SailorCowboy)


#define EL_RETURN_CHECK( CONTEXT, EXP )                    \
  do {                                                     \
     if( ! EXP.isSuccess() ) {                             \
        Error( CONTEXT,                                    \
               XAOD_MESSAGE( "Failed to execute: %s" ),    \
               #EXP );                                     \
        return EL::StatusCode::FAILURE;                    \
     }                                                     \
   } while( false )

typedef ElementLink<xAOD::VertexContainer> VertexLink;
typedef std::vector<VertexLink> VertexLinkVector;



SailorCowboy :: SailorCowboy ()
    : jpsi_container_name ("BPHY1OniaCandidates"),
      output_name         ("DefaultOutput"), m_doMC(true),
      m_event_counter(0), JpsiDecayFinder(nullptr), FullReconFound(0)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode SailorCowboy :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  
  job.useXAOD();
  EL_RETURN_CHECK("setupJob()", xAOD::Init());
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SailorCowboy :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  
  TFile *output_file = wk()->getOutputFile(output_name);
  m_tree         = new TTree("Jpsi", "Jpsi");
  m_tree        ->SetDirectory(output_file);
  m_tree->Branch("mass", &m_mass);
  m_tree->Branch("pt", &m_pt);
  m_tree->Branch("rapidity", &m_rapidity);
  m_tree->Branch("chi2", &m_chi2);
  m_tree->Branch("NDF", &m_NDF);
  
  m_tree->Branch("mu1_eta", &mu1_eta);
  m_tree->Branch("mu1_pT", &mu1_pT);
  m_tree->Branch("mu2_eta", &mu2_eta);
  m_tree->Branch("mu2_pT", &mu2_pT);
  m_tree->Branch("mu1_phi", &mu1_phi);
  m_tree->Branch("mu2_phi", &mu2_phi);
  
  m_tree->Branch("mu1_charge", &mu1_charge);
  m_tree->Branch("mu2_charge", &mu2_charge);  
  
//  m_tree->Branch("cowboy", &cowboy);
  
  m_tree->Branch("True_position_x", &true_pos[0]);
  m_tree->Branch("True_position_y", &true_pos[1]);
  m_tree->Branch("True_position_z", &true_pos[2]);    
  
  
  m_tree->Branch("truemass", &truemass);
  
  m_tree->Branch("Reco_position_x", &reco_pos[0]);
  m_tree->Branch("Reco_position_y", &reco_pos[1]);
  m_tree->Branch("Reco_position_z", &reco_pos[2]);
  
  m_tree->Branch("Tau_MAX_SUM_PT2", &Tau_MAX_SUM_PT2);
  m_tree->Branch("TauErr_MAX_SUM_PT2", &TauErr_MAX_SUM_PT2);
  m_tree->Branch("Tau_LowA0", &Tau_LowA0);
  m_tree->Branch("TauErr_LowA0", &TauErr_LowA0);  
  
  m_tree->Branch("evt_number", &m_evt_number);  
  m_tree->Branch("run_number", &m_run_number);  
  m_tree->Branch("issignal", &issignal);  
  
  m_tree->Branch("true_PV_pos", &true_PV_pos);
  m_tree->Branch("minA0_PV_pos", &PV_pos);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SailorCowboy :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SailorCowboy :: changeInput (bool)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SailorCowboy :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  
  xAOD::TEvent* event = wk()->xaodEvent();
  Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SailorCowboy :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  
  xAOD::TEvent *event = wk()->xaodEvent();


  if ((m_event_counter % 100) == 0)
    Info("execute()", "%i events prcessed so far", (int) m_event_counter);
  
  m_event_counter++;

  const xAOD::EventInfo *event_info = nullptr;
  EL_RETURN_CHECK("execute()", event->retrieve(event_info, "EventInfo"));


  m_evt_number = event_info->eventNumber();
  m_lumi_block = event_info->lumiBlock  ();
  m_run_number = event_info->runNumber  ();

  bool is_mc = event_info->eventType(xAOD::EventInfo::IS_SIMULATION);
  if(JpsiDecayFinder == nullptr && is_mc){

      JpsiDecayFinder = std::unique_ptr<xAOD::TruthNtupleNode>(new xAOD::TruthNtupleNode(443, "Jpsi"));
      JpsiDecayFinder->addChild(-13, "Muplus");
      JpsiDecayFinder->addChild(13, "Muminus");  
      JpsiDecayFinder->print();
      
      m_treeTruth    = new TTree("JpsiTruth"     , "JpsiTruth"     );
      m_treeTruth->SetDirectory(wk()->getOutputFile(output_name));

      JpsiDecayFinder->Book(m_treeTruth, &m_run_number,
            &m_lumi_block, &m_evt_number);      
  }
  std::vector<const xAOD::Truth_Particle*> TrueBsReconstructed;
  std::vector<std::array<const xAOD::Track_Particle*, 2>> TracksMatchtoTrue;
  size_t truthsfound =0;
  if(is_mc){
        const xAOD::TruthParticleContainer* xTruthParticleContainer = nullptr;

        if(!event->retrieve( xTruthParticleContainer, "TruthParticles").isSuccess()) {
            Error("execute()", "Failed to find TruthParticles");
            return EL::StatusCode::FAILURE;
        }
        const xAOD::TrackParticleContainer* tracks = 0;
        if(!event->retrieve( tracks, "InDetTrackParticles" ).isSuccess()) {
            Error("execute()", "Failed to find InDetTrackParticles");
            return EL::StatusCode::FAILURE;
        }
        
        truthsfound = JpsiDecayFinder->FindTruth(xTruthParticleContainer, false, false);
        
        
        for(size_t i=0; i<truthsfound;i++){

          const xAOD::Track_Particle* mu1 = JpsiDecayFinder->getChildNode(0)->getBestTrack(tracks, i);
          const xAOD::Track_Particle* mu2 = JpsiDecayFinder->getChildNode(1)->getBestTrack(tracks, i);

          
          if(mu1 && mu2)
          {
             FullReconFound++;          
             TracksMatchtoTrue.emplace_back();
             TracksMatchtoTrue.back() =  {mu1, mu2};

             TrueBsReconstructed.push_back(JpsiDecayFinder->getTrueParticle(i));
          }
        }
        if(truthsfound){
          JpsiDecayFinder->PrepareForFill();
          m_treeTruth->Fill();
        }
  }
  
  
  const xAOD::VertexContainer *container_jpsi = nullptr;
  EL_RETURN_CHECK("execute()", event->retrieve(container_jpsi, jpsi_container_name.c_str()));
  
  for (const xAOD::Vertex* jpsi_cand : (*container_jpsi))
  {
    xAOD::BPhysHypoHelper jpsi_helper("Jpsi", jpsi_cand);
    if(jpsi_helper.nRefTrks() != 2 ||
       jpsi_helper.nMuons()   != 2)
      {
      Warning("execute()", "Expected two muon tracks, skip");
      continue;
      }

   TLorentzVector muon0_ref_track = jpsi_helper.refTrk(0, 105.65837);
   TLorentzVector muon1_ref_track = jpsi_helper.refTrk(1, 105.65837);


   TLorentzVector jpsi_ref_track = muon0_ref_track + muon1_ref_track;



   m_mass  = jpsi_cand->auxdata<float>("Jpsi_mass");
   m_pt    = jpsi_ref_track.Pt      ();
   m_rapidity = jpsi_ref_track.Rapidity();
   m_chi2  = jpsi_helper.vtx()->chiSquared();
   m_NDF   = jpsi_helper.vtx()->numberDoF();
   mu1_eta = muon0_ref_track.Eta();
   mu1_pT  = muon0_ref_track.Pt ();
   mu2_eta = muon1_ref_track.Eta();
   mu2_pT  = muon1_ref_track.Pt ();
   mu1_phi = muon0_ref_track.Phi();
   mu2_phi = muon1_ref_track.Phi ();
   Tau_MAX_SUM_PT2 = jpsi_helper.tau(xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_CONST_MASS);
   TauErr_MAX_SUM_PT2 = jpsi_helper.tauErr(xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_CONST_MASS);
   Tau_LowA0 = jpsi_helper.tau(xAOD::BPhysHelper::PV_MIN_A0, xAOD::BPhysHypoHelper::TAU_CONST_MASS);
   TauErr_LowA0 = jpsi_helper.tauErr(xAOD::BPhysHelper::PV_MIN_A0, xAOD::BPhysHypoHelper::TAU_CONST_MASS);



   
   std::array<const xAOD::TrackParticle*, 2> trackptr;
   for(size_t i =0; i<2;i++){
        trackptr[i] =  dynamic_cast<const xAOD::TrackParticle*>( jpsi_helper.refTrkOrigin(i) );
   }
   if(trackptr[0] == nullptr || !trackptr[1]) { Warning("execute()" , "FAILURE TO GET dynamic_cast<const xAOD::TrackParticle*>( jpsi_helper.refTrkOrigin(i) )");
   continue;   }
   mu1_charge = trackptr[0]->charge();
   mu2_charge = trackptr[1]->charge();   

   auto mina0PV = jpsi_helper.pv(xAOD::BPhysHelper::PV_MIN_A0);
   PV_pos = TVector3(mina0PV->x(), mina0PV->y(), mina0PV->z());

/*   
   float charge, deltaPhi;   
   if (mu1_pT < mu2_pT) {
     charge = mu1_charge;
     deltaPhi = mu1_phi-mu2_phi;
   } else {
     charge = mu2_charge;
     deltaPhi = mu2_phi-mu1_phi;
   }
   cowboy = charge*sin(deltaPhi) < 0.;   
*/   
   const xAOD::Truth_Particle* trueVertex = nullptr;
   issignal = false;
   if(is_mc){
      size_t i;
      for(i =0; i<TracksMatchtoTrue.size(); i++){
          issignal = collectionsMatch(TracksMatchtoTrue[i], trackptr);
          if(issignal){
           trueVertex = TrueBsReconstructed[i];
           break;
          }
      }
   }
   reco_pos.at(0) = jpsi_cand->x();
   reco_pos.at(1) = jpsi_cand->y();
   reco_pos.at(2) = jpsi_cand->z();
   
   if(issignal){
      auto vtx = trueVertex->decayVtx ();
      true_pos.at(0) = vtx->x();
      true_pos.at(1) = vtx->y();
      true_pos.at(2) = vtx->z();      
      truemass = trueVertex->p4().M();
      auto pv = trueVertex->prodVtx ();
      true_PV_pos = TVector3(pv->x(), pv->y(), pv->z());
   }else{
      true_pos.fill(0.);
      true_PV_pos = TVector3(0.,0.,0.);
      truemass = 0.;
   }
   
   m_tree->Fill();
  }
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SailorCowboy :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SailorCowboy :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SailorCowboy :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
