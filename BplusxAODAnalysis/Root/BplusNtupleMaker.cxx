// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODBPhys/BPhysHypoHelper.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackingPrimitives.h"
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <GoodRunsLists/GoodRunsListSelectionTool.h>
#include <TrigConfxAOD/xAODConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include <BplusxAODAnalysis/BplusNtupleMaker.h>
#include "BPhysxAODTools/BPhysFunctions.h"
#include "xAODMuon/MuonContainer.h"
#include <TH1.h>
#include <TLorentzVector.h>
#include <TTree.h>
#include <TFile.h>
#include <TSystem.h>
#include <algorithm>


// this is needed to distribute the algorithm to the workers
ClassImp(BplusNtupleMaker)

#define EL_RETURN_CHECK( CONTEXT, EXP )                    \
  do {                                                     \
     if( ! EXP.isSuccess() ) {                             \
        Error( CONTEXT,                                    \
               XAOD_MESSAGE( "Failed to execute: %s" ),    \
               #EXP );                                     \
        return EL::StatusCode::FAILURE;                    \
     }                                                     \
   } while( false )

typedef ElementLink<xAOD::VertexContainer> VertexLink;
typedef std::vector<VertexLink> VertexLinkVector;


BplusNtupleMaker::BplusNtupleMaker()
  : jpsi_container_name ("BPHY5JpsiCandidates"),
    bplus_container_name("BpmJpsiKpmCandidates"),
    PV_container_name("PrimaryVertices"),
    output_name         ("DefaultOutput"),
    m_AllCandVectorTree(false),
    m_AllCandSingleTree(false),
    m_BestChiTree(true),
    m_eventTriggertree(true),
    print_trigger_regexp(".*"),
    use_grl        (false),
    truth_only     (false),
    truth_channel  (1),
    cut_chi2_ndof  (   3.0f),
    cut_kaon_pT    (1000.0f),
    m_event_counter(0),
    m_skip_grl     (0),
    m_skip_cuts    (0),
    m_h_jpsi_mass       (nullptr),
    m_h_jpsi_pT         (nullptr),
    m_h_jpsi_rap        (nullptr),
    m_h_bplus_mass      (nullptr),
    m_h_bplus_pT        (nullptr),
    m_h_bplus_rap       (nullptr),
    m_tree              (nullptr),
    m_tree_single       (nullptr),
    m_tree_trigger      (nullptr),
    m_treeTruth         (nullptr),
    m_grl               (nullptr),
    m_trig_decision_tool(nullptr),
    m_trig_config_tool  (nullptr),

    m_n_B_candidates_loose_chi(0),
    m_n_B_candidates_tight_chi(0),
    m_n_Jpsi_candidates(0),
    m_n_events_with_zero_jpsis(0), BplusDecayFinder(nullptr)
  {
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  }



EL::StatusCode BplusNtupleMaker::setupJob(EL::Job& job)
  {
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  
  job.useXAOD();
  EL_RETURN_CHECK("setupJob()", xAOD::Init());

  return EL::StatusCode::SUCCESS;
  }



EL::StatusCode BplusNtupleMaker::histInitialize()
  {
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  
  m_h_jpsi_mass  = new TH1D("h_JpsiMass" , "h_JpsiMass" , 2000,  0.0, 20000.0);
  m_h_jpsi_pT    = new TH1D("h_JpsiPt"   , "h_JpsiPt"   , 2000,  0.0, 60000.0);
  m_h_jpsi_rap   = new TH1D("h_JpsiRap"  , "h_JpsiRap"  , 2000, -2.6, 2.6    );
  m_h_bplus_mass = new TH1D("h_BplusMass", "h_BplusMass", 2000,  0.0, 20000.0);
  m_h_bplus_pT   = new TH1D("h_BplusPt"  , "h_BplusPt"  , 2000,  0.0, 60000.0);
  m_h_bplus_rap  = new TH1D("h_BplusRap" , "h_BplusRap" , 2000, -2.6, 2.6    );

  wk()->addOutput(m_h_jpsi_mass);
  wk()->addOutput(m_h_jpsi_pT  );
  wk()->addOutput(m_h_jpsi_rap );
  wk()->addOutput(m_h_bplus_mass);
  wk()->addOutput(m_h_bplus_pT  );
  wk()->addOutput(m_h_bplus_rap );


  TFile *output_file = wk()->getOutputFile(output_name);
  
  
  if(m_BestChiTree){
      m_tree_single  = new TTree("BplusBestChi"      , "BplusBestChi"      );
      m_tree_single ->SetDirectory(output_file);
  }else{
      m_tree_single = nullptr;
  }
  
  if(m_eventTriggertree){  
      m_tree_trigger = new TTree("BplusTriggers"     , "BplusTriggers"     );
      m_tree_trigger->SetDirectory(output_file);
  }else{
      m_tree_trigger = nullptr;
  }
  
  if(m_AllCandSingleTree){
     m_treeAllSingle = new TTree("BplusAllCandSingle", "BplusAllCandSingle");
     m_treeAllSingleTrigger = new TTree("BplusAllSingleTrigger", "BplusAllSingleTrigger");
     m_treeAllSingle->SetDirectory(output_file);
     m_treeAllSingleTrigger->SetDirectory(output_file);
  }else{
     m_treeAllSingle = nullptr;
     m_treeAllSingleTrigger = nullptr;
  }
  
  if(m_AllCandVectorTree){
     m_tree         = new TTree("BplusAllCandidates", "BplusAllCandidates");
     m_tree        ->SetDirectory(output_file);
  }else{
     m_tree = nullptr;
  }
  
  InitBranches();
  
  return EL::StatusCode::SUCCESS;
  }



EL::StatusCode BplusNtupleMaker::fileExecute()
  {
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
  }



EL::StatusCode BplusNtupleMaker::changeInput(bool /*firstFile*/)
  {
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.


//  xAOD::TEvent* event = wk()->xaodEvent();


  auto &m_trigDecisionTool=  m_trig_decision_tool;
  if (m_trigDecisionTool)
    {   
    delete m_trigDecisionTool;//m_trig_config_tool
    ToolHandle< TrigConf::ITrigConfigTool > trigConfigHandle( m_trig_config_tool );
    m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
    EL_RETURN_CHECK( "initialize", m_trigDecisionTool->setProperty( "ConfigTool", trigConfigHandle ) ); // connect the TrigDecisionTool to the ConfigTool
    EL_RETURN_CHECK( "initialize", m_trigDecisionTool->setProperty( "TrigDecisionKey", "xTrigDecision" ) );
    EL_RETURN_CHECK( "initialize", m_trigDecisionTool->initialize() );
    }   


  return EL::StatusCode::SUCCESS;
  }

template <typename type>
type GetRangeofList(const std::vector<type> &list)
{
    auto maxmin = std::minmax_element(list.cbegin(), list.cend());
    return *maxmin.second - *maxmin.first;
} 


EL::StatusCode BplusNtupleMaker::initialize()
  {
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  xAOD::TEvent* event = wk()->xaodEvent();
  Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

  m_event_counter = 0;
  m_skip_grl      = 0;
  m_skip_cuts     = 0;

  if (use_grl)
    {
    Info("initialize()", "use_grl = true (GRLs will be applied for data)");
    for(auto &str : grl_files){
        str = gSystem->ExpandPathName (str.c_str());
    }
    m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
    EL_RETURN_CHECK("initialize()", m_grl->setProperty("GoodRunsListVec", grl_files));
    EL_RETURN_CHECK("initialize()", m_grl->setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
    EL_RETURN_CHECK("initialize()", m_grl->initialize());
    }
  else{
    Info("initialize()", "use_grl = false (GRLs will not be applied)"); 
    m_grl = nullptr;
  }

  if (!truth_only)
    {
    m_trig_config_tool = new TrigConf::xAODConfigTool("xAODConfigTool"); // gives us access to the meta-data
    EL_RETURN_CHECK("initialize()", m_trig_config_tool->initialize());
    ToolHandle<TrigConf::ITrigConfigTool> trig_config_handle(m_trig_config_tool);
    m_trig_decision_tool = new Trig::TrigDecisionTool("TrigDecisionTool");
    EL_RETURN_CHECK("initialize()", m_trig_decision_tool->setProperty("ConfigTool", trig_config_handle)); // connect the TrigDecisionTool to the ConfigTool
    EL_RETURN_CHECK("initialize()", m_trig_decision_tool->setProperty("TrigDecisionKey", "xTrigDecision"));
    EL_RETURN_CHECK("initialize()", m_trig_decision_tool->initialize());
    }

  TrueDecaysFound =0;
  TrueDecaysFoundOver1 =0;
  FullReconFound =0;
  
  extraChildren.fill(0);

  return EL::StatusCode::SUCCESS;
  }

uint8_t BplusNtupleMaker::GetTrackPropertyInt(const xAOD::TrackParticle *track, const xAOD::SummaryType e) {
    uint8_t num=0;
    if (track->summaryValue(num, e)){
       return num;    
    }else
    { Error("GetTrackPropertyInt", "Track variable not failed"); return -1; }
}

EL::StatusCode BplusNtupleMaker::execute()
  {
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  
  xAOD::TEvent *event = wk()->xaodEvent();


  if ((m_event_counter % 100) == 0)
    Info("execute()", "%i events prcessed so far", m_event_counter);
  
  m_event_counter++;



  const xAOD::EventInfo *event_info = nullptr;
  EL_RETURN_CHECK("execute()", event->retrieve(event_info, "EventInfo"));


  m_evt_number = event_info->eventNumber();
  m_lumi_block = event_info->lumiBlock  ();
  m_run_number = event_info->runNumber  ();

  bool is_mc = event_info->eventType(xAOD::EventInfo::IS_SIMULATION);
  if(BplusDecayFinder == nullptr && is_mc){

      if (truth_channel > 0)
        BplusDecayFinder = std::unique_ptr<xAOD::TruthNtupleNode>(new xAOD::TruthNtupleNode(521, "Bplus"));
      else
        BplusDecayFinder = std::unique_ptr<xAOD::TruthNtupleNode>(new xAOD::TruthNtupleNode(-521, "Bminus"));
      auto *jpsi = BplusDecayFinder->addChild(443, "Jpsi");
      jpsi->addChild(-13, "Muplus");
      jpsi->addChild(13, "Muminus");
      switch (truth_channel)
        {
        case 1:
          BplusDecayFinder->addChild(321, "Kplus");
          break;
        case 2:
          BplusDecayFinder->addChild(211, "Piplus");
          break;
        case -1:
          BplusDecayFinder->addChild(-321, "Kminus");
          break;
        case -2:
          BplusDecayFinder->addChild(-211, "Kminus");
          break;
        default:
          Error("execute()", "Unexpected truth_channel value");
          return EL::StatusCode::FAILURE;
        }
      BplusDecayFinder->print();

      m_treeTruth    = new TTree("BplusTruth"     , "BplusTruth"     );
      m_treeTruth->SetDirectory(wk()->getOutputFile(output_name));

      BplusDecayFinder->Book(m_treeTruth, &m_run_number.value,
            &m_lumi_block.value, &m_evt_number.value);

  }
  std::vector<std::array<const xAOD::Track_Particle*, 3>> TracksMatchtoTrue;
  std::vector<bool> Bexclusive; //Cache because looking up is slow
  std::vector<const xAOD::Truth_Particle*> TrueBsReconstructed;
  size_t truthsfound =0;
  
  if(is_mc){
        const xAOD::TruthParticleContainer* xTruthParticleContainer = nullptr;

        if(!event->retrieve( xTruthParticleContainer, "TruthParticles").isSuccess()) {
            Error("execute()", "Failed to find TruthParticles");
            return EL::StatusCode::FAILURE;
        }
        const xAOD::TrackParticleContainer* tracks = 0;
        if(!truth_only && !event->retrieve( tracks, "InDetTrackParticles" ).isSuccess()) {
            Error("execute()", "Failed to find InDetTrackParticles");
            return EL::StatusCode::FAILURE;
        }
        
        
        truthsfound = BplusDecayFinder->FindTruth(xTruthParticleContainer, false, false);
//        if(BplusDecayFinder->DebugInternalState() == false) Error("execute()", "Internal state error");
//        Info("execute()", "Found %lli decays", decays);
//        BplusDecayFinder->printResults();
        TrueDecaysFound+=truthsfound;
        TrueDecaysFoundOver1+=truthsfound>1;
        
        
        for(size_t i=0; !truth_only && i<truthsfound;i++){
          auto jpsi = BplusDecayFinder->getChildNode(0);
          const xAOD::Track_Particle* mu1 = jpsi->getChildNode(0)->getBestTrack(tracks, i);
          const xAOD::Track_Particle* mu2 = jpsi->getChildNode(1)->getBestTrack(tracks, i);
          const xAOD::Track_Particle* Kplus = BplusDecayFinder->getChildNode(1)->getBestTrack(tracks, i);
          
          
          if(mu1 && mu2 && Kplus)
          {
             FullReconFound++;          
             TracksMatchtoTrue.emplace_back();
             TracksMatchtoTrue.back() =  {mu1, mu2, Kplus};
             
             extraChildren[0] += !BplusDecayFinder->DecaySegmentWasExclusive(i);
             extraChildren[1] += !jpsi->DecaySegmentWasExclusive(i);
             extraChildren[2] += !jpsi->getChildNode(0)->DecaySegmentWasExclusive(i);
             extraChildren[3] += !jpsi->getChildNode(1)->DecaySegmentWasExclusive(i);
             extraChildren[4] += !BplusDecayFinder->getChildNode(1)->DecaySegmentWasExclusive(i);             
             
             bool exclusive = BplusDecayFinder->DecaySegmentWasExclusive(i) && jpsi->DecaySegmentWasExclusive(i);
             
             Bexclusive.push_back(exclusive);
             TrueBsReconstructed.push_back(BplusDecayFinder->getTrueParticle(i));
             

          }


        }

        BplusDecayFinder->PrepareForFill();
        m_treeTruth->Fill();
  }
  if (truth_only) return EL::StatusCode::SUCCESS;

  // Apply GRL in case it's data
  
  m_pass_GRL = !is_mc && m_grl && m_grl->passRunLB(*event_info);
  m_skip_grl += !is_mc && !m_pass_GRL;







  
  const xAOD::VertexContainer *container_jpsi = nullptr;
  EL_RETURN_CHECK("execute()", event->retrieve(container_jpsi, jpsi_container_name.c_str()));

  m_n_events_with_zero_jpsis+= container_jpsi->empty();
  const xAOD::VertexContainer *container_bplus = nullptr;
  auto status = event->retrieve(container_bplus, bplus_container_name.c_str());
  if(status.isFailure()){
    Warning("execute()", "Missing bplus container skipping");
    ClearBranches();
    return EL::StatusCode::SUCCESS;
  }
  if (container_bplus->empty())
    {
    ClearBranches();
    return EL::StatusCode::SUCCESS;
    }


  const xAOD::MuonContainer* muons = 0;
  EL_RETURN_CHECK("execute()",event->retrieve( muons, "Muons" ));

  const xAOD::VertexContainer *container_PV = nullptr;
  EL_RETURN_CHECK("execute()", event->retrieve(container_PV, PV_container_name.c_str()));
  
  m_nPVs = container_PV->size();

  m_nJpsi = container_jpsi->size();
  m_nMuons = muons->size();

  std::vector<const xAOD::Vertex *> good_jpsis;
  for (const xAOD::Vertex* jpsi_cand : (*container_jpsi))
    {
    xAOD::BPhysHypoHelper jpsi_helper("Jpsi", jpsi_cand);
    if(jpsi_helper.nRefTrks() != 2 ||
       jpsi_helper.nMuons()   != 2)
      {
      Warning("execute()", "Expected two muon tracks, skip");
      continue;
      }
    m_n_Jpsi_candidates++;
    good_jpsis.push_back(jpsi_cand);
    TLorentzVector muon0_ref_track = jpsi_helper.refTrk(0, 105.65837);
    TLorentzVector muon1_ref_track = jpsi_helper.refTrk(1, 105.65837);
    TLorentzVector jpsi_ref_track = muon0_ref_track + muon1_ref_track;
    
    m_h_jpsi_pT ->Fill(jpsi_ref_track.Pt      ());
    m_h_jpsi_rap->Fill(jpsi_ref_track.Rapidity());

    float jpsi_mass = jpsi_cand->auxdata<float>("Jpsi_mass");
    m_h_jpsi_mass->Fill(jpsi_mass);
    m_Jpsi_mass    .push_back(jpsi_mass);
    m_Jpsi_rapidity.push_back(jpsi_ref_track.Rapidity());
    m_Jpsi_pT      .push_back(jpsi_ref_track.Pt      ());
    m_Jpsi_chi2    .push_back(jpsi_helper.vtx()->chiSquared());
    m_Jpsi_mu1_eta .push_back(muon0_ref_track.Eta());
    m_Jpsi_mu1_pT  .push_back(muon0_ref_track.Pt ());
    m_Jpsi_mu2_eta .push_back(muon1_ref_track.Eta());
    m_Jpsi_mu2_pT  .push_back(muon1_ref_track.Pt ());


    m_Jpsi_MaxSumPtConstM_tau.push_back(jpsi_helper.tau(xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_CONST_MASS));
    m_Jpsi_MaxSumPt_ConstM_tauErr.push_back(jpsi_helper.tauErr(xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_CONST_MASS));
    m_Jpsi_MaxSumPtInvM_tau.push_back(jpsi_helper.tau(xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_INV_MASS));
    m_Jpsi_MaxSumPtInvM_tauErr.push_back(jpsi_helper.tauErr(xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_INV_MASS));
    
    }

  for (const xAOD::Vertex* bplus_cand : (*container_bplus))
    {

    xAOD::BPhysHypoHelper bplus_helper("Bplus", bplus_cand);
    if (bplus_helper.nRefTrks() != 3)
      {
      Warning("execute()", "Expected three (not %d) tracks for B candidate, skip", bplus_helper.nRefTrks());
      continue;
      }
    VertexLinkVector prec_vtx = bplus_cand->auxdata<VertexLinkVector>("PrecedingVertexLinks");

    if (prec_vtx.size() != 1)
      {
      Warning("execute()", "Expected one (not %d) preceding vertices for B candidate, skip", (int)prec_vtx.size());
      continue;
      }
    auto jpsi_it = std::find(good_jpsis.begin(), good_jpsis.end(), *(prec_vtx[0]));
    if (jpsi_it == good_jpsis.end())
      {
      Warning("execute()", "Unknown preceding vertex for B candidate, skip");
      continue;
      }
     
    m_n_B_candidates_loose_chi++;

    TLorentzVector muon0_ref_track = bplus_helper.refTrk(0, 105.65837);
    TLorentzVector muon1_ref_track = bplus_helper.refTrk(1, 105.65837);
    TLorentzVector kaon_ref_track  = bplus_helper.refTrk(2, 493.677);
    TLorentzVector bplus_ref_track = muon0_ref_track + muon1_ref_track + kaon_ref_track;
    float chi2_ndof = bplus_helper.vtx()->chiSquared() / bplus_helper.vtx()->numberDoF();
//    if (chi2_ndof > cut_chi2_ndof || kaon_ref_track.Pt () < cut_kaon_pT 
//         || bplus_ref_track.M() < 4800.0 || bplus_ref_track.M() >  5800.0)
      if (chi2_ndof > cut_chi2_ndof || kaon_ref_track.Pt () < cut_kaon_pT)
      {
      m_skip_cuts++;
      continue;
      }
    
    m_B_Jpsi_index.push_back(jpsi_it - good_jpsis.begin());
    m_B_Jpsi_mass .push_back((*jpsi_it)->auxdata<float>("Jpsi_mass"));


    std::array<const xAOD::TrackParticle*, 3> trackptr;
//    std::array<int32_t, 3> sctHits;
    int32_t IBLhits= 0;
    for(size_t i =0; i<3;i++){
        trackptr[i] =  dynamic_cast<const xAOD::TrackParticle*>( bplus_helper.refTrkOrigin(i) );
        IBLhits += (GetTrackPropertyInt(trackptr[i], xAOD::SummaryType::numberOfInnermostPixelLayerHits) > 0);
//        sctHits[i] = GetTrackPropertyInt(trackptr[i], xAOD::SummaryType::numberOfSCTHits);
    }
    
    if(is_mc){
         bool match = false;
         bool exclusive = false;
         size_t i;
         for(i =0; i<TracksMatchtoTrue.size(); i++){
             match = collectionsMatch(TracksMatchtoTrue[i], trackptr);
             if(match){
                exclusive = Bexclusive[i];
                break;
             }
         }
         m_exclusiveTrueBplus.push_back(match & exclusive);
         m_inclusiveTrueBplus.push_back(match);
         float lifetime = match ? BPhysFunctions::tau(TrueBsReconstructed.at(i)): -1;
         m_TrueLifetime.push_back(lifetime);
         
    }else{
         m_exclusiveTrueBplus.push_back(false);
         m_inclusiveTrueBplus.push_back(false);
         m_TrueLifetime.push_back(-1);
    }
    
    m_B_nIBL.push_back(IBLhits);


    m_B_chi2_ndof.push_back(chi2_ndof);
    m_n_B_candidates_tight_chi++;

    m_B_mu1_eta.push_back(muon0_ref_track.Eta());
    m_B_mu2_eta.push_back(muon1_ref_track.Eta());
    m_B_trk_eta.push_back(kaon_ref_track .Eta());
    
    m_B_mu1_pT .push_back(muon0_ref_track.Pt ());
    m_B_mu2_pT .push_back(muon1_ref_track.Pt ());
    m_B_trk_pT .push_back(kaon_ref_track .Pt ());

    m_B_mu1_phi .push_back(muon0_ref_track.Phi ());
    m_B_mu2_phi .push_back(muon1_ref_track.Phi ());
    m_B_trk_phi .push_back(kaon_ref_track .Phi ());

    m_B_mu1_nSCT.push_back(GetTrackPropertyInt(trackptr[0], xAOD::SummaryType::numberOfSCTHits));
    m_B_mu2_nSCT.push_back(GetTrackPropertyInt(trackptr[1], xAOD::SummaryType::numberOfSCTHits));
    m_B_h1_nSCT .push_back(GetTrackPropertyInt(trackptr[2], xAOD::SummaryType::numberOfSCTHits));

    m_B_mu1_nSCThole.push_back(GetTrackPropertyInt(trackptr[0], xAOD::SummaryType::numberOfSCTHoles));
    m_B_mu2_nSCThole.push_back(GetTrackPropertyInt(trackptr[1], xAOD::SummaryType::numberOfSCTHoles));
    m_B_h1_nSCThole .push_back(GetTrackPropertyInt(trackptr[2], xAOD::SummaryType::numberOfSCTHoles));


    m_B_mu1_npix.push_back(GetTrackPropertyInt(trackptr[0], xAOD::SummaryType::numberOfPixelHits));
    m_B_mu2_npix.push_back(GetTrackPropertyInt(trackptr[1], xAOD::SummaryType::numberOfPixelHits));
    m_B_h1_npix.push_back(GetTrackPropertyInt(trackptr[2], xAOD::SummaryType::numberOfPixelHits));
    
    m_B_mu1_npixhole.push_back(GetTrackPropertyInt(trackptr[0], xAOD::SummaryType::numberOfPixelHoles));
    m_B_mu2_npixhole.push_back(GetTrackPropertyInt(trackptr[1], xAOD::SummaryType::numberOfPixelHoles));
    m_B_h1_npixhole.push_back(GetTrackPropertyInt(trackptr[2], xAOD::SummaryType::numberOfPixelHoles));
    
    m_B_mu1_nSCTsh.push_back(GetTrackPropertyInt(trackptr[0], xAOD::SummaryType::numberOfSCTSharedHits));
    m_B_mu2_nSCTsh.push_back(GetTrackPropertyInt(trackptr[1], xAOD::SummaryType::numberOfSCTSharedHits));
    m_B_h1_nSCTsh.push_back(GetTrackPropertyInt(trackptr[2], xAOD::SummaryType::numberOfSCTSharedHits));
    
    m_B_mu1_npixsh.push_back(GetTrackPropertyInt(trackptr[0], xAOD::SummaryType::numberOfPixelSharedHits));
    m_B_mu2_npixsh.push_back(GetTrackPropertyInt(trackptr[1], xAOD::SummaryType::numberOfPixelSharedHits));
    m_B_h1_npixsh.push_back(GetTrackPropertyInt(trackptr[2], xAOD::SummaryType::numberOfPixelSharedHits));


    m_B_rapidity.push_back(bplus_ref_track.Rapidity());
    m_B_pT      .push_back(bplus_ref_track.Pt      ());
    m_B_pT_err  .push_back(bplus_cand->auxdata<float>("PtErr"));

    m_B_Lxy_MaxSumPt     .push_back(bplus_helper.lxy    (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
    m_B_Lxy_MaxSumPt_err .push_back(bplus_helper.lxyErr (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
    m_B_A0_MaxSumPt      .push_back(bplus_helper.a0     (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
    m_B_A0_MaxSumPt_err  .push_back(bplus_helper.a0Err  (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
    m_B_A0xy_MaxSumPt    .push_back(bplus_helper.a0xy   (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
    m_B_A0xy_MaxSumPt_err.push_back(bplus_helper.a0xyErr(xAOD::BPhysHelper::PV_MAX_SUM_PT2));
    m_B_Z0_MaxSumPt      .push_back(bplus_helper.z0     (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
    m_B_Z0_MaxSumPt_err  .push_back(bplus_helper.z0Err  (xAOD::BPhysHelper::PV_MAX_SUM_PT2));

    m_B_Lxy_MinA0        .push_back(bplus_helper.lxy    (xAOD::BPhysHelper::PV_MIN_A0));
    m_B_Lxy_MinA0_err    .push_back(bplus_helper.lxyErr (xAOD::BPhysHelper::PV_MIN_A0));
    m_B_A0_MinA0         .push_back(bplus_helper.a0     (xAOD::BPhysHelper::PV_MIN_A0));
    m_B_A0_MinA0_err     .push_back(bplus_helper.a0Err  (xAOD::BPhysHelper::PV_MIN_A0));
    m_B_A0xy_MinA0       .push_back(bplus_helper.a0xy   (xAOD::BPhysHelper::PV_MIN_A0));
    m_B_A0xy_MinA0_err   .push_back(bplus_helper.a0xyErr(xAOD::BPhysHelper::PV_MIN_A0));
    m_B_Z0_MinA0         .push_back(bplus_helper.z0     (xAOD::BPhysHelper::PV_MIN_A0));
    m_B_Z0_MinA0_err     .push_back(bplus_helper.z0Err  (xAOD::BPhysHelper::PV_MIN_A0));


    m_h_bplus_pT ->Fill(bplus_ref_track.Pt      ());
    m_h_bplus_rap->Fill(bplus_ref_track.Rapidity());
    
    float bplus_mass = bplus_helper.mass();
    m_B_mass.push_back(bplus_mass);
    m_h_bplus_mass->Fill(bplus_mass);
    float bplus_mass_err = bplus_helper.massErr();
    m_B_mass_err.push_back(bplus_mass_err);

    m_B_tau_MaxSumPtConstM.push_back(bplus_helper.tau   (xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_CONST_MASS));
    m_B_tau_MaxSumPtConstM_err.push_back(bplus_helper.tauErr(xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_CONST_MASS));
    m_B_tau_MinA0ConstM.push_back(bplus_helper.tau   (xAOD::BPhysHelper::PV_MIN_A0 , xAOD::BPhysHypoHelper::TAU_CONST_MASS));
    m_B_tau_MinA0ConstM_err.push_back(bplus_helper.tauErr(xAOD::BPhysHelper::PV_MIN_A0, xAOD::BPhysHypoHelper::TAU_CONST_MASS));
    
    m_B_tau_MaxSumPtInvM    .push_back(bplus_helper.tau   (xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_INV_MASS));
    m_B_tau_MaxSumPtInvM_err.push_back(bplus_helper.tauErr(xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_INV_MASS));
    m_B_tau_MinA0InvM       .push_back(bplus_helper.tau   (xAOD::BPhysHelper::PV_MIN_A0  , xAOD::BPhysHypoHelper::TAU_INV_MASS));
    m_B_tau_MinA0InvM_err   .push_back(bplus_helper.tauErr(xAOD::BPhysHelper::PV_MIN_A0  , xAOD::BPhysHypoHelper::TAU_INV_MASS));
    
    m_B_mu1_charge.push_back(bplus_helper.refTrkCharge(0));
    m_B_mu2_charge.push_back(bplus_helper.refTrkCharge(1));
    m_B_trk_charge.push_back(bplus_helper.refTrkCharge(2));

    const xAOD::Vertex *orig_PV_MaxSumPt = bplus_helper.origPv(xAOD::BPhysHelper::PV_MAX_SUM_PT2);
    const xAOD::Vertex *orig_PV_MinA0    = bplus_helper.origPv(xAOD::BPhysHelper::PV_MIN_A0     );
//    const xAOD::Vertex *orig_PV_MinZ0    = bplus_helper.origPv(xAOD::BPhysHelper::PV_MIN_Z0     );
    const xAOD::Vertex * ref_PV_MaxSumPt = bplus_helper.pv    (xAOD::BPhysHelper::PV_MAX_SUM_PT2);
    const xAOD::Vertex * ref_PV_MinA0    = bplus_helper.pv    (xAOD::BPhysHelper::PV_MIN_A0     );
//    const xAOD::Vertex * ref_PV_MinZ0    = bplus_helper.pv    (xAOD::BPhysHelper::PV_MIN_Z0     );

    m_MaxSumPt_orig_ntrk .push_back(orig_PV_MaxSumPt->nTrackParticles());
    m_MinA0_orig_ntrk    .push_back(orig_PV_MinA0   ->nTrackParticles());
//    m_MinZ0_orig_ntrk    .push_back(orig_PV_MinZ0   ->nTrackParticles());
    m_MaxSumPt_ref_ntrk  .push_back( ref_PV_MaxSumPt->nTrackParticles());
    m_MinA0_ref_ntrk     .push_back( ref_PV_MinA0   ->nTrackParticles());
//    m_MinZ0_ref_ntrk     .push_back( ref_PV_MinZ0   ->nTrackParticles());
    m_MaxSumPt_ref_stat  .push_back(bplus_helper.RefitPVStatus(xAOD::BPhysHelper::PV_MAX_SUM_PT2));
    m_MinA0_ref_stat     .push_back(bplus_helper.RefitPVStatus(xAOD::BPhysHelper::PV_MIN_A0     ));
//    m_MinZ0_ref_stat     .push_back(bplus_helper.RefitPVStatus(xAOD::BPhysHelper::PV_MIN_Z0     ));
    m_MaxSumPt_vertextype.push_back( ref_PV_MaxSumPt->vertexType());
    m_MinA0_vertextype   .push_back( ref_PV_MinA0   ->vertexType());
//    m_MinZ0_vertextype   .push_back( ref_PV_MinZ0   ->vertexType());
    }

  if (m_B_chi2_ndof.empty())
    {
    ClearBranches();
    return EL::StatusCode::SUCCESS;
    }

  if (m_trigger_branches.size() != triggers.size())
    {
    Error("execute()", "Internal logic error: m_trigger_branches.size() != triggers.size()");
    return EL::StatusCode::FAILURE;
    }
  for (size_t i = 0; i < triggers.size(); i++)
    {
    auto chain_group = m_trig_decision_tool->getChainGroup(triggers[i].c_str());
//    if(m_trigger_branches[i] == nullptr) Error("execute()", "m_trigger_branches[i]");
    if(__builtin_expect(chain_group !=nullptr, 1) )
        *(m_trigger_branches[i]) = chain_group->isPassed();
    else{
        *(m_trigger_branches[i]) = false;
        Error("execute()", "Trigger returned null");
    }
    }

  static bool parse_triggers = true;
  if (parse_triggers)
    {
    parse_triggers = false;
    Info("execute()", "Parsing triggers");
    auto chain_group = m_trig_decision_tool->getChainGroup(print_trigger_regexp.c_str());
    if(chain_group !=nullptr){
      for(auto &trig : chain_group->getListOfTriggers())
        {
//         std::string this_trig = trig;
         Info("execute()", "%60s", trig.c_str());
        } 
    }else
        Error("execute()", "Trigger returned null 429");

    }

   
  size_t lowest_chi2_index = 0;
  for (size_t i = 1; i < m_B_chi2_ndof.size(); i++)
    {
    if (m_B_chi2_ndof[i] < m_B_chi2_ndof[lowest_chi2_index])
      lowest_chi2_index = i;
    }
  m_nCands = m_B_chi2_ndof.size();

  m_MassRange = GetRangeofList(m_B_mass);
  m_LifeRange = GetRangeofList(m_B_tau_MinA0ConstM);

  IBranchVariable::SetAllElementsToStore(lowest_chi2_index, this   );
  IBranchVariable::SetAllElementsToStore(lowest_chi2_index, this, 1); // 1 - trigger branches group
////NEW CUTS
  bool skipEvent = m_B_mass.MainValue() > 5800. || m_B_mass.MainValue() < 4800. || m_B_trk_pT.MainValue() < cut_kaon_pT;
  if(skipEvent) goto endofEVENT;

  if(m_tree) m_tree        ->Fill();
  if(m_tree_single) m_tree_single ->Fill();
  if(m_tree_trigger) m_tree_trigger->Fill();
  
  
  if(m_treeAllSingle) {
    for (size_t i = 0; i < m_B_chi2_ndof.size(); i++){
      IBranchVariable::SetAllElementsToStore(i, this   );
      IBranchVariable::SetAllElementsToStore(i, this, 1);
      m_treeAllSingle->Fill();
      m_treeAllSingleTrigger->Fill();
    }
  }
endofEVENT:  
  
  ClearBranches();
  return EL::StatusCode::SUCCESS;
  }



EL::StatusCode BplusNtupleMaker::postExecute()
  {
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
  }



EL::StatusCode BplusNtupleMaker::finalize()
  {
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  
  //xAOD::TEvent* event = wk()->xaodEvent();
  
  Info("finalize()", "Number of B candidates (loose): %i", m_n_B_candidates_loose_chi);
  Info("finalize()", "Number of B candidates (tight): %i", m_n_B_candidates_tight_chi);
  Info("finalize()", "Number of Jpsi candidates: %i"     , m_n_Jpsi_candidates       );
  Info("finalize()", "Number of events with 0 jpsis: %i" , m_n_events_with_zero_jpsis);
  Info("finalize()", "Cut out by GRL: %i"                , m_skip_grl                );
  Info("finalize()", "Cut out by selection cuts: %i"     , m_skip_cuts               );


  Info("finalize()", "TrueDecaysFound: %i"     , TrueDecaysFound           );
  Info("finalize()", "TrueDecaysFoundOver1: %i"     , TrueDecaysFoundOver1           );
  Info("finalize()", "FullReconFound: %i"     , FullReconFound           );
  for(size_t i =0;i< extraChildren.size(); i++) Info("finalize()", "Node %lli extra children: %i", i , extraChildren[i]);
  
  
  delete m_trig_decision_tool;
  m_trig_decision_tool = nullptr;
  delete m_trig_config_tool;
  m_trig_config_tool = nullptr;
  delete m_grl;
  m_grl = nullptr;
  return EL::StatusCode::SUCCESS;
  }

EL::StatusCode BplusNtupleMaker::histFinalize()
  {
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  for (auto trig_branch : m_trigger_branches)
    {
    delete trig_branch;
    trig_branch = nullptr;
    }
  m_trigger_branches.clear();
  return EL::StatusCode::SUCCESS;
  }

void BplusNtupleMaker::InitBranches()
  {
  for (const std::string &trigger : triggers)
    {
    std::string trigger_branch_name = trigger;
    std::replace(trigger_branch_name.begin(),
                 trigger_branch_name.end  (),
                 '-', '_');
    m_trigger_branches.push_back(new ScalarBranchVariable<bool>(trigger_branch_name.c_str(), this, 1)); // 1 - trigger branches group
    }

  if(m_tree || m_tree_single)IBranchVariable::SetAllBranches(m_tree        , m_tree_single, this   );
  if(m_tree_trigger) IBranchVariable::SetAllBranches(m_tree_trigger, nullptr      , this, 1); // 1 - trigger branches group
  
  if(m_treeAllSingle || m_tree) IBranchVariable::SetAllBranches(m_tree        , m_treeAllSingle, this   );
  if(m_treeAllSingleTrigger) IBranchVariable::SetAllBranches(m_treeAllSingleTrigger, nullptr      , this, 1); // 1 - trigger branches group  
  }

void BplusNtupleMaker::ClearBranches()
  {
  IBranchVariable::ResetAllBranches(this   );
  IBranchVariable::ResetAllBranches(this, 1); // 1 - trigger branches group
  }


