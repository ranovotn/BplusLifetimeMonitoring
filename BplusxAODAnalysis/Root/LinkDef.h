#include <BplusxAODAnalysis/BplusTagging.h>

#include <BplusxAODAnalysis/SailorCowboy.h>

#include <BplusxAODAnalysis/BplusTruthToNtuple.h>


#include <BplusxAODAnalysis/BplusNtupleMaker.h>
//#include <BplusxAODAnalysis/TriggerList.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class BplusNtupleMaker+;
#endif


#ifdef __CINT__
#pragma link C++ class BplusTruthToNtuple+;
#endif

#ifdef __CINT__
#pragma link C++ class SailorCowboy+;
#endif

#ifdef __CINT__
#pragma link C++ class BplusTagging+;
#endif
