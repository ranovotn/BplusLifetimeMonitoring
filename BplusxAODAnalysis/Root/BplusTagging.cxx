// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODBPhys/BPhysHelper.h"
#include "BplusxAODAnalysis/BplusTagging.h"
#include "BPhysxAODTools/BPhysFunctions.h"
// #include "DataModel/ElementLink.h"
#include "xAODEgamma/versions/ElectronContainer_v1.h"
#include "xAODEgamma/ElectronxAODHelpers.h"

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <TH1.h>
#include <TLorentzVector.h>
#include <TTree.h>
#include <TFile.h>

#include <TSystem.h>
#include <vector>
#include <algorithm>
#include "GoodRunsLists/GoodRunsListSelectionTool.h"

// this is needed to distribute the algorithm to the workers
ClassImp(BplusTagging)

#define EL_RETURN_CHECK( CONTEXT, EXP )                    \
  do {                                                     \
     if( ! EXP.isSuccess() ) {                             \
        Error( CONTEXT,                                    \
               XAOD_MESSAGE( "Failed to execute: %s" ),    \
               #EXP );                                     \
        return EL::StatusCode::FAILURE;                    \
     }                                                     \
   } while( false )

typedef ElementLink<xAOD::VertexContainer> VertexLink;
typedef std::vector<VertexLink> VertexLinkVector;
using namespace std;

BplusTagging :: BplusTagging ()  : jpsi_container_name ("JpsiCandidates"),
    bplus_container_name("BpmJpsiKpmCandidates"),
    output_name         ("DefaultOutput"),
    muons_container_name("Muons"),
    electrons_container_name("Electrons"),
    pvrefitted_container_name("BPHY2RefittedPrimaryVertices"),
    pv_container_name("PrimaryVertices"),
    pv_ref_container_name("BPHY5RefBplJpsiKplPrimaryVertices"),
    m_eventCounter(0),
    m_h_jpsi_mass (nullptr),
    m_h_jpsi_pT   (nullptr),
    m_h_jpsi_rap  (nullptr),
    m_h_bplus_mass(nullptr),
    m_h_bplus_charge(nullptr),
    m_h_bplus_pT  (nullptr),
    m_h_bplus_rap (nullptr),
    m_tree        (nullptr),

    m_run_number(-1),
    m_lumi_block(-1),
    m_evt_number(-1),
    m_n_B_candidates_loose_chi(0),
    m_n_B_candidates_tight_chi(0),
    m_n_Jpsi_candidates(0),
    m_n_events_with_zero_jpsis(0),
    m_n_muons( 0),
    m_n_electrons( 0),
    m_n_el_tight( 0), 
    m_el_chi2( 0),
    m_el_cone_ch_estimator( 0),
    m_el_cone_ch_estimator2( 0),
    m_el_author    ( 0 ),
    m_el_charge    ( 0 ),
    m_el_eta       ( 0 ),
    m_el_phi       ( 0 ),
    m_el_pT        ( 0 ),
    m_el_z0        ( 0 ),
    m_el_ifTight   ( 0 ),
    m_el_ifMedium  ( 0 ),
    m_el_ifLoose   ( 0 ),
    m_pt_charge    ( 0 ),
    m_pt_pT        ( 0 ),
    m_pt_eta       ( 0 ),
    m_pt_phi       ( 0 ),
    m_pt_z0        ( 0 ),
    m_tr_eta       ( 0 ),
    m_tr_phi       ( 0 ),
    m_tr_pT        ( 0 ),
    m_tr_eta_b          ( 0 ),
    m_tr_eta_e          ( 0 ),
    m_tr_charge         ( 0 ),
    m_tr_z0             ( 0 ),
    n_el_cone_trk       ( 0 ),
    m_el_cone_trk_charge( 0 ),
    m_el_cone_trk_eta   ( 0 ),
    m_el_cone_trk_phi   ( 0 ),
    m_el_cone_trk_pt    ( 0 ),
    m_el_cone_trk_z0    ( 0 )  


//  m_mu3_Charge(-1),
//  m_mu3_ifComb(-1),
//  m_mu3_muPt(0),
//  m_mu3_muEta(0),
//  m_mu3_muPhi(0),
//  m_mu3_z0V(0),

{
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  Note that you should only put
    // the most basic initialization here, since this method will be
    // called on both the submission and the worker node.  Most of your
    // initialization code will go into histInitialize() and
    // initialize().
    VtxQualCut =20;
    R_cut =  0.6;
    m_deltaR = 0.5;
    m_skip=0;
    double deltaR     = 0.5;
    double el_cone_cut= deltaR;     //  Cone around Electron
    double el_cone_cut_z0 = 5.0;        //  Cone around Electron
//  double cut_z0 = 5.0;
}



EL::StatusCode BplusTagging :: setupJob (EL::Job& job)
{
    // Here you put code that sets up the job on the submission object
    // so that it is ready to work with your algorithm, e.g. you can
    // request the D3PDReader service or add output files.  Any code you
    // put here could instead also go into the submission script.  The
    // sole advantage of putting it here is that it gets automatically
    // activated/deactivated when you add/remove the algorithm from your
    // job, which may or may not be of value to you.

    job.useXAOD();
    EL_RETURN_CHECK("setupJob()", xAOD::Init());
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusTagging :: histInitialize ()
{
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.

    m_h_jpsi_mass  = new TH1D("h_JpsiMass" , "h_JpsiMass" , 2000,  0.0, 20000.0);
    m_h_jpsi_pT    = new TH1D("h_JpsiPt"   , "h_JpsiPt"   , 2000,  0.0, 60000.0);
    m_h_jpsi_rap   = new TH1D("h_JpsiRap"  , "h_JpsiRap"  , 2000, -2.6, 2.6    );
    m_h_bplus_mass = new TH1D("h_BplusMass", "h_BplusMass", 2000,  0.0, 20000.0);
    m_h_bplus_charge = new TH1D("h_BplusCharge", "h_BplusCharge", 22, -1.1, 1.1);
    m_h_bplus_pT   = new TH1D("h_BplusPt"  , "h_BplusPt"  , 2000,  0.0, 60000.0);
    m_h_bplus_rap  = new TH1D("h_BplusRap" , "h_BplusRap" , 2000, -2.6, 2.6    );

// define the config files

    wk()->addOutput(m_h_jpsi_mass);
    wk()->addOutput(m_h_jpsi_pT  );
    wk()->addOutput(m_h_jpsi_rap );
    wk()->addOutput(m_h_bplus_mass);
    wk()->addOutput(m_h_bplus_charge);
    wk()->addOutput(m_h_bplus_pT  );
    wk()->addOutput(m_h_bplus_rap );


    TFile *output_file = wk()->getOutputFile(output_name);
    m_tree = new TTree("tree", "tree");
    m_tree->SetDirectory(output_file);
    InitBranches();

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusTagging :: fileExecute ()
{
    // Here you do everything that needs to be done exactly once for every
    // single file, e.g. collect a list of all lumi-blocks processed
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusTagging :: changeInput (bool )
{
    // Here you do everything you need to do when we change input files,
    // e.g. resetting branch addresses on trees.  If you are using
    // D3PDReader or a similar service this method is not needed.
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusTagging :: initialize ()
{
    // Here you do everything that you need to do after the first input
    // file has been connected and before the first event is processed,
    // e.g. create additional histograms based on which variables are
    // available in the input files.  You can also create all of your
    // histograms and trees in here, but be aware that this method
    // doesn't get called if no events are processed.  So any objects
    // you create here won't be available in the output if you have no
    // input events.

    xAOD::TEvent* event = wk()->xaodEvent();
    Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int


    m_eventCounter = 0;

// GRL
   if(GRLFilePath.empty()){
    m_grl=nullptr;
   }else{
    m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
//const char* GRLFilePath = "$ALRB_TutorialData/data12_8TeV.periodAllYear_DetStatus-v61-pro14-02_DQDefects-00-01-00_PHYS_StandardGRL_All_Good.xml";
    const char* fullGRLFilePath = gSystem->ExpandPathName (GRLFilePath.c_str());
    std::vector<std::string> vecStringGRL;
    vecStringGRL.push_back(fullGRLFilePath);
    EL_RETURN_CHECK("initialize()",m_grl->setProperty( "GoodRunsListVec", vecStringGRL));
    EL_RETURN_CHECK("initialize()",m_grl->setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
    EL_RETURN_CHECK("initialize()",m_grl->initialize());
   }
    return EL::StatusCode::SUCCESS;
}




double SumPtSq(const xAOD::Vertex *PV_test) {
    double SumPt2 = 0;
    for(size_t NP=0; NP<PV_test->nTrackParticles(); NP++) {
        const xAOD::TrackParticle* tp = PV_test->trackParticle(NP);
        double pt_curr = tp->pt();
        SumPt2 += pt_curr*pt_curr;
    }
    return SumPt2;
}

const xAOD::Vertex* GetMaxPtPV(const xAOD::VertexContainer* container) {
    const xAOD::Vertex* PV =  nullptr;
    double maxSumPt2=0;
    for(size_t NV=0; NV<container->size(); NV++) {

        auto PV_test =  container->at(NV); // [NV];
        unsigned int ntrk = PV_test->nTrackParticles();
        if(ntrk > 2) {
            double SumPt2 = SumPtSq(PV_test);
            if(SumPt2 > maxSumPt2 ) {
                maxSumPt2 = SumPt2;
                PV = PV_test;
//              std::cout << " PV redefined from NV = " << NV << " with Ntrack = " << PV_test->nTrackParticles() << std::endl;
            }
        }
    }
    return PV;
}

const xAOD::Vertex* GetMinDeltaMin(double save_z0_from_Bpm, const vector<const xAOD::Vertex*> &Pvertices) {
    float delta_min = 10000.;
    const xAOD::Vertex* PV = nullptr;
    for(auto pv : Pvertices) {
        float z_itr = pv->z();
//      std::cout << " z_itr bcand_z = " << z_itr << " " << save_z0_from_Bpm << std::endl;
        float diff = TMath::Abs(z_itr - save_z0_from_Bpm);
        if(diff < delta_min) {
            delta_min = diff;
            PV = pv;
        }
    }
//  std::cout << " found delta_min = " << delta_min << std::endl;
    return PV;
}

vector<const xAOD::Vertex*> GetCandidateMinA0PVRemovingDuplicates(const xAOD::VertexContainer *bcandidates) {

    vector<const xAOD::Vertex*> list;
    list.reserve(bcandidates->size());
    for (const auto& bcand: (*bcandidates)) {
        xAOD::BPhysHelper bHelper( bcand );
        auto thePV = bHelper.pv( xAOD::BPhysHelper::PV_MIN_A0);
        list.push_back(thePV);
    }
    //This sort and unique will remove any duplicated PV
    std::sort(list.begin(), list.end());
    auto last = std::unique(list.begin(), list.end());
    list.erase(last, list.end());
    return list;
}

double deltaEta(const TVector3            & p1,const TVector3            & p2)  {
    // add safety measures?
    return (p1.Eta() - p2.Eta());
}
double deltaPhi(const TVector3            & p1,const TVector3            & p2)  {
    // note that this is unsigned
    return acos(cos(p1.Phi() - p2.Phi()));
}

std::vector<const xAOD::TrackParticle*>  selectTracksInCone(
    const std::vector< const xAOD::TrackParticle*> & allTracks,
    TVector3            & axis,
    double deltaR,
    const std::vector<const xAOD::TrackParticle*> & excludedTracks)      {

    std::vector<const xAOD::TrackParticle*> selectedTracks;

    for (auto track : allTracks) {
        if (!track) {
// off             ATH_MSG_WARNING("Null track in selectTracksInCone input");
            continue;
        }
        if ( find(excludedTracks.begin(), excludedTracks.end(), track) != excludedTracks.end() ) continue;
        TVector3     ptl( track->p4().Px(), track->p4().Py(), track->p4().Pz());

        double deta = deltaEta(ptl,axis);  // both arguments TVector3
        double dphi = deltaPhi(ptl,axis);
        double dR   = sqrt( deta*deta + dphi*dphi);
        if (dR > deltaR) continue; // remove the selected track
        // if here, track passed all selection requirements
        selectedTracks.push_back(track);

    } // for

    return selectedTracks;
}

template<typename T>
T NormalisetoPi(T input) {
    while(input > M_PI) {
        input = input -2.*M_PI;
    }
    while(input < -M_PI) {        //  bug corrected
        input = input +2.*M_PI;
    }
    return input;
}

EL::StatusCode BplusTagging :: execute ()
{
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.  This is where most of your actual analysis
    // code will go.


    xAOD::TEvent* event = wk()->xaodEvent();

    if ((m_eventCounter % 100) == 0)
        Info("execute()", "%i events prcessed so far", m_eventCounter);
    m_eventCounter++;

    const xAOD::EventInfo *eventInfo = nullptr;
    EL_RETURN_CHECK("execute()", event->retrieve(eventInfo, "EventInfo"));

    bool isMC = eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION);

// if data check if event passes GRL
    if(!isMC) { // it's data!
        if(!m_grl->passRunLB(*eventInfo)) {
            Info("execute()", "Skipping because of GRL");
            m_skip++;
            return EL::StatusCode::SUCCESS; // go to next event
        }
    } // end if not MC

    m_evt_number = eventInfo->eventNumber();
    m_lumi_block = eventInfo->lumiBlock  ();
    m_run_number = eventInfo->runNumber  ();

    const xAOD::VertexContainer     *importedPVCollection = nullptr;
    const xAOD::VertexContainer     *importedPVRefCollection = nullptr;
    EL_RETURN_CHECK("execute()", event->retrieve(importedPVCollection, pv_container_name.c_str()));
//  std::cout << "importedPVCollection size = " << importedPVCollection->size() << std::endl;

    EL_RETURN_CHECK("execute()",event->retrieve(importedPVRefCollection, pv_ref_container_name.c_str()));
//  std::cout << "importedPVRefCollection size = " << importedPVRefCollection->size() << std::endl;

    const xAOD::VertexContainer *bcandidates(nullptr);
    EL_RETURN_CHECK("execute()", event->retrieve( bcandidates   , bplus_container_name.c_str()));
//  std::cout << " Found " << bcandidates->size() << " B candidates" << std::endl;

    constexpr float el_mass = 0.511;
    constexpr float pi_mass = 139.6;
    double deltaR     = 0.5;
    double el_cone_cut= deltaR;     //  Cone around Electron
    double el_cone_cut_z0 = 5.0;        //  Cone around Electron

    const xAOD::MuonContainer   *importedMuonCollection = nullptr;
    const xAOD::ElectronContainer  *importedElectronCollection = nullptr;

    EL_RETURN_CHECK("execute()", event->retrieve(importedMuonCollection, muons_container_name.c_str()));
//  std::cout << "importedMuonCollection size = " << importedMuonCollection->size() << std::endl;
    EL_RETURN_CHECK("execute()", event->retrieve(importedElectronCollection, electrons_container_name.c_str()) );
    m_n_electrons= importedElectronCollection->size();
//  std::cout << "importedElectronCollection size = " << m_n_electrons << std::endl;
    m_n_muons    = importedMuonCollection->size();

    vector<const xAOD::Vertex*> allPV_minA0unique = GetCandidateMinA0PVRemovingDuplicates(bcandidates);

    for (const xAOD::Vertex *bcand: (*bcandidates)) {

//  select J/psi+1Track
        if( bcand->numberDoF () > 4) continue;
        if ( bcand->chiSquared()/bcand->numberDoF () >VtxQualCut) continue;
        VertexLinkVector prec_vtx = bcand->auxdata<VertexLinkVector>("PrecedingVertexLinks");
//      std::cout << " prec_vtx.size = " << prec_vtx.size() << std::endl;
        if (prec_vtx.size() != 1)
        {
            Warning("execute()", "Expected one (not %d) preceding vertices for B candidate, skip", prec_vtx.size());
            continue;

        }
//Is this used anywhere?
        auto MaxPtPV = GetMaxPtPV(importedPVCollection);

        xAOD::BPhysHelper bHelper( bcand );
        const xAOD::Vertex     *  thePV = bHelper.pv( xAOD::BPhysHelper::PV_MIN_A0);
        double save_z0_from_Bpm = bcand->z();

        auto LowMinDeltaPV = GetMinDeltaMin(save_z0_from_Bpm, allPV_minA0unique);

        m_trk_NtrkPV .push_back( LowMinDeltaPV->nTrackParticles() );
        m_n_B_candidates_loose_chi++;
        m_n_B_candidates_tight_chi++;

        float bplus_mass = bcand->auxdata<float>("Bplus_mass");
        bool bplus_passed=static_cast<bool>(bcand->auxdata<char>("passed_Bplus"));
        float bplus_mass_err=bcand->auxdata<float>("Bplus_massErr");

        constexpr float mmu = 105.658;
        constexpr float mkc = 493.677;
        constexpr float masses[3] = {mmu, mmu, mkc};

        std::array<float, 3> charges;

        m_B_mass.push_back( bplus_mass   );
        m_h_bplus_mass->Fill(bplus_mass);

//      std::cout << " aft m_h_bplus_mass "     << std::endl;
        m_B_mass_err.push_back( bplus_mass_err );
        m_B_passed.push_back(     bplus_passed );
//      std::cout << " aft m_B_passed.push"     << std::endl;
        std::array<TLorentzVector, 3> lor_trk;
        int loop = 0;
        for (auto &tpel : bcand->trackParticleLinks()) {
            auto &p =  (*tpel)->p4();
            lor_trk[loop].SetXYZM(p.Px(), p.Py(), p.Pz(), masses[loop]); //This will calculate energy for you
            charges[loop] = (*tpel)->charge();
            loop++;
        }

        TLorentzVector lor_trkB = lor_trk[0] + lor_trk[1] + lor_trk[2];

        float B_eta = lor_trkB.Eta();
        m_B_rapidity.push_back( B_eta );
        float B_pt  = lor_trkB.Pt();
        float B_pT_err   = bcand->auxdata<float>("PtErr" );
        float B_phi = lor_trkB.Phi();

        float B_Lxy      = bcand->auxdata<float>("LxyMaxSumPt2" );
        float B_Lxy_err  = bcand->auxdata<float>("LxyErrMaxSumPt2" );
        float B_A0       = bcand->auxdata<float>("A0MaxSumPt2" );
        float B_A0_err   = bcand->auxdata<float>("A0ErrMaxSumPt2" );
        float B_A0xy     = bcand->auxdata<float>("A0xyMaxSumPt2" );
        float B_A0xy_err = bcand->auxdata<float>("A0xyErrMaxSumPt2" );
        float B_z0       = bcand->auxdata<float>("Z0MaxSumPt2" );
        float B_z0_err   = bcand->auxdata<float>("Z0ErrMaxSumPt2" );
        float B_tau      = bcand->auxdata<float>("Bplus_TauConstMassPVMaxSumPt2" );
        float B_tau_err  = bcand->auxdata<float>("Bplus_TauErrConstMassPVMaxSumPt2" );

        m_B_pT.push_back( B_pt );
// out  m_B_trk_charge.push_back( bplus_charge );
        m_B_pT_err   .push_back( B_pT_err );
        m_B_phi .push_back( B_phi );
        m_B_Lxy      .push_back( B_Lxy );
        m_B_Lxy_err  .push_back( B_Lxy_err );
        m_B_A0       .push_back( B_A0 );
        m_B_A0_err   .push_back( B_A0_err );
        m_B_A0xy     .push_back( B_A0xy );
        m_B_A0xy_err .push_back( B_A0xy_err );
        m_B_Z0       .push_back( B_z0 );
        m_B_Z0_err   .push_back( B_z0_err );
        m_B_tau      .push_back( B_tau );
        m_B_tau_err  .push_back( B_tau_err );
//      std::cout << " aft m_B_tau_err "   << std::endl;
        TLorentzVector &muon0_ref_track = lor_trk[0];
        TLorentzVector &muon1_ref_track = lor_trk[1];
        TLorentzVector &kaon_ref_track  = lor_trk[2];
        m_B_mu1_eta.push_back(muon0_ref_track.Eta());
        m_B_mu2_eta.push_back(muon1_ref_track.Eta());
        m_B_trk_eta.push_back(kaon_ref_track .Eta());
        m_B_mu1_pT .push_back(muon0_ref_track.Pt ());
        m_B_mu2_pT .push_back(muon1_ref_track.Pt ());
        m_B_trk_pT .push_back(kaon_ref_track .Pt ());
        m_B_mu1_charge.push_back( charges[0] );
        m_B_mu2_charge.push_back( charges[1] );
        m_B_trk_charge.push_back( charges[2] );

// J/psi parameters from mu1 and mu2
        m_Jpsi_mass  .push_back( (lor_trk[0]+lor_trk[1]).M() );
        m_Jpsi_rapidity.push_back( (lor_trk[0]+lor_trk[1]).Eta() );
        m_Jpsi_pT      .push_back( (lor_trk[0]+lor_trk[1]).Pt () );
//      std::cout << " aft m_Jpsi_pT    "  << std::endl;
        int if_mu3 = 0;

        double ptmax = 0;
        float mu3_Charge;
        float mu3_muPt;
        float mu3_muEta;
        float mu3_muPhi;
        int mu3_type = -1;
        const xAOD::TrackParticle* muonTrk_keep=nullptr;
//
//   Begin MuonChargeTag
//
        if(m_n_muons > 2) {
            for (auto muItr=importedMuonCollection->begin(); muItr!=importedMuonCollection->end(); ++muItr) {
                if (!(*muItr)->inDetTrackParticleLink().isValid()) continue; // No muons without ID tracks
                const xAOD::TrackParticle* muonTrk = (*muItr)->inDetTrackParticleLink().cachedElement();
//              std::cout << " aft muonTrk      "  << std::endl;
//              std::cout << " aft muon0_ref Eta"  << muon0_ref_track.Eta () << std::endl;
//              std::cout << " aft muonTrk link "  << muonTrk << std::endl;
//              std::cout << " mu.isValid()= " << (*muItr)->inDetTrackParticleLink().isValid() << std::endl;
//              std::cout << " m_persIndex = " << (*muItr)->combinedTrackParticleLink() << std::endl;
//              std::cout << " muonTrk_2.Eta = " << (*muItr)->eta() << std::endl;
//              std::cout << " muonTrk_2.Phi = " << (*muItr)->phi() << std::endl;
//              std::cout << " muonTrk_2.Pt  = " << (*muItr)->pt () << std::endl;
//              std::cout << " muonTrk_2.char= " << (*muItr)->charge() << std::endl;
//              std::cout << " aft muonTrk_2    "  << std::endl;
                int if_mu1 = ( fabs((*muItr)->eta() - muon0_ref_track.Eta ())<0.01) &&
                             ( fabs((*muItr)->phi() - muon0_ref_track.Phi ())<0.01) ; //in C++ true = 1 and false=0
                int if_mu2 = ( fabs((*muItr)->eta() - muon1_ref_track.Eta ())<0.01) &&
                             ( fabs((*muItr)->phi() - muon1_ref_track.Phi ())<0.01) ;

//              std::cout << " aft muon match mu1 2 " << if_mu1 << " " << if_mu2 << std::endl;
                if( if_mu1 == 1) {
                    int mu1_type= (*muItr)->muonType() == xAOD::Muon::Combined;
//                  std::cout << " mu1_type = " << mu1_type << std::endl;
                    m_B_mu1_type.push_back( mu1_type );
//                  std::cout << " art mu1_type     "  << std::endl;
                }
                if( if_mu2 == 1) {
                    int mu2_type= (*muItr)->muonType() == xAOD::Muon::Combined ;
                    m_B_mu2_type.push_back( mu2_type );
//                  std::cout << " art mu1_type     "  << std::endl;
                }



                if( (if_mu1 != 1) && (if_mu2 != 1) ) {
//   protection
//
//                  std::cout << " mu3Comb = " << (*muItr)->muonType() << std::endl;
                    mu3_type = (*muItr)->muonType();
//                  std::cout << " inDetTrackParticleLink = " << (*muItr)->inDetTrackParticleLink() << std::endl;

                    if( *(*muItr)->inDetTrackParticleLink() != 0) {
                        if_mu3++;
                        if( (*muItr)->pt() > ptmax ) {
                            ptmax = (*muItr)->pt();
                            mu3_type = (*muItr)->muonType() == xAOD::Muon::Combined;
                            mu3_Charge = (*muItr)->charge();
                            mu3_muPt   = (*muItr)->pt();
                            mu3_muEta  = (*muItr)->eta();
                            mu3_muPhi  = (*muItr)->phi();
                            muonTrk_keep = (*muItr)->inDetTrackParticleLink().cachedElement();
//                          std::cout << " art mu3_type     "  << std::endl;

//                          std::cout << " aft muon z0 " << std::endl;
                        }  //  select ptmax
                    }   //  muonTrk_keep != 0
                }    //  if_mu1 != 1) && (if_mu2 != 1
            }    //  loop over muItr
        }    //  m_n_muons > 2
//      std::cout << " aft m_n_muons > 2 " << std::endl;
        if(if_mu3 > 0) {
            m_B_mu3_type .push_back( mu3_type );
            m_mu3_ifComb .push_back( mu3_type );
            m_mu3_Charge.push_back(  mu3_Charge);
            m_mu3_muPt  .push_back(  mu3_muPt);
            m_mu3_muEta .push_back(  mu3_muEta);
            m_mu3_muPhi .push_back(  mu3_muPhi);
            //  added 35 lines
            if( muonTrk_keep !=0 ) {
                //  ckeck impact parameter w.r.t. selected PV
//              std::cout << " inportedMuonCollection      .size = " << importedMuonCollection->size()      << std::endl;
//              //    std::cout << " muonTrk_keep->z0 = " << muonTrk_keep->z0() << std::endl;
                //  possibly the muonTrk_keep os assigned to another Vertex
                double d0V, z0V;
                BPhysFunctions::track_to_vertex( muonTrk_keep,      LowMinDeltaPV   , &d0V, &z0V);
                m_mu3_z0V .push_back( (float)z0V );
            }   // if( muonTrk_keep !=0 )
        }   // if(if_mu3 > 0)
//
//   End   MuonChargeTag
//

        m_el_chi2 .push_back(  bcand->chiSquared() );
        el_cone_ch_estimator = 0;
        el_cone_ch_estimator2 = 0;
        m_n_el_tight = 0;
        for(auto elItr=importedElectronCollection->begin(); elItr !=importedElectronCollection->end();   elItr++) {
         if( (* elItr) != NULL ) {
//  init
            double charge_el_cone_trk = 0;
            double charge_el_cone_trk2 = 0;
            double charge_el_cone_trk_tmp = 0;
            double pt_norm = 0;
            int int0 = 0;
            int int1 = 1;
            int intX = 0;
            int fill_Trk_keep = 0;
            int fill_z0_cut = 0;
            int fill_cos98  = 0;
            int fill_Med    = 0;
            float default_val = -1000.;
            bool  cut_val; //  = 0.5;
            bool  cut_valMed; //  = 0.5;
                  cut_val = false;
                  cut_valMed = false;
//          bool  value = (*elItr)->passSelection(cut_val,"LHTight") ;
            bool  value = (*elItr)->passSelection(cut_val,"Tight") ;
//          if( value ) { cout << " value = true " << endl; }
//              else    { cout << " value = false" << endl; }
//          m_el_ifTight  .push_back( cut_val );
           if( cut_val && value ) { m_el_ifTight  .push_back( int1 ); }
                   else  { m_el_ifTight  .push_back( int0 ); }
            if( cut_val>0 && value ) {
                m_n_el_tight++;
            }
//          bool  value2= (*elItr)->passSelection(cut_valMed,"LHMedium") ;
            bool  value2= (*elItr)->passSelection(cut_valMed,"Medium") ;
            cout << " value2 " << value2 << " " << cut_valMed << endl;
//          m_el_ifMedium .push_back( cut_val );
           if( cut_valMed && value2 ) { m_el_ifMedium  .push_back( int1 ); 
                              intX = int1; }
                      else  { m_el_ifMedium .push_back( int0 );
                              intX = int0; }
//            bool  value3= (*elItr)->passSelection(cut_val,"LHLoose") ;
//          m_el_ifLoose  .push_back( cut_val );
//          std::cout << " el value = " << value << " cut_val " << cut_val<< std::endl;
            //  value==1 means that the structure found; cut_val==0, 1 means false/True
//          if( cut_val || cut_valMed) {
            if( intX == int1 )         {
                fill_Med    = 1;
                m_el_author .push_back( (*elItr)-> author() );
//              std::cout << " aft inDetTrackParticleLink() " << std::endl;
                auto el_Trk_keep = (*elItr)->  trackParticle(0);  // 20 Oct
                m_el_charge .push_back( (*elItr) -> charge() );
//              std::cout << " elItr ->  nTrackParticles() " <<(* elItr) -> nTrackParticles( ) << std::endl;  // 20 Oct
                m_el_eta    .push_back( (*elItr) -> eta() );
                m_el_phi    .push_back( (*elItr) -> phi() );
                m_el_pT     .push_back( (*elItr) -> pt() );
                TLorentzVector lor_el;
                lor_el.SetPtEtaPhiM((*elItr) -> pt(), (*elItr) -> eta(), (*elItr) -> phi(), el_mass);
//              std::cout << " aft elItr) -> trackParticle() -> z0() " << std::endl;
//  EgammaHelpers' has not been declared
                if(el_Trk_keep != NULL) {
//               std::cout << " inside (el_Trk_keep != NULL) " << std::endl;
                 double d0V, z0V;
                 BPhysFunctions::track_to_vertex( el_Trk_keep,      LowMinDeltaPV   , &d0V, &z0V);
                 m_el_z0     .push_back( z0V);
                 fill_z0_cut = 1;
                 double sinth = sin(2.*TMath::ATan( exp(-(*elItr)-> eta())) );
//               if(sinth*TMath::Abs( z0V < cut_z0) ) {
                 if(      TMath::Abs( z0V < cut_z0) ) {
//                double bb_px = bcand->auxdata<double>("Px");
//                double bb_py = bcand->auxdata<double>("Py");
//                double bb_pz = bcand->auxdata<double>("Pz");
                  double bb_px = lor_trkB.Px(); 
                  double bb_py = lor_trkB.Py(); 
                  double bb_pz = lor_trkB.Pz(); 
                  auto &elp4 = (*elItr)->p4();
                  double el_px = elp4.Px();       
                  double el_py = elp4.Py();       
                  double el_pz = elp4.Pz();       
                  double cos_bbel = (bb_px*el_px +bb_py*el_py +bb_pz*el_pz)/( TMath::Sqrt(bb_px*bb_px +bb_py*bb_py +bb_pz*bb_pz)*TMath::Sqrt(el_px*el_px +el_py*el_py +el_pz*el_pz) );
                  if( cos_bbel < 0.98 ) {
                   el_cone_ch_estimator = (*elItr) -> charge();
                   fill_cos98  = 1;
//   tracks in a Cone around Electron
                    for (auto tpel: LowMinDeltaPV   ->trackParticleLinks()) {
                        TLorentzVector lor_tr;
//                      constexpr float pi_mass = 139.6;
                        lor_tr.SetPtEtaPhiM((*tpel)-> pt(), (*tpel)-> eta(), (*tpel)->phi(), pi_mass);
//  needed code from new Helper


                        double delta_eta = lor_tr.Eta() - lor_el.Eta();
                        double delta_phi = NormalisetoPi(lor_tr.Phi() - lor_el.Phi());

                        double delta_R = TMath::Sqrt(delta_eta*delta_eta +delta_phi*delta_phi);
//                      std::cout << " delta_eta " << delta_eta << " " <<
//                                delta_phi << " " <<
//                                delta_R   << " " <<
//                                R_cut     << std::endl;
                        if(delta_R < R_cut) {
//   put protection from tracks close tp B+- direction
//                          std::cout << "  in R_cut   " << std::endl;
                            delta_eta = lor_tr.Eta() - lor_trkB.Eta();
                            delta_phi = NormalisetoPi(lor_tr.Phi() - lor_trkB.Phi());

                            if(delta_R < R_cut) {
//   store tracks in Cone around Electron
//                              std::cout << " tr_eta tr_phi " << lor_tr.Eta() << " " <<
//                                        lor_tr.Phi() << std::endl;
                                m_tr_eta  .push_back( lor_tr.Eta()   );
                                m_tr_phi  .push_back( lor_tr.Phi()   );
                                m_tr_pT   .push_back( lor_tr.Pt()    );
                                m_tr_eta_b.push_back( lor_trkB.Eta() );
                                m_tr_eta_e.push_back( lor_el.Eta()   );
                                m_tr_charge.push_back( (*tpel)->charge() );
                                double d0V, z0V;
                                BPhysFunctions::track_to_vertex( (*tpel)       ,      LowMinDeltaPV   , &d0V, &z0V);
                                m_tr_z0    .push_back( z0V);
                            }
                        }
                    }   //   loop over tracks in PV  54 lines


                    std::vector<const xAOD::TrackParticle*>  allTracks(LowMinDeltaPV->nTrackParticles());
                    std::vector<const xAOD::TrackParticle*>  excludedTracks(bcand->nTrackParticles());
                    
                    for(size_t ItrTrk=0; ItrTrk<LowMinDeltaPV->nTrackParticles(); ItrTrk++) {
                        const xAOD::TrackParticle* tp = LowMinDeltaPV    ->trackParticle( ItrTrk );
                        allTracks[ItrTrk] =    tp;
                    }
                    for(size_t ItrTrk=0; ItrTrk<bcand->nTrackParticles(); ItrTrk++) {
                        const xAOD::TrackParticle* tp = bcand ->trackParticle( ItrTrk );
                        excludedTracks[ItrTrk]  = tp;
                    }

                    TVector3 axis;
                    TVector3 vtx;
                    auto &elp = (*elItr)->p4();
                    axis. SetXYZ( elp.Px(), elp.Py(), elp.Pz() );
                    vtx.  SetXYZ( LowMinDeltaPV->x()   , LowMinDeltaPV->y()   , LowMinDeltaPV->z()   );
//                  std::cout << " aft vtx ->SetXYZ " << std::endl;
                    int  n_selectTracksInCone = 0;
                    std::vector<const xAOD::TrackParticle*> tracksInCone =
                        selectTracksInCone( allTracks, axis, m_deltaR , excludedTracks );
//                  std::cout << " aft tracksInCone " << tracksInCone.size()  <<" bplus_mass = " << bplus_mass << " tau = " << B_tau << std::endl;

//                  if(5239.<bplus_mass && bplus_mass<5319. && B_tau > 0.2 && !tracksInCone.empty()) {            // mass cut
                    if(5190.<bplus_mass && bplus_mass<5370. && B_tau > 0.2 && !tracksInCone.empty()) {            // mass cut
                        for(size_t icon=0; icon<tracksInCone.size(); icon++) {
//                          std::cout << " in el_cone_trk fill " << std::endl;
                            float el_cone_trk_eta = tracksInCone[icon]->eta();
                            float el_cone_trk_phi = tracksInCone[icon]->phi();
                            float el_cone_trk_pt  = tracksInCone[icon]->pt ();
                            float el_cone_trk_charge = tracksInCone[icon]->charge();
                            double d0V, z0V;
                            BPhysFunctions::track_to_vertex(tracksInCone[icon],bcand, &d0V, &z0V);
                            float el_cone_trk_z0     = z0V  ;
                            n_selectTracksInCone++;
                            m_el_cone_trk_charge.push_back( el_cone_trk_charge );
                            m_el_cone_trk_eta.push_back( el_cone_trk_eta );
                            m_el_cone_trk_phi.push_back( el_cone_trk_phi );
                            m_el_cone_trk_pt.push_back( el_cone_trk_pt );
                            m_el_cone_trk_z0.push_back( el_cone_trk_z0 );
                            TLorentzVector el_cone_trk_lor;
                            el_cone_trk_lor.SetPtEtaPhiM(el_cone_trk_pt, el_cone_trk_eta, el_cone_trk_phi, pi_mass);
//                          elp == el_lor;
                            double ang_el_cone_trk =  elp.Angle(el_cone_trk_lor.Vect());
                            double eta_diff = elp.Eta() - el_cone_trk_lor.Eta();
                            double el_cone = TMath::Sqrt(ang_el_cone_trk*ang_el_cone_trk +eta_diff*eta_diff );
//                          std::cout <<" ang_el_cone_trk eta_diff el_cone " << ang_el_cone_trk << " " << eta_diff << " " << el_cone << std::endl;
                            if(el_cone < el_cone_cut && TMath::Abs(  el_cone_trk_z0         ) < el_cone_cut_z0 ) {
                             charge_el_cone_trk_tmp = charge_el_cone_trk_tmp +el_cone_trk_charge*el_cone_trk_pt; 
                             pt_norm =   pt_norm + el_cone_trk_pt;
                            }
                        }      //  loop over tracksInCone
                        charge_el_cone_trk_tmp = charge_el_cone_trk_tmp +el_cone_ch_estimator;
                        n_el_cone_trk .  push_back( n_selectTracksInCone );
                        if( pt_norm !=0) {
                          charge_el_cone_trk2  = charge_el_cone_trk_tmp/pt_norm;
                          pt_norm = pt_norm +  (*elItr) -> pt() ;
                          el_cone_ch_estimator2 = charge_el_cone_trk2;
                        }
//                       clean tracks in cone, if n_selectTracksInCone not good
                       
                    } // mass and lifetime selection
                  }   // reject electrons in Forward Jet
                 }   // z0 cut for electron
                }     // el_Trk_keep check
            }    //  Tight or Medium or Loose  selection  if( cut_val>0 )
            if(fill_z0_cut ==0) { m_el_z0     .push_back( default_val); }
            if(fill_cos98  ==0) { el_cone_ch_estimator = default_val ; }
            if(fill_Med    ==0) { el_cone_ch_estimator2 = default_val;  
                m_el_author .push_back( default_val    );
//              std::cout << " fill default " << std::endl;
                m_el_charge .push_back( default_val    );
                m_el_eta    .push_back( default_val    );
                m_el_phi    .push_back( default_val    );
                m_el_pT     .push_back( default_val    );
                                }
         }     //  non-zero (* elIt
        }   //   loop over electrons
        m_el_cone_ch_estimator.push_back( el_cone_ch_estimator );
        m_el_cone_ch_estimator2.push_back( el_cone_ch_estimator2 );
    }   //   end if filling  (loop over B-candidates)
    if ((m_eventCounter % 100) == 0)
    {
        Info("execute()", "%i at m_tree->Fill() events prcessed so far", m_eventCounter);
    }

    m_tree->Fill();
    ClearBranches();

    return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusTagging :: postExecute ()
{
    // Here you do everything that needs to be done after the main event
    // processing.  This is typically very rare, particularly in user
    // code.  It is mainly used in implementing the NTupleSvc.
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusTagging :: finalize ()
{
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.  This is different from histFinalize() in that it only
    // gets called on worker nodes that processed input events.
    Info("finialize()", "Skip GRL %d", m_skip);
    return EL::StatusCode::SUCCESS;
}

void BplusTagging::ClearBranches() {
    m_run_number = -1;
    m_lumi_block = -1;
    m_evt_number = -1;
    m_B_mass       .clear();
    m_B_mass_err   .clear();
    m_B_passed     .clear();
    m_B_rapidity   .clear();
    m_B_pT         .clear();
    m_B_phi        .clear();
    m_B_pT_err     .clear();
    m_B_Lxy        .clear();
    m_B_Lxy_err    .clear();
    m_B_A0         .clear();
    m_B_A0_err     .clear();
    m_B_A0xy       .clear();
    m_B_A0xy_err   .clear();
    m_B_Z0         .clear();
    m_B_Z0_err     .clear();
    m_B_tau        .clear();
    m_B_tau_err    .clear();
    m_B_chi2       .clear();
    m_B_mu1_type   .clear();
    m_B_mu1_eta    .clear();
    m_B_mu1_pT     .clear();
    m_B_mu1_charge .clear();
    m_B_mu2_type   .clear();
    m_B_mu2_eta    .clear();
    m_B_mu2_pT     .clear();
    m_B_mu2_charge .clear();
    m_B_trk_eta    .clear();
    m_B_trk_pT     .clear();
    m_B_trk_charge .clear();
    m_B_Jpsi_index .clear();
    m_B_Jpsi_mass  .clear();
    m_Jpsi_mass    .clear();
    m_Jpsi_rapidity.clear();
    m_Jpsi_pT      .clear();
    m_Jpsi_chi2    .clear();
    m_n_muons =  0;
    m_B_mu3_type   .clear();
    m_mu3_Charge   .clear();
    m_mu3_ifComb   .clear();
    m_mu3_muPt     .clear();
    m_mu3_muEta    .clear();
    m_mu3_muPhi    .clear();
    m_mu3_z0V      .clear();
    m_trk_NtrkPV   .clear();
    m_n_electrons = 0;
    m_n_el_tight  = 0;
    m_el_author    .clear();
    m_el_charge    .clear();
    m_el_eta       .clear();
    m_el_phi       .clear();
    m_el_pT        .clear();
    m_el_z0        .clear();
    m_el_ifTight   .clear();
    m_el_ifMedium  .clear();
    m_el_ifLoose   .clear();
    m_pt_charge    .clear();
    m_pt_pT        .clear();
    m_pt_eta       .clear();
    m_pt_phi       .clear();
    m_pt_z0        .clear();
    m_tr_eta  .clear();
    m_tr_phi  .clear();
    m_tr_pT   .clear();
    m_tr_eta_b.clear();
    m_tr_eta_e.clear();
    m_tr_charge.clear();
    m_tr_z0    .clear();
    n_el_cone_trk .clear();
    m_el_cone_trk_charge.clear();
    m_el_cone_trk_eta.clear();
    m_el_cone_trk_phi.clear();
    m_el_cone_trk_pt.clear();
    m_el_cone_trk_z0.clear();
    m_el_chi2  .clear();
    m_el_cone_ch_estimator .clear();
    m_el_cone_ch_estimator2.clear();
}

void BplusTagging::InitBranches() {
    m_tree->Branch("run_number"   , &m_run_number   );
    m_tree->Branch("lumi_block"   , &m_lumi_block   );
    m_tree->Branch("evt_number"   , &m_evt_number   );
    m_tree->Branch("B_mass"       , &m_B_mass       );
    m_tree->Branch("B_mass_err"   , &m_B_mass_err   );
    m_tree->Branch("B_passed"     , &m_B_passed    );
    m_tree->Branch("B_rapidity"   , &m_B_rapidity   );
    m_tree->Branch("B_pT"         , &m_B_pT         );
    m_tree->Branch("B_phi"        , &m_B_phi        );
    m_tree->Branch("B_pT_err"     , &m_B_pT_err     );
    m_tree->Branch("B_Lxy"        , &m_B_Lxy        );
    m_tree->Branch("B_Lxy_err"    , &m_B_Lxy_err    );
    m_tree->Branch("B_A0"         , &m_B_A0         );
    m_tree->Branch("B_A0_err"     , &m_B_A0_err     );
    m_tree->Branch("B_A0xy"       , &m_B_A0xy       );
    m_tree->Branch("B_A0xy_err"   , &m_B_A0xy_err   );
    m_tree->Branch("B_Z0"         , &m_B_Z0         );
    m_tree->Branch("B_Z0_err"     , &m_B_Z0_err     );
    m_tree->Branch("B_tau"        , &m_B_tau        );
    m_tree->Branch("B_tau_err"    , &m_B_tau_err    );
    m_tree->Branch("B_chi2"       , &m_B_chi2       );
    m_tree->Branch("B_mu1_type"   , &m_B_mu1_type   );
    m_tree->Branch("B_mu1_eta"    , &m_B_mu1_eta    );
    m_tree->Branch("B_mu1_pT"     , &m_B_mu1_pT     );
    m_tree->Branch("B_mu1_charge" , &m_B_mu1_charge );
    m_tree->Branch("B_mu2_type"   , &m_B_mu2_type   );
    m_tree->Branch("B_mu2_eta"    , &m_B_mu2_eta    );
    m_tree->Branch("B_mu2_pT"     , &m_B_mu2_pT     );
    m_tree->Branch("B_mu2_charge" , &m_B_mu2_charge );
    m_tree->Branch("B_trk_eta"    , &m_B_trk_eta    );
    m_tree->Branch("B_trk_pT"     , &m_B_trk_pT     );
    m_tree->Branch("B_trk_charge" , &m_B_trk_charge );
    m_tree->Branch("B_Jpsi_index" , &m_B_Jpsi_index );
    m_tree->Branch("B_Jpsi_mass"  , &m_B_Jpsi_mass  ); // this can be used to debug B_Jpsi_index (compare B_Jpsi_mass and Jpsi_mass->at(B_Jpsi_index)), should be same
    m_tree->Branch("Jpsi_mass"    , &m_Jpsi_mass    );
    m_tree->Branch("Jpsi_rapidity", &m_Jpsi_rapidity);
    m_tree->Branch("Jpsi_pT"      , &m_Jpsi_pT      );
    m_tree->Branch("Jpsi_chi2"    , &m_Jpsi_chi2    );
    m_tree->Branch("Nmuons"       , &m_n_muons      );
    m_tree->Branch("m_B_mu3_type" , &m_B_mu3_type   );
    m_tree->Branch("m_mu3_Charge" , &m_mu3_Charge   );
    m_tree->Branch("m_mu3_ifComb" , &m_mu3_ifComb   );
    m_tree->Branch("m_mu3_muPt"   , &m_mu3_muPt     );
    m_tree->Branch("m_mu3_muEta"  , &m_mu3_muEta    );
    m_tree->Branch("m_mu3_muPhi"  , &m_mu3_muPhi    );
    m_tree->Branch("m_mu3_z0V"    , &m_mu3_z0V      );
    m_tree->Branch("m_trk_NtrkPV" , &m_trk_NtrkPV   );
    m_tree->Branch("N_el"         , &m_n_electrons  );
    m_tree->Branch("N_el_tight"   , &m_n_el_tight   );
    m_tree->Branch("m_el_author"  , &m_el_author    );
    m_tree->Branch("m_el_charge"  , &m_el_charge    );
    m_tree->Branch("m_el_eta"     , &m_el_eta       );
    m_tree->Branch("m_el_phi"     , &m_el_phi       );
    m_tree->Branch("m_el_pT"      , &m_el_pT        );
    m_tree->Branch("m_el_z0"     , &m_el_z0         );
    m_tree->Branch("m_el_ifTight" , &m_el_ifTight   );
    m_tree->Branch("m_el_ifMedium", &m_el_ifMedium  );
    m_tree->Branch("m_el_ifLoose" , &m_el_ifLoose   );
    m_tree->Branch("m_pt_charge"  , &m_pt_charge    );
    m_tree->Branch("m_pt_pT"      , &m_pt_pT        );
    m_tree->Branch("m_pt_eta"     , &m_pt_eta       );
    m_tree->Branch("m_pt_phi"     , &m_pt_phi       );
    m_tree->Branch("m_pt_z0"      , &m_pt_z0        );
    m_tree->Branch("m_tr_eta"   , &m_tr_eta          );
    m_tree->Branch("m_tr_phi"   , &m_tr_phi          );
    m_tree->Branch("m_tr_pT"    , &m_tr_pT           );
    m_tree->Branch("m_tr_eta_b" , &m_tr_eta_b        );
    m_tree->Branch("m_tr_eta_e" , &m_tr_eta_e        );
    m_tree->Branch("m_tr_charge", &m_tr_charge       );
    m_tree->Branch("m_tr_z0"    , &m_tr_z0           );
    m_tree->Branch("n_el_cone_trk", &n_el_cone_trk   );

    m_tree->Branch("m_el_cone_trk_charge", &m_el_cone_trk_charge );
    m_tree->Branch("m_el_cone_trk_eta", &m_el_cone_trk_eta );
    m_tree->Branch("m_el_cone_trk_phi", &m_el_cone_trk_phi );
    m_tree->Branch("m_el_cone_trk_pt",  &m_el_cone_trk_pt );
    m_tree->Branch("m_el_cone_trk_z0",  &m_el_cone_trk_z0 );
//  m_el_chi2->Branch("m_el_chi2",  &m_el_chi2 );
    m_tree   ->Branch("m_el_chi2",  &m_el_chi2 );
    m_tree   ->Branch("m_el_cone_ch_estimator", &m_el_cone_ch_estimator );
    m_tree   ->Branch("m_el_cone_ch_estimator2",&m_el_cone_ch_estimator2);
}

EL::StatusCode BplusTagging :: histFinalize ()
{
    // This method is the mirror image of histInitialize(), meaning it
    // gets called after the last event has been processed on the worker
    // node and allows you to finish up any objects you created in
    // histInitialize() before they are written to disk.  This is
    // actually fairly rare, since this happens separately for each
    // worker node.  Most of the time you want to do your
    // post-processing on the submission node after all your histogram
    // outputs have been merged.  This is different from finalize() in
    // that it gets called on all worker nodes regardless of whether
    // they processed input events.
    return EL::StatusCode::SUCCESS;
}

