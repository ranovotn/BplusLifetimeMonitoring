#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include <BplusxAODAnalysis/BplusTruthToNtuple.h>
#include "xAODEventInfo/EventInfo.h"
#include "TFile.h"

/// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )                     \
   do {                                                     \
      if( ! EXP.isSuccess() ) {                             \
         Error( CONTEXT,                                    \
                XAOD_MESSAGE( "Failed to execute: %s" ),    \
                #EXP );                                     \
         return EL::StatusCode::FAILURE;                    \
      }                                                     \
   } while( false )


// this is needed to distribute the algorithm to the workers
ClassImp(BplusTruthToNtuple)



BplusTruthToNtuple :: BplusTruthToNtuple () : output_name("DefaultOutput"), TrueDecaysFound(0), TrueDecaysFoundOver1(0)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  
   m_event_counter=0;
}



EL::StatusCode BplusTruthToNtuple :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  
  job.useXAOD();
  EL_RETURN_CHECK("setupJob()", xAOD::Init());
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusTruthToNtuple :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusTruthToNtuple :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusTruthToNtuple :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusTruthToNtuple :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  
  TFile *output_file = wk()->getOutputFile(output_name);
  m_tree         = new TTree("True_Bplus", "True_Bplus");
  m_tree        ->SetDirectory(output_file);
  
  BplusDecayFinder = std::unique_ptr<xAOD::TruthNtupleNode>( new xAOD::TruthNtupleNode(521, "Bplus") );
  auto *jpsi = BplusDecayFinder->addChild(443, "Jpsi");
  jpsi->addChild(-13, "Muplus");
  jpsi->addChild(13, "Muminus");
  BplusDecayFinder->addChild(321, "Kplus");
  BplusDecayFinder->print();
  BplusDecayFinder->Book(m_tree, &m_run_number,
            &m_lumi_block, &m_evt_number);
  
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusTruthToNtuple :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  
  xAOD::TEvent *event = wk()->xaodEvent();


  if ((m_event_counter % 100) == 0)
    Info("execute()", "%i events prcessed so far", m_event_counter);

  m_event_counter++;

  const xAOD::EventInfo *event_info = nullptr;
  EL_RETURN_CHECK("execute()", event->retrieve(event_info, "EventInfo"));


  m_evt_number = event_info->eventNumber();
  m_lumi_block = event_info->lumiBlock  ();
  m_run_number = event_info->runNumber  ();

//  bool is_mc = event_info->eventType(xAOD::EventInfo::IS_SIMULATION);

  const xAOD::TruthParticleContainer* xTruthParticleContainer = nullptr;

  if(!event->retrieve( xTruthParticleContainer, "TruthParticles").isSuccess()) {
     Error("execute()", "Failed to find TruthParticles");
     return EL::StatusCode::FAILURE;
  }

  size_t truthsfound = BplusDecayFinder->FindTruth(xTruthParticleContainer, false, false);
  TrueDecaysFound+=truthsfound;
  TrueDecaysFoundOver1+=truthsfound>1;  

  BplusDecayFinder->PrepareForFill();
  m_tree->Fill();
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusTruthToNtuple :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusTruthToNtuple :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  
  Info("finalize()", "TrueDecaysFound: %i"     , TrueDecaysFound           );
  Info("finalize()", "TrueDecaysFoundOver1: %i"     , TrueDecaysFoundOver1           );  
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusTruthToNtuple :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
