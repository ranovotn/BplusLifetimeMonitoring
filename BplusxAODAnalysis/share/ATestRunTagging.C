void ATestRunTagging(const std::string& submitDir)
  {
  //===========================================
  // FOR ROOT6 WE DO NOT PUT THIS LINE 
  // (ROOT6 uses Cling instead of CINT)
  // Load the libraries for all packages
  // gROOT->Macro("$ROOTCOREDIR/scripts/load_packages.C");
  // Instead on command line do:
  // > root -l '$ROOTCOREDIR/scripts/load_packages.C' 'ATestRun.cxx ("submitDir")'
  // The above works for ROOT6 and ROOT5
  //==========================================
  bool submittoGrid = false;
  xAOD::Init().ignore();
  SH::SampleHandler sh;

  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  // inputFilePath needsto be changed to your local input file 

//  const char *inputFilePath = "/scratch/ab/xAODBplusWork/user.abarton.mc15_13TeV.300999.Bplus.e4064_a766_a769_r6264.BPHYS2.NoEventCut_EXT0.47203042";

if(submittoGrid){
       SH::scanRucio(sh,"user.abarton.mc13.424100.Pythia8B_A14_CTEQ6L1_Jpsimu4mu4.eva.1_EXT0/");
       sh.setMetaString("nc_grid_filter", "*");

}else{
  const char *inputFilePath = "/tmp/ab/WorkArea/run";
  SH::DiskListLocal list (inputFilePath);
  SH::scanSingleDir(sh, "periodA", list, "*.root*");
}


  sh.setMetaString("nc_tree", "CollectionTree");
  sh.print();

  EL::Job job;
  job.sampleHandler(sh);
  // job.options()->setDouble(EL::Job::optSkipEvents, 1);
  // job.options()->setDouble(EL::Job::optMaxEvents, 10);

  const char *output_name = "DefaultOutput";
  EL::OutputStream output(output_name);
  job.outputAdd(output);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc(output_name);
  job.algsAdd(ntuple);

  BplusTagging *alg = new BplusTagging;
//  alg->m_doMC = true;
//  alg->jpsi_container_name = "BPHY1OniaCandidates";
  job.algsAdd(alg);
  alg->output_name = output_name;
  alg->GRLFilePath ="";// "$ROOTCOREBIN/data/BplusxAODAnalysis/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good.xml";
if(submittoGrid){
    EL::PrunDriver driver;
    driver.options()->setString( "nc_outputSampleName" , "user.abarton.mc13.424100.Pythia8B_A14_CTEQ6L1_Jpsimu4mu4.eva.1_ntp.3");
    driver.options()->setString(EL::Job::optGridNFilesPerJob,  "1");

    driver.submitOnly (job, submitDir);
}else{

  EL::DirectDriver driver;
  driver.submit(job, submitDir);

}
  
  }
