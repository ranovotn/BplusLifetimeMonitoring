#ifndef BplusxAODAnalysis_BplusTagging_H
#define BplusxAODAnalysis_BplusTagging_H

#include <EventLoop/Algorithm.h>
#include <vector>
#include <string>
class TTree;
class TH1;
class GoodRunsListSelectionTool;
class BplusTagging : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;

    std::string jpsi_container_name;
    std::string bplus_container_name;
    std::string output_name;
    std::string muons_container_name;
    std::string electrons_container_name;
    std::string pvrefitted_container_name;
    std::string pv_container_name;
    std::string pv_ref_container_name;
    float VtxQualCut;
    float R_cut;
    double m_deltaR = 0.5;
    double  cut_z0 = 2.0;
//  double  cut_z0 = 5.0;
    std::string GRLFilePath;
  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!



  // this is a standard constructor
  BplusTagging ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(BplusTagging, 1);

private:



   GoodRunsListSelectionTool *m_grl; //!
   void ClearBranches();
   void InitBranches ();

   int                 m_eventCounter; //!
   int m_skip;//!
    // Histograms
    TH1 *m_h_jpsi_mass ; //!
    TH1 *m_h_jpsi_pT   ; //!
    TH1 *m_h_jpsi_rap  ; //!
    TH1 *m_h_bplus_mass; //!
    TH1 *m_h_bplus_charge ; //!
    TH1 *m_h_bplus_pT  ; //!
    TH1 *m_h_bplus_rap ; //!

    // Tree
    TTree *m_tree; //!

    // Branches
    uint32_t                   m_run_number   ; //!
    uint32_t                   m_lumi_block   ; //!
    unsigned long long         m_evt_number   ; //!
    std::vector<bool>         m_B_passed     ; //!
    std::vector<float>         m_B_mass       ; //!
    std::vector<float>         m_B_mass_err   ; //!
    std::vector<float>         m_B_charge     ; //!
    std::vector<float>         m_B_rapidity   ; //!
    std::vector<float>         m_B_pT         ; //!
    std::vector<float>         m_B_phi        ; //!
    std::vector<float>         m_B_pT_err     ; //!
    std::vector<float>         m_B_Lxy        ; //!
    std::vector<float>         m_B_Lxy_err    ; //!
    std::vector<float>         m_B_A0         ; //!
    std::vector<float>         m_B_A0_err     ; //!
    std::vector<float>         m_B_A0xy       ; //!
    std::vector<float>         m_B_A0xy_err   ; //!
    std::vector<float>         m_B_Z0         ; //!
    std::vector<float>         m_B_Z0_err     ; //!
    std::vector<float>         m_B_tau        ; //!
    std::vector<float>         m_B_tau_err    ; //!
    std::vector<int>           m_B_mu1_type   ; //!
    std::vector<float>         m_B_mu1_eta    ; //!
    std::vector<float>         m_B_mu1_pT     ; //!
    std::vector<float>         m_B_mu1_charge ; //!
    std::vector<int>           m_B_mu2_type   ; //!
    std::vector<float>         m_B_mu2_eta    ; //!
    std::vector<float>         m_B_mu2_pT     ; //!
    std::vector<float>         m_B_mu2_charge ; //!
    std::vector<float>         m_B_trk_eta    ; //!
    std::vector<float>         m_B_trk_pT     ; //!
    std::vector<float>         m_B_trk_charge ; //!
    std::vector<unsigned char> m_B_Jpsi_index ; //!
    std::vector<float>         m_B_Jpsi_mass  ; //!
    std::vector<float>         m_Jpsi_mass    ; //!
    std::vector<float>         m_Jpsi_rapidity; //!
    std::vector<float>         m_Jpsi_pT      ; //!
    std::vector<float>         m_Jpsi_chi2    ; //!
//   new vectors
    std::vector<float>         m_B_chi2       ; //!
    std::vector<float>  m_el_cone_ch_estimator; //!
    float el_cone_ch_estimator;
    std::vector<float>  m_el_cone_ch_estimator2; //!
    float el_cone_ch_estimator2;
   
    int m_n_B_candidates_loose_chi; //!
    int m_n_B_candidates_tight_chi; //!
    int m_n_Jpsi_candidates; //!
    int m_n_events_with_zero_jpsis; //!
    int m_n_muons;                  //!
    int m_n_electrons;              //!
    int m_n_el_tight;               //!
//  int m_B_mu3_type ;              //!
    std::vector<int>           m_B_mu3_type   ; //!
    std::vector<float>         m_mu3_Charge ; //!
    std::vector<float>         m_mu3_ifComb ; //!
    std::vector<float>         m_mu3_muPt   ; //!
    std::vector<float>         m_mu3_muEta  ; //!
    std::vector<float>         m_mu3_muPhi  ; //!
    std::vector<float>         m_mu3_z0V    ; //!
    std::vector<int>           m_trk_NtrkPV ; //!
    std::vector<float>         m_el_author  ; //!
    std::vector<float>         m_el_charge  ; //!
    std::vector<float>         m_el_eta     ; //!
    std::vector<float>         m_el_phi     ; //!
    std::vector<float>         m_el_pT      ; //!
    std::vector<float>         m_el_z0      ; //!
    std::vector<float>         m_el_chi2    ; //!
    std::vector<int>           m_el_ifTight;    //!
    std::vector<int>           m_el_ifMedium;   //!
    std::vector<int>           m_el_ifLoose;    //!

    std::vector<float>         m_pt_charge;  //!
    std::vector<float>         m_pt_pT;      //!
    std::vector<float>         m_pt_eta;     //!
    std::vector<float>         m_pt_phi;     //!
    std::vector<float>         m_pt_z0;      //!
//   tracks in Cone around Electron momentum
    std::vector<float>         m_tr_charge  ; //!
    std::vector<float>         m_tr_eta     ; //!
    std::vector<float>         m_tr_phi     ; //!
    std::vector<float>         m_tr_pT      ; //!
    std::vector<float>         m_tr_z0      ; //!
    std::vector<float>         m_tr_eta_b   ; //!
    std::vector<float>         m_tr_eta_e   ; //!
// TracksInCone
    std::vector<int>           n_el_cone_trk  ;      //!
    std::vector<float>         m_el_cone_trk_charge; //!
    std::vector<float>         m_el_cone_trk_eta;    //!
    std::vector<float>         m_el_cone_trk_phi;    //!
    std::vector<float>         m_el_cone_trk_pt;     //!
    std::vector<float>         m_el_cone_trk_z0;     //!


};

#endif
