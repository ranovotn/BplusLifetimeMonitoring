#ifndef BplusxAODAnalysis_BplusNtupleMaker_H
#define BplusxAODAnalysis_BplusNtupleMaker_H

#include <string>
#include <vector>
#include <memory>
#include <EventLoop/Algorithm.h>

#include <BPhysxAODTools/BranchVariable.h>
#include <BPhysxAODTools/TruthNtuple.h>


class TH1;
class TTree;
class GoodRunsListSelectionTool;
namespace Trig
  {
  class TrigDecisionTool;
  }
namespace TrigConf
  {
  class xAODConfigTool;
  }


class BplusNtupleMaker : public EL::Algorithm
  {
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
  public:
    // variables that can be set up through the job options
    std::string              jpsi_container_name;
    std::string              bplus_container_name;
    std::string              PV_container_name;
    std::string              output_name;
    bool                     m_AllCandVectorTree;
    bool                     m_AllCandSingleTree;
    bool                     m_BestChiTree;
    bool                     m_eventTriggertree;
    std::vector<std::string> grl_files;
    std::vector<std::string> triggers;
    std::string              print_trigger_regexp;
    bool                     use_grl;
    bool                     truth_only;
    int                      truth_channel; //  1 = BpJpsiKp
                                            // -1 = BmJpsiKm
                                            //  2 = BpJpsiPip
                                            // -2 = BmJpsiPim
    // cuts
    float                    cut_chi2_ndof;
    float                    cut_kaon_pT;


    // variables that don't get filled at submission time should be
    // protected from being send from the submission node to the worker
    // node (done by the //!)
  public:
  
    uint8_t GetTrackPropertyInt(const xAOD::TrackParticle *track, const xAOD::SummaryType e);
  
  
    int m_event_counter; //!
    int m_skip_grl     ; //!
    int m_skip_cuts    ; //!

    // Histograms
    TH1 *m_h_jpsi_mass ; //!
    TH1 *m_h_jpsi_pT   ; //!
    TH1 *m_h_jpsi_rap  ; //!
    TH1 *m_h_bplus_mass; //!
    TH1 *m_h_bplus_pT  ; //!
    TH1 *m_h_bplus_rap ; //!

    // Trees
    TTree *m_tree        ; //!
    TTree *m_tree_single ; //!
    TTree *m_tree_trigger; //!
    TTree *m_treeTruth; //!
    TTree *m_treeAllSingle; //!
    TTree *m_treeAllSingleTrigger; //!

    // tools
    GoodRunsListSelectionTool *m_grl               ; //!
    Trig::TrigDecisionTool    *m_trig_decision_tool; //!
    TrigConf::xAODConfigTool  *m_trig_config_tool  ; //!

    // Branches
    ScalarBranchVariable<uint32_t> m_run_number          = ScalarBranchVariable<uint32_t>("run_number"          , this); //!
    ScalarBranchVariable<uint32_t> m_lumi_block          = ScalarBranchVariable<uint32_t>("lumi_block"          , this); //!
    ScalarBranchVariable<unsigned long long> m_evt_number          = ScalarBranchVariable<unsigned long long>("evt_number"          , this); //!

    ScalarBranchVariable<uint32_t> m_nPVs          = ScalarBranchVariable<uint32_t>("nPVs"          , this); //!
    ScalarBranchVariable<uint32_t> m_nJpsi         = ScalarBranchVariable<uint32_t>("nJpsi"          , this); //!
    ScalarBranchVariable<uint32_t> m_nMuons        = ScalarBranchVariable<uint32_t>("nMuons"          , this); //!
    ScalarBranchVariable<uint32_t> m_nCands        = ScalarBranchVariable<uint32_t>("nCands"          , this); //!
    ScalarBranchVariable<float> m_MassRange       = ScalarBranchVariable<float>("massRangeofCandidates" , this); //!
    ScalarBranchVariable<float> m_LifeRange       = ScalarBranchVariable<float>("LifetimeRangeofCandidates" , this); //!
    
    ScalarBranchVariable<bool> m_pass_GRL          = ScalarBranchVariable<bool>("pass_GRL"          , this); //!    
    VectorBranchVariable<float> m_B_mass              = VectorBranchVariable<float>("B_mass"              , this); //!
    VectorBranchVariable<float> m_B_mass_err          = VectorBranchVariable<float>("B_mass_err"          , this); //!
    VectorBranchVariable<float> m_B_rapidity          = VectorBranchVariable<float>("B_rapidity"          , this); //!
    VectorBranchVariable<float> m_B_pT                = VectorBranchVariable<float>("B_pT"                , this); //!
    VectorBranchVariable<float> m_B_pT_err            = VectorBranchVariable<float>("B_pT_err"            , this); //!
    VectorBranchVariable<float> m_B_Lxy_MaxSumPt      = VectorBranchVariable<float>("B_Lxy_MaxSumPt"      , this); //!
    VectorBranchVariable<float> m_B_Lxy_MaxSumPt_err  = VectorBranchVariable<float>("B_Lxy_MaxSumPt_err"  , this); //!
    VectorBranchVariable<float> m_B_A0_MaxSumPt       = VectorBranchVariable<float>("B_A0_MaxSumPt"       , this); //!
    VectorBranchVariable<float> m_B_A0_MaxSumPt_err   = VectorBranchVariable<float>("B_A0_MaxSumPt_err"   , this); //!
    VectorBranchVariable<float> m_B_A0xy_MaxSumPt     = VectorBranchVariable<float>("B_A0xy_MaxSumPt"     , this); //!
    VectorBranchVariable<float> m_B_A0xy_MaxSumPt_err = VectorBranchVariable<float>("B_A0xy_MaxSumPt_err" , this); //!
    VectorBranchVariable<float> m_B_Z0_MaxSumPt       = VectorBranchVariable<float>("B_Z0_MaxSumPt"       , this); //!
    VectorBranchVariable<float> m_B_Z0_MaxSumPt_err   = VectorBranchVariable<float>("B_Z0_MaxSumPt_err"   , this); //!
    
    VectorBranchVariable<float> m_B_tau_MaxSumPtConstM      = VectorBranchVariable<float>("B_tau_MaxSumPtConstM"      , this); //!
    VectorBranchVariable<float> m_B_tau_MaxSumPtConstM_err  = VectorBranchVariable<float>("B_tau_MaxSumPtConstM_err"  , this); //!    
    VectorBranchVariable<float> m_B_tau_MaxSumPtInvM      = VectorBranchVariable<float>("B_tau_MaxSumPtInvM"      , this); //!
    VectorBranchVariable<float> m_B_tau_MaxSumPtInvM_err  = VectorBranchVariable<float>("B_tau_MaxSumPtInvM_err"  , this); //!    
        
    
    VectorBranchVariable<float> m_B_Lxy_MinA0         = VectorBranchVariable<float>("B_Lxy_MinA0"         , this); //!
    VectorBranchVariable<float> m_B_Lxy_MinA0_err     = VectorBranchVariable<float>("B_Lxy_MinA0_err"     , this); //!
    VectorBranchVariable<float> m_B_A0_MinA0          = VectorBranchVariable<float>("B_A0_MinA0"          , this); //!
    VectorBranchVariable<float> m_B_A0_MinA0_err      = VectorBranchVariable<float>("B_A0_MinA0_err"      , this); //!
    VectorBranchVariable<float> m_B_A0xy_MinA0        = VectorBranchVariable<float>("B_A0xy_MinA0"        , this); //!
    VectorBranchVariable<float> m_B_A0xy_MinA0_err    = VectorBranchVariable<float>("B_A0xy_MinA0_err"    , this); //!
    VectorBranchVariable<float> m_B_Z0_MinA0          = VectorBranchVariable<float>("B_Z0_MinA0"          , this); //!
    VectorBranchVariable<float> m_B_Z0_MinA0_err      = VectorBranchVariable<float>("B_Z0_MinA0_err"      , this); //!
    VectorBranchVariable<float> m_B_tau_MinA0ConstM         = VectorBranchVariable<float>("B_tau_MinA0ConstM"         , this); //!
    VectorBranchVariable<float> m_B_tau_MinA0ConstM_err     = VectorBranchVariable<float>("B_tau_MinA0ConstM_err"     , this); //!
    VectorBranchVariable<float> m_B_tau_MinA0InvM         = VectorBranchVariable<float>("B_tau_MinA0InvM"         , this); //!
    VectorBranchVariable<float> m_B_tau_MinA0InvM_err     = VectorBranchVariable<float>("B_tau_MinA0InvM_err"     , this); //!            

    VectorBranchVariable<float> m_B_chi2_ndof         = VectorBranchVariable<float>("B_chi2_ndof"         , this); //!
    VectorBranchVariable<float> m_B_mu1_eta           = VectorBranchVariable<float>("B_mu1_eta"           , this); //!
    VectorBranchVariable<float> m_B_mu1_pT            = VectorBranchVariable<float>("B_mu1_pT"            , this); //!
    VectorBranchVariable<float> m_B_mu1_phi           = VectorBranchVariable<float>("B_mu1_phi"          , this); //!

    
    VectorBranchVariable<float> m_B_mu1_charge        = VectorBranchVariable<float>("B_mu1_charge"        , this); //!
    VectorBranchVariable<float> m_B_mu2_eta           = VectorBranchVariable<float>("B_mu2_eta"           , this); //!
    VectorBranchVariable<float> m_B_mu2_pT            = VectorBranchVariable<float>("B_mu2_pT"            , this); //!
    VectorBranchVariable<float> m_B_mu2_phi            = VectorBranchVariable<float>("B_mu2_phi"          , this); //!
    

    VectorBranchVariable<int32_t> m_B_mu1_nSCT            = VectorBranchVariable<int32_t>("B_mu1_nSCT"        , this); //!
    VectorBranchVariable<int32_t> m_B_mu2_nSCT            = VectorBranchVariable<int32_t>("B_mu2_nSCT"        , this); //!
    VectorBranchVariable<int32_t> m_B_h1_nSCT             = VectorBranchVariable<int32_t>("B_trk_nSCT"        , this); //!

    VectorBranchVariable<int32_t> m_B_mu1_nSCThole        = VectorBranchVariable<int32_t>("B_mu1_nSCThole"     , this); //!
    VectorBranchVariable<int32_t> m_B_mu2_nSCThole        = VectorBranchVariable<int32_t>("B_mu2_nSCThole"     , this); //!
    VectorBranchVariable<int32_t> m_B_h1_nSCThole         = VectorBranchVariable<int32_t>("B_trk_nSCThole"     , this); //!

    
    VectorBranchVariable<int32_t> m_B_mu1_npix            = VectorBranchVariable<int32_t>("B_mu1_npix"         , this); //!
    VectorBranchVariable<int32_t> m_B_mu2_npix            = VectorBranchVariable<int32_t>("B_mu2_npix"         , this); //!
    VectorBranchVariable<int32_t> m_B_h1_npix             = VectorBranchVariable<int32_t>("B_trk_npix"         , this); //!

    VectorBranchVariable<int32_t> m_B_mu1_npixhole        = VectorBranchVariable<int32_t>("B_mu1_npixhole"     , this); //!
    VectorBranchVariable<int32_t> m_B_mu2_npixhole        = VectorBranchVariable<int32_t>("B_mu2_npixhole"     , this); //!
    VectorBranchVariable<int32_t> m_B_h1_npixhole         = VectorBranchVariable<int32_t>("B_trk_npixhole"     , this); //!

    VectorBranchVariable<int32_t> m_B_mu1_nSCTsh            = VectorBranchVariable<int32_t>("B_mu1_nSCTsh"            , this); //!
    VectorBranchVariable<int32_t> m_B_mu2_nSCTsh            = VectorBranchVariable<int32_t>("B_mu2_nSCTsh"            , this); //!
    VectorBranchVariable<int32_t> m_B_h1_nSCTsh             = VectorBranchVariable<int32_t>("B_trk_nSCTsh"            , this); //!
    
    VectorBranchVariable<int32_t> m_B_mu1_npixsh            = VectorBranchVariable<int32_t>("B_mu1_npixsh"            , this); //!
    VectorBranchVariable<int32_t> m_B_mu2_npixsh            = VectorBranchVariable<int32_t>("B_mu2_npixsh"            , this); //!
    VectorBranchVariable<int32_t> m_B_h1_npixsh             = VectorBranchVariable<int32_t>("B_trk_npixsh"            , this); //!


    VectorBranchVariable<float> m_B_mu2_charge        = VectorBranchVariable<float>("B_mu2_charge"        , this); //!
    VectorBranchVariable<float> m_B_trk_eta           = VectorBranchVariable<float>("B_trk_eta"           , this); //!
    VectorBranchVariable<float> m_B_trk_pT            = VectorBranchVariable<float>("B_trk_pT"            , this); //!
    VectorBranchVariable<float> m_B_trk_phi           = VectorBranchVariable<float>("B_trk_phi"          , this); //!
    VectorBranchVariable<float> m_B_trk_charge        = VectorBranchVariable<float>("B_trk_charge"        , this); //!


    VectorBranchVariable<int32_t> m_B_nIBL        = VectorBranchVariable<int32_t>("B_nIBL"        , this); //!    
    
    VectorBranchVariable<unsigned char     > m_B_Jpsi_index        = VectorBranchVariable<unsigned char     >("B_Jpsi_index"        , this); //!
    VectorBranchVariable<float> m_B_Jpsi_mass         = VectorBranchVariable<float>("B_Jpsi_mass"         , this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_mass     = VectorBranchVariableMapped<float, unsigned char>("Jpsi_mass"    , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!

    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_MaxSumPtConstM_tau  = VectorBranchVariableMapped<float, unsigned char>("Jpsi_MaxSumPtConstM_tau"    , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!    
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_MaxSumPt_ConstM_tauErr  = VectorBranchVariableMapped<float, unsigned char>("Jpsi_MaxSumPtConstM_tau_err"    , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!   
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_MaxSumPtInvM_tau  = VectorBranchVariableMapped<float, unsigned char>("Jpsi_MaxSumPtInvM_tau"    , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!    
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_MaxSumPtInvM_tauErr  = VectorBranchVariableMapped<float, unsigned char>("Jpsi_MaxSumPtInvM_tau_err"    , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!   

    
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_rapidity = VectorBranchVariableMapped<float, unsigned char>("Jpsi_rapidity", dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_pT       = VectorBranchVariableMapped<float, unsigned char>("Jpsi_pT"      , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_chi2     = VectorBranchVariableMapped<float, unsigned char>("Jpsi_chi2"    , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_mu1_eta  = VectorBranchVariableMapped<float, unsigned char>("Jpsi_mu1_eta" , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_mu1_pT   = VectorBranchVariableMapped<float, unsigned char>("Jpsi_mu1_pT"  , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_mu2_eta  = VectorBranchVariableMapped<float, unsigned char>("Jpsi_mu2_eta" , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_mu2_pT   = VectorBranchVariableMapped<float, unsigned char>("Jpsi_mu2_pT"  , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariable<int  > m_MaxSumPt_orig_ntrk  = VectorBranchVariable<int  >("MaxSumPt_orig_ntrk"  , this); //!
    VectorBranchVariable<int  > m_MaxSumPt_ref_ntrk   = VectorBranchVariable<int  >("MaxSumPt_ref_ntrk"   , this); //!
    VectorBranchVariable<int  > m_MaxSumPt_ref_stat   = VectorBranchVariable<int  >("MaxSumPt_ref_stat"   , this); //!
    VectorBranchVariable<int  > m_MinA0_orig_ntrk     = VectorBranchVariable<int  >("MinA0_orig_ntrk"     , this); //!
    VectorBranchVariable<int  > m_MinA0_ref_ntrk      = VectorBranchVariable<int  >("MinA0_ref_ntrk"      , this); //!
    VectorBranchVariable<int  > m_MinA0_ref_stat      = VectorBranchVariable<int  >("MinA0_ref_stat"      , this); //!
//    VectorBranchVariable<int  > m_MinZ0_orig_ntrk     = VectorBranchVariable<int  >("MinZ0_orig_ntrk"     , this); //!
//    VectorBranchVariable<int  > m_MinZ0_ref_ntrk      = VectorBranchVariable<int  >("MinZ0_ref_ntrk"      , this); //!
//    VectorBranchVariable<int  > m_MinZ0_ref_stat      = VectorBranchVariable<int  >("MinZ0_ref_stat"      , this); //!
    VectorBranchVariable<int  > m_MaxSumPt_vertextype = VectorBranchVariable<int  >("MaxSumPt_vertextype" , this); //!
    VectorBranchVariable<int  > m_MinA0_vertextype    = VectorBranchVariable<int  >("MinA0_vertextype"    , this); //!
//    VectorBranchVariable<int  > m_MinZ0_vertextype    = VectorBranchVariable<int  >("MinZ0_vertextype"    , this); //!
   
   VectorBranchVariable<bool  > m_exclusiveTrueBplus    = VectorBranchVariable<bool  >("exclusiveTrueBplus"    , this); //!
   VectorBranchVariable<bool  > m_inclusiveTrueBplus    = VectorBranchVariable<bool  >("inclusiveTrueBplus"    , this); //!   
   VectorBranchVariable<float  > m_TrueLifetime    = VectorBranchVariable<float  >("Bplus_TrueLifetime"    , this); //!   
   
    std::vector<ScalarBranchVariable<bool> *> m_trigger_branches;

    int m_n_B_candidates_loose_chi; //!
    int m_n_B_candidates_tight_chi; //!
    int m_n_Jpsi_candidates; //!
    int m_n_events_with_zero_jpsis; //!


    std::array<int, 5> extraChildren;//!

    // this is a standard constructor
    BplusNtupleMaker();
    std::unique_ptr<xAOD::TruthNtupleNode> BplusDecayFinder;//!
    // these are the functions inherited from Algorithm
    virtual EL::StatusCode setupJob(EL::Job& job);
    virtual EL::StatusCode fileExecute();
    virtual EL::StatusCode histInitialize();
    virtual EL::StatusCode changeInput(bool firstFile);
    virtual EL::StatusCode initialize();
    virtual EL::StatusCode execute();
    virtual EL::StatusCode postExecute();
    virtual EL::StatusCode finalize();
    virtual EL::StatusCode histFinalize();

    // 
    void InitBranches();
    void ClearBranches();
    int TrueDecaysFound;//!
    int TrueDecaysFoundOver1;//!    
    int FullReconFound;//!
    // this is needed to distribute the algorithm to the workers
    ClassDef(BplusNtupleMaker, 1);
  };

#endif
