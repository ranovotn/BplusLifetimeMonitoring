#ifndef BplusxAODAnalysis_BplusTruthToNtuple_H
#define BplusxAODAnalysis_BplusTruthToNtuple_H

#include <EventLoop/Algorithm.h>
#include <memory>
#include <BPhysxAODTools/TruthNtuple.h>
#include "TTree.h"

class BplusTruthToNtuple : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;

  std::string output_name;

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  int m_event_counter;//!
  TTree *m_tree; //!
  // TH1 *myHist; //!
    uint32_t m_run_number;//!
    uint32_t m_lumi_block;//!
    unsigned long long m_evt_number;//!

  std::unique_ptr<xAOD::TruthNtupleNode> BplusDecayFinder;//!
    int TrueDecaysFound;//!
    int TrueDecaysFoundOver1;//!    
  // this is a standard constructor
  BplusTruthToNtuple ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(BplusTruthToNtuple, 1);
};

#endif
