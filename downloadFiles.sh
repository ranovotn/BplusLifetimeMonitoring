#!/bin/bash

dwnloadFolder="./data/"
resubmit=""
localPath=$(pwd)

cd "$dwnloadFolder"

for var in "$@"
do
#	rucio download "user.ranovotn.data18_BPHY5.Bplus_run"$var"_"$resubmit"_DefaultOutput.root"
	rucio download "user.ranovotn.data18_BPHY5.Bplus_run"$var"_DefaultOutput.root"
done

echo "INFO: Download completed!"

rm -fv "data2018.root"
hadd "data2018.root" */*.root

echo "INFO: Merge completed!"

cd "-"
