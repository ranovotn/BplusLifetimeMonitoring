// $Id: $

// Gaudi/Athena include(s):
#include "GaudiKernel/DeclareFactoryEntries.h"

// Local include(s):
#include "xAODBPhys/BPhysMetaDataTool.h"
#include "xAODBPhys/BPhysTrackVertexMapTool.h"

DECLARE_NAMESPACE_TOOL_FACTORY( xAOD, BPhysMetaDataTool )
DECLARE_NAMESPACE_TOOL_FACTORY( xAOD, BPhysTrackVertexMapTool )

DECLARE_FACTORY_ENTRIES( xAODBPhys ) {

  DECLARE_NAMESPACE_TOOL( xAOD, BPhysMetaDataTool )
  DECLARE_NAMESPACE_TOOL( xAOD, BPhysTrackVertexMapTool )

}
