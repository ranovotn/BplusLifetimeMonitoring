// $Id: $

// ROOT include(s):
#include "TString.h"

// Local include(s):
#include "xAODBPhys/BPhysMetaDataTool.h"

namespace xAOD {
  
  BPhysMetaDataTool::BPhysMetaDataTool( const std::string& name )
    : asg::AsgMetadataTool( name ),
      m_md(), m_mdAux(),
      m_inputMd(NULL),
      m_beginFileIncidentSeen( false ) {

    // Note: Keys need to explicitely be set!
    declareProperty( "InputKey"   , m_inputKey  = "_NONE_" );
    declareProperty( "OutputKey"  , m_outputKey = "_NONE_" );
    declareProperty( "CopyInput"  , m_copyInput = false    );
    // The prefix would typically be the derivation format name
    declareProperty( "Prefix"     , m_prefix = ""          );
    
#ifdef ASGTOOL_ATHENA
    declareInterface< ::IMetaDataTool >( this );
#endif // ASGTOOL_ATHENA
  }
  
  StatusCode BPhysMetaDataTool::initialize() {
    
    // Greet the user:
    ATH_MSG_DEBUG( "Initialising xAOD::BPhysMetaDataTool" );
    ATH_MSG_DEBUG( "  InputKey     = " << m_inputKey );
    ATH_MSG_DEBUG( "  OutputKey    = " << m_outputKey );
    ATH_MSG_DEBUG( "  CopyInput    = " << (m_copyInput ? "True" : "False") );
    ATH_MSG_DEBUG( "  Prefix       = " << m_prefix );
    
    // Reset the member variable(s):
    m_md.reset();
    m_mdAux.reset();
    m_beginFileIncidentSeen = false;
    
    // Return gracefully:
    return StatusCode::SUCCESS;
  }
  
  StatusCode BPhysMetaDataTool::beginInputFile() {
    
    ATH_MSG_DEBUG( "xAOD::BPhysMetaDataTool::beginInputFile()" );

    // Whatever happens, we've seen the incident:
    m_beginFileIncidentSeen = true;

    // make sure output objects exist
    createOutputObjects();

    ATH_MSG_DEBUG("xAOD::BPhysMetaDataTool::beginInputFile(): reading tests");
    if ( m_inputKey != "_NONE_" ) {
      if ( inputMetaStore()->contains< xAOD::BPhysMetaData >( m_inputKey ) ) {
	// Retrieve the input object:
	m_inputMd = NULL;
	ATH_CHECK( inputMetaStore()->retrieve( m_inputMd, m_inputKey ) );

	// copy input to output if requested
	if ( m_copyInput ) {
	  *( m_md.get() ) = *m_inputMd;
	}
      } else {
	ATH_MSG_WARNING("Can not find input metadata key: "
			<< m_inputKey);
      }
    }
    // Return gracefully:
    return StatusCode::SUCCESS;
  }
  
  StatusCode BPhysMetaDataTool::beginEvent() {
    
    // In case we missed the BeginInputFile incident for the first input file,
    // make sure that we still run the appropriate function.
    if( ! m_beginFileIncidentSeen ) {
      ATH_CHECK( beginInputFile() );
    }

    // Return gracefully:
    return StatusCode::SUCCESS;
  }
  
  StatusCode BPhysMetaDataTool::metaDataStop() {
    
    ATH_MSG_DEBUG( "xAOD::BPhysMetaDataTool::metaDataStop()" );
    
    // Don't be offended if the metadata already exists in the output:
    if( outputMetaStore()->contains< xAOD::BPhysMetaData >( m_outputKey ) ) {
      ATH_MSG_DEBUG( "xAOD::BPhysMetaData already in the output: "
		     << m_outputKey);
      return StatusCode::SUCCESS;
    }
   
    // Record the metadata if requested via output key
    if ( m_outputKey != "_NONE_" ) { 
      if ( m_md.get() && m_mdAux.get() ) {
	ATH_MSG_DEBUG( "Recording BPhys metadata" );
	ATH_CHECK( outputMetaStore()->record( m_md.release(), m_outputKey ) );
	ATH_CHECK( outputMetaStore()->record( m_mdAux.release(),
					      m_outputKey + "Aux." ) );
      }
    }
    // Return gracefully:
    return StatusCode::SUCCESS;
  }

  // pointer to input metadata object
  const xAOD::BPhysMetaData* BPhysMetaDataTool::getInputMetaData() {

    return m_inputMd;
  }

  // pointer to output metadata object
  xAOD::BPhysMetaData* BPhysMetaDataTool::getOutputMetaData() {

    createOutputObjects();
    return m_md.get();
  }

  // create output objects
  void BPhysMetaDataTool::createOutputObjects() {

    if ( ! m_md.get() && ! m_mdAux.get() ) {
      ATH_MSG_DEBUG( "Creating output objects" );
      m_md.reset( new xAOD::BPhysMetaData() );
      m_mdAux.reset( new xAOD::BPhysMetaDataAuxInfo() );
      m_md->setStore( m_mdAux.get() );
      m_md->setPrefix(m_prefix);
    }
  }

  // dump metadata contents (and optionally the full type information) to string
  TString BPhysMetaDataTool::metaDataToString(bool withTypes, bool forOutput) {

    std::string mdKey = ( forOutput ? m_outputKey : m_inputKey );
    const BPhysMetaData* md = ( forOutput ? getOutputMetaData()
				: getInputMetaData() );
    TString str;

    if ( md != NULL ) {
      str += Form("Summary for MetaStore %s\n", mdKey.c_str());
      str += "----------------------------------------------------\n\n";
      str += md->toString();
      if ( withTypes ) {
	str += "\n";
	str += md->varTypesToString();
      }
    } else {
      ATH_MSG_WARNING("Can not read metadata for key: " << mdKey);
    }
    return str;
  }
  
} // namespace xAOD
