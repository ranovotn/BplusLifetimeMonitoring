// $Id: $

// System include(s):
#include <iostream>
//#include <array>
#include <cmath>
#include <cstdlib>

// Core include(s):
#include "AthContainers/AuxTypeRegistry.h"
#include "AthContainers/normalizedTypeinfoName.h"

// Local include(s):
#include "xAODBPhys/versions/BPhysMetaData_v1.h"

namespace xAOD {

  /// Design inspired by FileMetaData_v1.cxx
  ///
  /// @param Wolfgang Walkowiak <Wolfgang.Walkowiak@cern.ch>
  ///
  /// $Revision: $
  /// $Date: $

   BPhysMetaData_v1::BPhysMetaData_v1()
     : SG::AuxElement(), m_prefix("") {

   }

  void BPhysMetaData_v1::setPrefix(std::string prefix) {

    m_prefix = prefix;
  }
  
   bool BPhysMetaData_v1::operator==( const BPhysMetaData_v1& rhs ) {

      // Get the variable types from both objects:
      const SG::auxid_set_t& auxids1 = this->getAuxIDs();
      const SG::auxid_set_t& auxids2 = rhs.getAuxIDs();

      // They need to be the same. If the two objects have different variables,
      // that's bad. Unfortunately there's no equivalency operator for
      // auxid_set_t, so this check needs to be spelled out. :-(
      if( auxids1.size() != auxids2.size() ) {
         return false;
      }
      for( SG::auxid_t auxid : auxids1 ) {
         if( auxids2.find( auxid ) == auxids2.end() ) {
            return false;
         }
      }

      // Now, compare all elements:
      for( SG::auxid_t auxid : auxids1 ) {

         // Check the type of the variable:
         SG::AuxTypeRegistry& reg = SG::AuxTypeRegistry::instance();
         const std::type_info* ti = reg.getType( auxid );
         if ( ! ti ) {
            // This is weird, but there's not much that we can do about it
            // here...
            continue;
         }
         if ( ( *ti != typeid( std::string ) ) &&
	      ( *ti != typeid( int ) ) &&
	      ( *ti != typeid( float ) ) &&
	      ( *ti != typeid( double ) ) &&
	      ( *ti != typeid( bool ) ) &&
	      ( *ti != typeid( std::vector<std::string> ) ) &&
	      ( *ti != typeid( std::vector<int> ) ) &&
	      ( *ti != typeid( std::vector<float> ) ) &&
	      ( *ti != typeid( std::vector<double> ) ) &&
	      ( *ti != typeid( std::vector<bool> ) ) ) {
	   // We just ignore every other type. Still, this is strange, let's
	   // warn the user about it.
	   std::cerr << "xAOD::BPhysMetaData::operator==  WARNING  Unsupported "
		     << "variable (\"" << reg.getName( auxid ) << "\"/"
		     << SG::normalizedTypeinfoName( *ti )
		     << ") encountered" << std::endl;
	   continue;
         }

         // The variable name:
         const std::string name = reg.getName( auxid );

         // Treat the string and float options separately:
         if ( *ti == typeid( std::string ) ) {

            // Retrieve the values:
            const std::string& value1 = this->auxdata< std::string >( name );
            const std::string& value2 = rhs.auxdata< std::string >( name );
            // And simply compare them:
            if ( value1 != value2 ) {
               return false;
            }

         } else if ( *ti == typeid( float ) ) {

            // Retrieve the values:
            const float& value1 = this->auxdata< float >( name );
            const float& value2 = rhs.auxdata< float >( name );
            // And (not so simply) compare them:
            if ( std::abs( value1 - value2 ) > 0.001 ) {
               return false;
            }

         } else if ( *ti == typeid( int ) ) {

            // Retrieve the values:
            const int& value1 = this->auxdata< int >( name );
            const int& value2 = rhs.auxdata< int >( name );
            // And simply compare them:
            if ( value1 != value2 ) {
               return false;
            }

         } else if ( *ti == typeid( double ) ) {

            // Retrieve the values:
            const double& value1 = this->auxdata< double >( name );
            const double& value2 = rhs.auxdata< double >( name );
            // And (not so simply) compare them:
            if ( std::abs( value1 - value2 ) > 0.001 ) {
               return false;
            }

         } else if ( *ti == typeid( bool ) ) {

            // Retrieve the values:
            const bool& value1 = this->auxdata< bool >( name );
            const bool& value2 = rhs.auxdata< bool >( name );
            // And simply compare them:
            if ( value1 != value2 ) {
               return false;
            }

         } else if ( *ti == typeid( std::vector<int> ) ) {

	   // Retrieve the vectors:
	   const std::vector<int>& vec1 =
	     this->auxdata< std::vector<int> >( name );
	   const std::vector<int>& vec2 =
	     rhs.auxdata< std::vector<int> >( name );
	   // And simply compare them:
	   if ( vec1.size() != vec2.size() ) {
	     return false;
	   } else {
	     for (unsigned int i=0; i < vec1.size(); ++i) {
	       if ( vec1[i] != vec2[i] ) {
		 return false;
	       }
	     }
	   }
	   
         } else if ( *ti == typeid( std::vector<float> ) ) {

	   // Retrieve the vectors:
	   const std::vector<float>& vec1 =
	     this->auxdata< std::vector<float> >( name );
	   const std::vector<float>& vec2 =
	     rhs.auxdata< std::vector<float> >( name );
	   // And (not so simply) compare them:
	   if ( vec1.size() != vec2.size() ) {
	     return false;
	   } else {
	     for (unsigned int i=0; i < vec1.size(); ++i) {
	       if ( std::abs( vec1[i] - vec2[i] ) > 0.001 ) {
		 return false;
	       }
	     }
	   }
	   
         } else if ( *ti == typeid( std::vector<double> ) ) {

	   // Retrieve the vectors:
	   const std::vector<double>& vec1 =
	     this->auxdata< std::vector<double> >( name );
	   const std::vector<double>& vec2 =
	     rhs.auxdata< std::vector<double> >( name );
	   // And (not so simply) compare them:
	   if ( vec1.size() != vec2.size() ) {
	     return false;
	   } else {
	     for (unsigned int i=0; i < vec1.size(); ++i) {
	       if ( std::abs( vec1[i] - vec2[i] ) > 0.001 ) {
		 return false;
	       }
	     }
	   }
	   
         } else if ( *ti == typeid( std::vector<bool> ) ) {

	   // Retrieve the vectors:
	   const std::vector<bool>& vec1 =
	     this->auxdata< std::vector<bool> >( name );
	   const std::vector<bool>& vec2 =
	     rhs.auxdata< std::vector<bool> >( name );
	   // And simply compare them:
	   if ( vec1.size() != vec2.size() ) {
	     return false;
	   } else {
	     for (unsigned int i=0; i < vec1.size(); ++i) {
	       if ( vec1[i] != vec2[i] ) {
		 return false;
	       }
	     }
	   }
	   
         } else if ( *ti == typeid( std::vector<std::string> ) ) {

	   // Retrieve the vectors:
	   const std::vector<std::string>& vec1 =
	     this->auxdata< std::vector<std::string> >( name );
	   const std::vector<std::string>& vec2 =
	     rhs.auxdata< std::vector<std::string> >( name );
	   // And simply compare them:
	   if ( vec1.size() != vec2.size() ) {
	     return false;
	   } else {
	     for (unsigned int i=0; i < vec1.size(); ++i) {
	       if ( vec1[i] != vec2[i] ) {
		 return false;
	       }
	     }
	   }
	   
         } else {
            // We should really never end up here unless a coding mistake was
            // made upstream.
            std::abort();
         }
      }

      // The two objects were found to be equivalent:
      return true;
   }

   bool BPhysMetaData_v1::operator!=( const BPhysMetaData_v1& rhs ) {

      return !( this->operator==( rhs ) );
   }

  /// metadata variable types
  std::map<std::string, const std::type_info*> BPhysMetaData_v1::varTypes()
    const {

    std::map<std::string, const std::type_info*> tmap;

    // Get the variable types
    const SG::auxid_set_t& auxids = this->getAuxIDs();
    for ( SG::auxid_t auxid : auxids ) {
      SG::AuxTypeRegistry& reg = SG::AuxTypeRegistry::instance();
      tmap[reg.getName(auxid)] = reg.getType( auxid );
    }
    return tmap;
  }

  /// metadata variable types list as string
  TString BPhysMetaData_v1::varTypesToString(TString header) const {

    TString str = header;
    std::map<std::string, const std::type_info*> tmap = varTypes();
    for (auto &ent : tmap) {
      str += Form("%-30s : %s\n", ent.first.c_str(),
		  SG::normalizedTypeinfoName( *ent.second ).c_str());
    }
    return str;
  }
  
/// Getter methods for maps for different metadata types 
#define GET_VALUES_IMP( TYPE )                                    \
    std::map<std::string, TYPE > metaMap;                         \
    const SG::auxid_set_t& auxids = this->getAuxIDs();            \
    for ( SG::auxid_t auxid : auxids ) {                          \
      SG::AuxTypeRegistry& reg = SG::AuxTypeRegistry::instance(); \
      const std::type_info* ti = reg.getType( auxid );            \
      if ( ti != NULL && *ti == typeid( TYPE ) ) {                \
	const std::string name = reg.getName( auxid );            \
	const TYPE & value = this->auxdata< TYPE >( name );       \
	metaMap[name] = value;                                    \
      }                                                           \
    }                                                             \
    return metaMap;
  
  std::map<std::string, int> BPhysMetaData_v1::valuesI() const {

    GET_VALUES_IMP( int )
  }

  std::map<std::string, float> BPhysMetaData_v1::valuesF() const {

    GET_VALUES_IMP( float )
  }

  std::map<std::string, double> BPhysMetaData_v1::valuesD() const {

    GET_VALUES_IMP( double )
  }

  std::map<std::string, bool> BPhysMetaData_v1::valuesB() const {

    GET_VALUES_IMP( bool )
  }

  std::map<std::string, std::string> BPhysMetaData_v1::valuesS() const {

    GET_VALUES_IMP( std::string )
  }

  std::map<std::string, std::vector<int> > BPhysMetaData_v1::valuesVI()
    const {

    GET_VALUES_IMP( std::vector<int> )
  }

  std::map<std::string, std::vector<float> > BPhysMetaData_v1::valuesVF()
    const {

    GET_VALUES_IMP( std::vector<float> )
  }

  std::map<std::string, std::vector<double> > BPhysMetaData_v1::valuesVD()
    const {

    GET_VALUES_IMP( std::vector<double> )
  }

  std::map<std::string, std::vector<bool> > BPhysMetaData_v1::valuesVB()
    const {

    GET_VALUES_IMP( std::vector<bool> )
  }

  std::map<std::string, std::vector<std::string> > BPhysMetaData_v1::valuesVS()
    const {

    GET_VALUES_IMP( std::vector<std::string> )
  }


#undef GET_VALUES_IMP

/// Setter methods for maps for different metadata types 
#define SET_VALUES_IMP( TYPE )                                              \
  bool BPhysMetaData_v1::setValues(const std::map<std::string,              \
						 TYPE >& metaMap) {	    \
    for (auto const &ent : metaMap) {                                       \
      Accessor< TYPE > acc( m_prefix + ent.first );				    \
      acc( *this ) = ent.second;	                                    \
    }                                                                       \
    return true;                                                            \
  }								            \

  SET_VALUES_IMP( int         )
  SET_VALUES_IMP( float       )
  SET_VALUES_IMP( double      )
  SET_VALUES_IMP( bool        )
  SET_VALUES_IMP( std::string )
  SET_VALUES_IMP( std::vector<int> )
  SET_VALUES_IMP( std::vector<float> )
  SET_VALUES_IMP( std::vector<double> )
  SET_VALUES_IMP( std::vector<bool> )
  SET_VALUES_IMP( std::vector<std::string> )

#undef SET_VALUES_IMP

/// Getter methods for different metadata types
#define GET_VALUE_IMP( TYPE )             \
      Accessor< TYPE > acc( m_prefix + name );       \
      if( ! acc.isAvailable( *this ) ) {  \
         return false;                    \
      }                                   \
      val = acc( *this );                 \
      return true;


  bool BPhysMetaData_v1::value( const std::string& name,
				int& val ) const {
    GET_VALUE_IMP( int )
  }

  bool BPhysMetaData_v1::value( const std::string& name,
				float& val ) const {
    GET_VALUE_IMP( float )
  }

  bool BPhysMetaData_v1::value( const std::string& name,
				double& val ) const {
    GET_VALUE_IMP( double )
  }

  bool BPhysMetaData_v1::value( const std::string& name,
				bool& val ) const {
    GET_VALUE_IMP( bool )
  }

  bool BPhysMetaData_v1::value( const std::string& name,
				std::string& val ) const {
    GET_VALUE_IMP( std::string )
  }

  bool BPhysMetaData_v1::value( const std::string& name,
				std::vector<int>& val ) const {
    GET_VALUE_IMP( std::vector<int> )
  }

  bool BPhysMetaData_v1::value( const std::string& name,
				std::vector<float>& val ) const {
    GET_VALUE_IMP( std::vector<float> )
  }

  bool BPhysMetaData_v1::value( const std::string& name,
				std::vector<double>& val ) const {
    GET_VALUE_IMP( std::vector<double> )
  }

  bool BPhysMetaData_v1::value( const std::string& name,
				std::vector<bool>& val ) const {
    GET_VALUE_IMP( std::vector<bool> )
  }

  bool BPhysMetaData_v1::value( const std::string& name,
				std::vector<std::string>& val ) const {
    GET_VALUE_IMP( std::vector<std::string> )
  }

#undef GET_VALUE_IMP

/// Setter methods for different metadata types
#define SET_VALUE_IMP( TYPE )	      \
     Accessor< TYPE > acc( m_prefix + name );    \
     acc( *this ) = val;              \
     return true;

  bool BPhysMetaData_v1::setValue( const std::string& name,
				   const std::string& val ) {
    SET_VALUE_IMP( std::string )
  }

  bool BPhysMetaData_v1::setValue( const std::string& name, int val ) {
    SET_VALUE_IMP( int )
  }

  bool BPhysMetaData_v1::setValue( const std::string& name, float val ) {
    SET_VALUE_IMP( float )
  }

  bool BPhysMetaData_v1::setValue( const std::string& name, double val ) {
    SET_VALUE_IMP( double )
  }

  bool BPhysMetaData_v1::setValue( const std::string& name, bool val ) {
    SET_VALUE_IMP( bool )
  }

  bool BPhysMetaData_v1::setValue( const std::string& name,
				   std::vector<int>& val ) {
    SET_VALUE_IMP( std::vector<int> )
  }
  
  bool BPhysMetaData_v1::setValue( const std::string& name,
				   std::vector<float>& val ) {
    SET_VALUE_IMP( std::vector<float> )
  }
  
  bool BPhysMetaData_v1::setValue( const std::string& name,
				   std::vector<double>& val ) {
    SET_VALUE_IMP( std::vector<double> )
  }
  
  bool BPhysMetaData_v1::setValue( const std::string& name,
				   std::vector<bool>& val ) {
    SET_VALUE_IMP( std::vector<bool> )
  }
  
  bool BPhysMetaData_v1::setValue( const std::string& name,
				   std::vector<std::string>& val ) {
    SET_VALUE_IMP( std::vector<std::string> )
  }
  
#undef SET_VALUE_IMP

  TString BPhysMetaData_v1::toString(TString header) const {
  
    TString str = header;
    std::map<std::string, std::string> ms = valuesS();
    for ( auto it = ms.begin(); it != ms.end(); ++it ) {
      str += Form("%-30s :S : %s\n", it->first.c_str(), it->second.c_str());
    }
    std::map<std::string, int> mi = valuesI();
    for ( auto it = mi.begin(); it != mi.end(); ++it ) {
      str += Form("%-30s :I : %d\n", it->first.c_str(), it->second);
    }
    std::map<std::string, float> mf = valuesF();
    for ( auto it = mf.begin(); it != mf.end(); ++it ) {
      str += Form("%-30s :F : %f\n", it->first.c_str(), it->second);
    }
    std::map<std::string, double> md = valuesD();
    for ( auto it = md.begin(); it != md.end(); ++it ) {
      str += Form("%-30s :D : %f\n", it->first.c_str(), it->second);
    }
    std::map<std::string, bool> mb = valuesB();
    for ( auto it = mb.begin(); it != mb.end(); ++it ) {
      str += Form("%-30s :B : %s\n", it->first.c_str(),
                                  (it->second ? "True" : "False") );
    }
    std::map<std::string, std::vector<int> > mvi = valuesVI();
    for ( auto it = mvi.begin(); it != mvi.end(); ++it ) {
      TString strv;
      for (auto &ent : it->second) {
	strv += Form("%d,", ent);
      }
      strv.Remove(TString::kTrailing, ',');
      str += Form("%-30s :VI: [%s]\n", it->first.c_str(), strv.Data());
    }
    std::map<std::string, std::vector<float> > mvf = valuesVF();
    for ( auto it = mvf.begin(); it != mvf.end(); ++it ) {
      TString strv;
      for (auto &ent : it->second) {
	strv += Form("%f,", ent);
      }
      strv.Remove(TString::kTrailing, ',');
      str += Form("%-30s :VF: [%s]\n", it->first.c_str(), strv.Data());
    }
    std::map<std::string, std::vector<double> > mvd = valuesVD();
    for ( auto it = mvd.begin(); it != mvd.end(); ++it ) {
      TString strv;
      for (auto &ent : it->second) {
	strv += Form("%f,", ent);
      }
      strv.Remove(TString::kTrailing, ',');
      str += Form("%-30s :VD: [%s]\n", it->first.c_str(), strv.Data());
    }
    std::map<std::string, std::vector<bool> > mvb = valuesVB();
    for ( auto it = mvb.begin(); it != mvb.end(); ++it ) {
      TString strv;
      // vector<bool> needs special treatment
      for (auto &&ent : it->second) {
	strv += Form("%s,", ent ? "True" : "False");
      }
      strv.Remove(TString::kTrailing, ',');
      str += Form("%-30s :VB: [%s]\n", it->first.c_str(), strv.Data());
    }
    std::map<std::string, std::vector<std::string> > mvs = valuesVS();
    for ( auto it = mvs.begin(); it != mvs.end(); ++it ) {
      TString strv;
      for (auto &ent : it->second) {
	strv += Form("%s,", ent.c_str());;
      }
      strv.Remove(TString::kTrailing, ',');
      str += Form("%-30s :VS: [%s]\n", it->first.c_str(), strv.Data());
    }

    return str;
  }

} // namespace xAOD
