// $Id: $

// Local include(s):
#include "xAODBPhys/versions/BPhysMetaDataAuxInfo_v1.h"

namespace xAOD {

   BPhysMetaDataAuxInfo_v1::BPhysMetaDataAuxInfo_v1()
      : AuxInfoBase() {

     // Declare the auxiliary variables of the class:
     // AUX_VARIABLE( myvariable );
   }

} // namespace xAOD
