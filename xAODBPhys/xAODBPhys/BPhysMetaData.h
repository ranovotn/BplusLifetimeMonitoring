// Dear emacs, this is -*- c++ -*-
// $Id: $
#ifndef XAODBPHYS_BPHYSMETADATA_H
#define XAODBPHYS_BPHYSMETADATA_H

// Local include(s):
#include "xAODBPhys/versions/BPhysMetaData_v1.h"

namespace xAOD {
  /// Design inspired by FileMetaData.h.
  ///
  /// @param Wolfgang Walkowiak <Wolfgang.Walkowiak@cern.ch>
  ///
  /// $Revision: $
  /// $Date: $
  ///
  /// Declare the latest version of the class
  typedef BPhysMetaData_v1 BPhysMetaData;
} // namespace xAOD

// Declare a CLID for the type:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::BPhysMetaData , 247987690 , 1 )

#endif // XAODBPHYS_BPHYSMETADATA_H
