// Dear emacs, this is -*- c++ -*-
// $Id: $
#ifndef XAODBPHYS_BPHYSMETADATAAUXINFO_H
#define XAODBPHYS_BPHYSMETADATAAUXINFO_H

// Local include(s):
#include "xAODBPhys/versions/BPhysMetaDataAuxInfo_v1.h"

namespace xAOD {
   /// Declare the latest version of the class
   typedef BPhysMetaDataAuxInfo_v1 BPhysMetaDataAuxInfo;
} // namespace xAOD

// Declare a CLID for the type:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::BPhysMetaDataAuxInfo, 223945583, 1 )

#endif // XAODBPHYS_BPHYSMETADATAAUXINFO_H
