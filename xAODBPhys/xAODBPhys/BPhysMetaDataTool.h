// Dear emacs, this is -*- c++ -*-
// $Id: $
#ifndef XAODBPHYS_BPHYSMETADATATOOL_H
#define XAODBPHYS_BPHYSMETADATATOOL_H

// System include(s):
#include <string>
#include <memory>

// Infrastructure include(s):
#include "AsgTools/AsgMetadataTool.h"
#ifdef ASGTOOL_ATHENA
#include "AthenaPoolKernel/IMetaDataTool.h"
#endif // ASGTOOL_ATHENA

// EDM include(s):
#include "xAODBPhys/BPhysMetaData.h"
#include "xAODBPhys/BPhysMetaDataAuxInfo.h"

namespace xAOD {

  /// Dual-use tool taking care of xAOD::BPhysMetaData information
  ///
  /// Designed after FileMetaDataTool.
  /// 
  /// This dual-use tool can be used both in Athena and in AnalysisBase
  /// to either write B physics file-level metadata to the output (D)xAOD or  
  /// to read the B physics file-level metadata from an input (D)xAOD file.
  ///
  /// The metadata may be provided individually or as std::maps to
  /// the metadata object created by calling getInputMetaData().
  ///
  /// From the output metadata object obtained by calling
  /// getOutputMetaData() the metadata my either be extracted
  /// individually by accessor methods or as std::maps (one for each
  /// type.)
  /// 
  /// The following types are available (see BPhysMetaData class):
  /// - std::string
  /// - int
  /// - float
  /// - double
  /// - bool
  /// - std::vector<std::string>
  /// - std::vector<int>
  /// - std::vector<float>
  /// - std::vector<double>
  /// - std::vector<bool>
  ///
  /// The following job options are available:
  /// InputKey : Name of the input metadata collection (for reading).
  /// OutputKey: Name of the output metadata collection (for writing).
  /// CopyInput: True|False : Copy the input metadata to the output
  ///            metadata object.  (Requires InputKey and OutputKey
  ///            to be set.)
  /// Prefix   : Prefix each metadata item name with this string.
  ///            Useful to avoid type clashes in the TypeRegistry.
  ///            Only applied to the output.
  ///
  /// Notes:
  /// - Output metadata are written at the call of metaDataStop()
  ///   by the ToolSvc.
  /// - To dump all metadata read or to be written, use
  ///   metaDataToString(bool withTypes, bool forOutput).
  ///
  /// @author Wolfgang Walkowiak <Wolfgang.Walkowiak@cern.ch>
  ///
  /// $Revision:$
  /// $Date: $
  ///
   class BPhysMetaDataTool : public asg::AsgMetadataTool
#ifdef ASGTOOL_ATHENA
                          , public virtual ::IMetaDataTool
#endif // ASGTOOL_ATHENA
   {

     /// Declare the correct interface and constructor for Athena
     ASG_TOOL_INTERFACE( xAOD::BPhysMetaDataTool )
     ASG_TOOL_CLASS2( BPhysMetaDataTool, xAOD::BPhysMetaDataTool, ::IMetaDataTool )
     
   public:
     /// Regular AsgTool constructor
     BPhysMetaDataTool( const std::string& name = "BPhysMetaDataTool" );
     
     /// Function initialising the tool
     virtual StatusCode initialize();
     
     /// Pointer to input metadata object
     virtual const xAOD::BPhysMetaData* getInputMetaData();
     
     /// Pointer to output metadata object
     virtual xAOD::BPhysMetaData* getOutputMetaData();

     ///  Function dumping metadata contents to string
     virtual TString metaDataToString(bool withTypes=false,
				      bool forOutput=false);
       

   protected:
     /// @name Functions called by the AsgMetadataTool base class
     /// @{
     
     /// Function collecting the metadata from a new input file
     virtual StatusCode beginInputFile();
     
     /// Function making sure that BeginInputFile incidents are not missed
     virtual StatusCode beginEvent();
     
     /// Function writing the collected metadata to the output
     virtual StatusCode metaDataStop();

      /// @}

   private:
     /// Create output objects
     void createOutputObjects();
     
   private:
     /// Key of the metadata object in the input file
     std::string m_inputKey;  //!
     /// Key of the metadata object for the output file
     std::string m_outputKey; //!
     /// Copy input metadata to output?
     bool m_copyInput;        //!
     /// Prefix for variable names (to avoid type clashes)
     std::string m_prefix;
     
     /// The output interface object
     std::unique_ptr< xAOD::BPhysMetaData > m_md;           //!
     /// The output auxiliary object
     std::unique_ptr< xAOD::BPhysMetaDataAuxInfo > m_mdAux; //!

     /// The input metadata object pointer
     const xAOD::BPhysMetaData* m_inputMd; //!
     
     /// Internal flag for keeping track of whether a BeginInputFile incident
     /// was seen already
     bool m_beginFileIncidentSeen; //!
     
   }; // class BPhysMetaDataTool

} // namespace xAOD

#endif // XAODBPHYS_BPHYSMETADATATOOL_H
