// Dear emacs, this is -*- c++ -*-
#ifndef XAODBPHYS_XAODBPHYSATHENA_H
#define XAODBPHYS_XAODBPHYSATHENA_H

// This file is used to convince checkreq.py that the correct dependencies
// were declared for the package. It should never be explicitly included
// by any client code.
//
// Idea copied from xAODMetaDataCnv/xAODMetaDataCnvAthena.h

#include "AthenaPoolKernel/IMetaDataTool.h"
 
#endif // XAODBPHYS_XAODBPHYSATHENA_H
