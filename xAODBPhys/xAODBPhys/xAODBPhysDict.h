// Dear emacs, this is -*- c++ -*-
// $Id: $
#ifndef XAODBPHYS_XAODBPHYSDICT_H
#define XAODBPHYS_XAODBPHYSDICT_H

// Local include(s):
#include "xAODBPhys/versions/BPhysMetaData_v1.h"
#include "xAODBPhys/versions/BPhysMetaDataAuxInfo_v1.h"

#endif // XAODBPHYS_XAODBPHYSDICT_H
