// Dear emacs, this is -*- c++ -*-
// $Id: $
#ifndef XAODBPHYS_VERSIONS_BPHYSMETADATAAUXINFO_V1_H
#define XAODBPHYS_VERSIONS_BPHYSMETADATAAUXINFO_V1_H

// System include(s):
#include <string>

// EDM include(s):
#include "xAODCore/AuxInfoBase.h"

namespace xAOD {

   /// Auxiliary store for xAOD::BPhysMetaData_v1
   ///
   /// This is a fairly generic auxiliary store implementation to be
   /// used with xAOD::BPhysMetaData_v1. Most of the variables in that
   /// class are dynamic, so this class is really small.
   ///
   /// @author Wolfgang Walkowiak <Wolfgang.Walkowiak@cern.ch>
   ///
   /// $Revision: $
   /// $Date: $
   ///
   class BPhysMetaDataAuxInfo_v1 : public AuxInfoBase {

   public:
      /// Default constructor
      BPhysMetaDataAuxInfo_v1();

   private:
     /// Example static string variable
     // std::string myvariable

   }; // class BPhysMetaDataAuxInfo_v1

} // namespace xAOD

// Declare a base class for the type:
#include "xAODCore/BaseInfo.h"
SG_BASE( xAOD::BPhysMetaDataAuxInfo_v1, xAOD::AuxInfoBase );

#endif // XAODBPHYS_VERSIONS_BPHYSMETADATAAUXINFO_V1_H
