// Dear emacs, this is -*- c++ -*-
// $Id: $
//
//
#ifndef XAODBPHYS_VERSIONS_BPHYSMETADATA_V1_H
#define XAODBPHYS_VERSIONS_BPHYSMETADATA_V1_H

// System include(s):
#include <iosfwd>
#include <map>
#include <vector>

// EDM include(s):
#include "AthContainers/AuxElement.h"

// ROOT include(s):
#include "TString.h"

namespace xAOD {

  /// Class holding file-level metadata about an xAOD file
  /// specific to B-physics derivations.
  ///
  /// Each xAOD file is expected to hold exactly one object of this type
  /// to describe much of the simple metadata about the file. So, metadata
  /// that might change event-by-event in the file is not stored in such
  /// objects.
  ///
  /// Design inspired by FileMetaData_v1.h
  ///
  /// @param Wolfgang Walkowiak <Wolfgang.Walkowiak@cern.ch>
  ///
  /// $Revision: $
  /// $Date: $
  ///
  class BPhysMetaData_v1 : public SG::AuxElement {

  public:
    /// Default constructor
    BPhysMetaData_v1();
    
    /// @name Comparison operators
    /// @{
    
    /// Operator testing the equality of two objects
    bool operator==( const BPhysMetaData_v1& rhs );
    /// Operator testing the inequality of two objects
    bool operator!=( const BPhysMetaData_v1& rhs );
    
    /// @}

    /// Set prefix for variable names
    void setPrefix(std::string prefix);
    
    /// Map of metadata names and types
    std::map<std::string, const std::type_info*> varTypes() const;

    /// List of metadata names and types as string
    TString varTypesToString(TString header="") const;

    /// Get a generic string value out of the object
    bool value( const std::string& name, std::string& val ) const;
    
    /// Set a generic string value on the object
    bool setValue( const std::string& name, const std::string& val );
    
    /// Get a generic int value out of the object
    bool value( const std::string& type, int& val ) const;
    
    /// Set a generic int value on the object
    bool setValue( const std::string& type, int val );

    /// Get a generic float value out of the object
    bool value( const std::string& type, float& val ) const;
    
    /// Set a generic float value on the object
    bool setValue( const std::string& type, float val );

    /// Get a generic double value out of the object
    bool value( const std::string& type, double& val ) const;
    
    /// Set a generic double value on the object
    bool setValue( const std::string& type, double val );

    /// Get a generic bool  value out of the object
    bool value( const std::string& type, bool& val ) const;
    
    /// Set a generic bool; value on the object
    bool setValue( const std::string& type, bool val );

    /// Get a generic vector<int> out of the object
    bool value( const std::string& type, std::vector<int>& val ) const;
    
    /// Set a generic vector<int> on the object
    bool setValue( const std::string& type, std::vector<int>& val );

    /// Get a generic vector<float> out of the object
    bool value( const std::string& type, std::vector<float>& val ) const;
    
    /// Set a generic vector<float> on the object
    bool setValue( const std::string& type, std::vector<float>& val );

    /// Get a generic vector<double> out of the object
    bool value( const std::string& type, std::vector<double>& val ) const;
    
    /// Set a generic vector<double> on the object
    bool setValue( const std::string& type, std::vector<double>& val );

    /// Get a generic vector<bool> out of the object
    bool value( const std::string& type, std::vector<bool>& val ) const;
    
    /// Set a generic vector<bool> on the object
    bool setValue( const std::string& type, std::vector<bool>& val );

    /// Get a generic vector<string> out of the object
    bool value( const std::string& type, std::vector<std::string>& val ) const;
    
    /// Set a generic vector<string> on the object
    bool setValue( const std::string& type, std::vector<std::string>& val );

    /// Get a map of all metadata of type int
    std::map<std::string, int> valuesI() const;

    /// Set a map of metadata of type int
    bool setValues(const std::map<std::string, int >& metaMap);
    
    /// Get a map of all metadata of type float
    std::map<std::string, float> valuesF() const;

    /// Set a map of metadata of type float
    bool setValues(const std::map<std::string, float >& metaMap);
    
    /// Get a map of all metadata of type double
    std::map<std::string, double> valuesD() const;

    /// Set a map of metadata of type double
    bool setValues(const std::map<std::string, double >& metaMap);
    
    /// Get a map of all metadata of type bool
    std::map<std::string, bool> valuesB() const;

    /// Set a map of metadata of type bool
    bool setValues(const std::map<std::string, bool >& metaMap);    

    /// Get a map of all metadata of type string
    std::map<std::string, std::string> valuesS() const;

    /// Set a map of metadata of type string
    bool setValues(const std::map<std::string, std::string >& metaMap);    

    /// Get a map of all metadata of type vector<int>
    std::map<std::string, std::vector<int> > valuesVI() const;

    /// Set a map of metadata of type vector<int>
    bool setValues(const std::map<std::string, std::vector<int> >& metaMap);    

    /// Get a map of all metadata of type vector<float>
    std::map<std::string, std::vector<float> > valuesVF() const;

    /// Set a map of metadata of type vector<float>
    bool setValues(const std::map<std::string, std::vector<float> >& metaMap);
    
    /// Get a map of all metadata of type vector<double>
    std::map<std::string, std::vector<double> > valuesVD() const;

    /// Set a map of metadata of type vector<double>
    bool setValues(const std::map<std::string, std::vector<double> >& metaMap);

    /// Get a map of all metadata of type vector<bool>
    std::map<std::string, std::vector<bool> > valuesVB() const;

    /// Set a map of metadata of type vector<bool>
    bool setValues(const std::map<std::string, std::vector<bool> >& metaMap);  

    /// Get a map of all metadata of type vector<string>
    std::map<std::string, std::vector<std::string> > valuesVS() const;

    /// Set a map of metadata of type vector<string>
    bool setValues(const std::map<std::string, std::vector<std::string> >&
		   metaMap);
    
    /// Dump all metadata info to a string
    TString toString(TString header="") const;

  private:
    std::string m_prefix; //!
    
  }; // class BPhysMetaData_v1
  
} // namespace xAOD

// Declare a base class for the type:
#include "xAODCore/BaseInfo.h"
SG_BASE( xAOD::BPhysMetaData_v1, SG::AuxElement );

#endif // XAODBPHYS_VERSIONS_BPHYSMETADATA_V1_H
