/**
   @mainpage: xAODBPhys package

   @author: Daniel Scheirich <daniel.scheirich@cern.ch>

   @section: xAODBPhys Overview

   This package holds the EDM classes describing the output of ATLAS's
   B decay reconstruction defined by the B-physics group.

*/
