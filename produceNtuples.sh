#!/bin/bash

if (( $# < 1 )); then
    echo "Add run number"
    return 0
fi

inputGRL="/afs/cern.ch/atlas/www/GROUPS/DATABASE/GroupData/GoodRunsLists/data17_13TeV/20170808/data17_13TeV.periodAllYear_DetStatus-v91-pro21-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"

##No need to change after this line
#resubmit="v1"
resubmit=""
cp -n "$inputGRL" "$ROOTCOREBIN/data/BplusxAODAnalysis/"
timeStamp=`date +%Y%m%d%H%M`
for var in "$@"
do
   rm -fv tmpRunList.txt
   rucio list-dids data18_13TeV.*$var*.physics_BphysLS.*.DAOD_BPHY5.* | grep "data18_13TeV:" | grep "| CONTAINER" >> tmpRunList.txt
   sed -i 's/                | CONTAINER    |//g' tmpRunList.txt
   sed -i 's/| //g' tmpRunList.txt
   sort tmpRunList.txt -o tmpRunList.txt
   read -r firstline<tmpRunList.txt
   root -l -b -q '$ROOTCOREDIR/scripts/load_packages.C' 'BplusxAODAnalysis/grid/produceNtuplesSubmit.C('$var',"'$firstline'","'$inputGRL'","'$resubmit'")' &
   sleep 10s
done

rm -fv tmpRunList.txt
