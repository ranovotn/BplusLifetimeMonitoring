#!/bin/bash
runStart=350184
runStop=350184
####2018
#A			350184			350184
####2017
#A			324320			325558
#B			325713			328393
#C			329385			330470
#D			330857			332304
#E			332720			334779
#F			334842			335290
#TS1-TS2	325713			335290
#G			335302			335302
#H			336497			336782
#I			336832			337833
#K			338183			338675
#L 			341257			341649

rm -fv "fitResults.log"
nohup root -l -b -q "./BplusLifetime/mainBplusLifetimeFit.cxx("$runStart" ,"$runStop")" >> "fitResults.log" 2>&1 &
