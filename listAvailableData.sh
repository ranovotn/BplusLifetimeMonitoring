#!/bin/bash
rm dataAvailable.txt
rm dataNtuples.txt
rucio list-dids data18_13TeV.*.physics_BphysLS.*.DAOD_BPHY5.* | grep "data18_13TeV:" | grep "| CONTAINER" >> dataAvailable.txt
sort dataAvailable.txt -o dataAvailable.txt
sed -i 's/| data18_13TeV:data18_13TeV.00//g' dataAvailable.txt 
sed -i 's/\.physics_BphysLS.*//g' dataAvailable.txt
sort dataAvailable.txt | uniq >> dataAvailableTmp.txt
mv dataAvailableTmp.txt dataAvailable.txt
#rucio list-dids user.ranovotn.data17_BPHY5.Bplus_run*_201709111410*_DefaultOutput.root | grep "data17_BPHY5" | grep "| CONTAINER" >> dataNtuples.txt
rucio list-dids user.ranovotn.data18_BPHY5.Bplus_run*_DefaultOutput.root | grep "data18_BPHY5" | grep "| CONTAINER" >> dataNtuples.txt
sort dataNtuples.txt -o dataNtuples.txt
sed -i 's/| user.ranovotn:user.ranovotn.data18_BPHY5.Bplus_run//g' dataNtuples.txt
sed -i 's/\_201709111410_.*//g' dataNtuples.txt

filename="dataAvailable.txt"

localNtup=0;
ntupGrid=0;
aodGrid=0;

aodGridString="";
ntupGridString="";

while read -r line
do
    name="$line"
    if grep -q $name "dataNtuples.txt"; then
    	if ls data/ | grep $name -q ;then
    		echo -e "Run: "$name" -- \033[32mNTUPLE LOCALLY\033[0m";	
    		let "localNtup += 1";
    	else
    		echo -e "Run: "$name" -- \033[33mNTUPLE ON GRID\033[0m";
    		let "ntupGrid += 1";
    		ntupGridString="$ntupGridString $name"
    	fi
    else
    echo -e "Run: "$name" -- \033[31mAOD ON GRID\033[0m";	
    let "aodGrid += 1";
    aodGridString="$aodGridString $name"
    fi

done < "$filename"

echo "--------------------------------------------------------";
echo "Summary:";
echo -e "\033[32m$localNtup ntuples available locally \033[0m";
echo -e "\033[33m$ntupGrid ntuples available on grid\033[0m";
if [ -n "$ntupGridString" ]; then
	echo "($ntupGridString )";
fi
echo -e "\033[31m$aodGrid BPHY5 available on grid\033[0m";
if [ -n "$aodGridString" ]; then
	echo "($aodGridString )";
fi